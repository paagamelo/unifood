<!DOCTYPE html>
<?php
require 'db_connect.php';
require 'login_functions.php';

sec_session_start(); //Avvio sessione php sicura

if($conn->connect_error) {
  die("Errore di comunicazione col server");
}

$logged = loggedAs($conn,"fattorini");
if($logged) { //Login effettuato
?>
<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- JQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- Font Awesome (for icons) -->
  <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>
  <!-- Topbar style -->
  <link rel="stylesheet" type="text/css" href="./topbar/topbar_style.css">
  <link rel="stylesheet" type="text/css" href="./modal.css">
  <link rel="stylesheet" type="text/css" href="./breadcrumb.css">
  <link rel="stylesheet" href="fattorini_ordini.css">
  <link rel="stylesheet" href="./alert.css">
  <script src="./fattorini_ordini.js"></script>
  <title>Consegne</title>
</head>
<body>
  <div class="main">
  <?php require './topbar/topbar.php'; ?>
  <header><h1>Le Tue Consegne</h1>
  <ul class="breadcrumb">
    <li><a href="./unifood.php">Home</a></li>
    <li>Consegne</li>
  </ul>
  </header>
  <div id="ordini-in-consegna">
    <header><h2>In Corso</h2></header>
    <table class="ordini in-consegna">
      <caption class="visuallyhidden">Ordini da consegnare</caption>
      <thead>
        <th>Fornitore</th>
        <th>Piatti</th>
        <th>Note</th>
        <th>Totale</th>
        <th>Pagato</th>
        <th>Cliente</th>
        <th>Orario</th>
        <th>Piano</th>
        <th></th>
      </thead>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
  <div id="ordini-conclusi">
    <header><h2>Concluse</h2></header>
    <table class="ordini conclusi">
      <caption class="visuallyhidden">Ordini consegnati</caption>
      <thead>
        <th>Fornitore</th>
        <th>Piatti</th>
        <th>Note</th>
        <th>Totale</th>
        <th>Pagato</th>
        <th>Cliente</th>
        <th>Orario</th>
        <th>Piano</th>
        <th>Data</th>
      </thead>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
  <div class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
  </div>
  </div>
  </div>
</body>
<?php } else header('Location: ./'.closeConnectionAndReturn($conn,"login.php?Atype=fattorini")); //Effettua il login per accedere
?>

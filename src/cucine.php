<?php
require 'db_connect.php';
require 'login_functions.php';
require 'common_queries.php';

sec_session_start(); //Avvio sessione php sicura
if($conn->connect_error) {
  die("Impossibile connettersi al database");
}
$logged = login_check($conn);
getCucine($conn, $cucine);
?>

<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- JQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- Font Awesome (for icons) -->
  <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>
  <!-- Topbar style -->
  <link rel="stylesheet" type="text/css" href="./topbar/topbar_style.css">
  <link rel="stylesheet" href="cucine.css">
  <link rel="stylesheet" href="breadcrumb.css">
  <title>Cucine</title>
</head>
<body>
  <?php require './topbar/topbar.php'; closeConnection($conn); ?>
  <header>
    <h1>Cucine</h1>
    <ul class="breadcrumb">
      <li><a href="./unifood.php">Home</a></li>
      <li>Cucine</li>
    </ul>
  </header>
  <section id="cucine">
    <?php
    if(isset($cucine["errore"]) && $cucine["errore"]) {
      echo "<p>Impossibile caricare le cucine</p>";
    } else if(empty($cucine)) {
      echo "<p>Nessuna cucina da mostrare</p>";
    } else {
      foreach($cucine as $key => $cucina) {
        echo'<a href="./fornitori.php?cucina='.$cucina["Id"].'">'.$cucina["Nome"].'</a>';
      }
    }
    ?>
  </section>
</body>
</html>

<?php
header('Content-Type: application/json');

require 'db_connect.php';
require 'login_functions.php';
require 'informazioni_ordini_fattorini.php';

sec_session_start();

if($conn->connect_error) {
  $output["errore"] = "errore";
  die(json_encode($output));
}


if(!loggedAs($conn, 'fattorini')) {
  $output["errore"] = "errore";
  closeConnection($conn);
  die(json_encode($output));
}


if(isset($_GET['request'])) {

  $idfattorino = $_SESSION['user_id'];
  $rows_per_page = 1;
  $output = array();

  switch($_GET['request']) {

    case 'nuovi':
    {
      getNuoveConsegneFattorino($conn, $idfattorino, $consegne);
      if(isset($consegne["errore"]) && $consegne["errore"]) {
        $output["errore"] = "errore";
        closeConnection($conn);
        die(json_encode($output));
      }

      print json_encode($consegne);
      break;
    }

    /* Is there more? */
    case 'more': //?request=more&page={..}
    {
      if(!isset($_GET['page']) || $_GET['page'] < -1) { //-1 = nothing is being displayed
        $output["errore"] = "errore";
        closeConnection($conn);
        die(json_encode($output));
      }

      $count = countVecchieConsegneFattorino($conn, $idfattorino);
      if($count < 0) {
        $output["errore"] = "errore";
        closeConnection($conn);
        die(json_encode($output));
      }

      $more = ($count > (($_GET['page'] + 1) * $rows_per_page)) ? "true" : "false";
      $output['more'] = $more;
      print json_encode($output);
      break;
    }

    case 'page': //?request=page&page={..}
    {
      if(!isset($_GET['page']) || $_GET['page'] < 0) {
        $output["errore"] = "errore";
        closeConnection($conn);
        die(json_encode($output));
      }

      $start_row = $_GET['page'] * $rows_per_page;
      $filter = "";


      getVecchieConsegneFattorinoLimit($conn, $idfattorino, $consegne, $start_row, $rows_per_page);
      if(isset($consegne["errore"]) && $consegne["errore"]) {
        $output["errore"] = "errore";
        closeConnection($conn);
        die(json_encode($output));
      }

      print json_encode($consegne);
      break;
    }

    case 'topage':
    {
      if(!isset($_GET['page']) || $_GET['page'] < 0) {
        $output["errore"] = "errore";
        closeConnection($conn);
        die(json_encode($output));
      }

      $to = ($_GET['page'] * $rows_per_page) + $rows_per_page;

      getVecchieConsegneFattorinoLimit($conn, $idfattorino, $consegne, 0, $to);
      if(isset($consegne["errore"]) && $consegne["errore"]) {
        $output["errore"] = "errore";
        closeConnection($conn);
        die(json_encode($output));
      }

      print json_encode($consegne);
      break;
    }
  }
  closeConnection($conn);
}

?>

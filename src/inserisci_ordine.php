<?php

function undoOrdine(&$conn, $idordine) {
  $stmt = $conn->prepare("DELETE FROM ORDINI WHERE Id = ?");
  if(  !$stmt
    || !$stmt->bind_param("i", $idordine)
    || !$stmt->execute())
  {
    return false;
  }
  return true;
}

function undoPiatti(&$conn, $idordine) {
  $stmt = $conn->prepare("DELETE FROM PIATTI_IN_ORDINE WHERE IdOrdine = ?");
  if(  !$stmt
    || !$stmt->bind_param("i", $idordine)
    || !$stmt->execute())
  {
    return false;
  }
  return true;
}

function undoModifiche(&$conn, $idordine) {
  $stmt = $conn->prepare("DELETE FROM MODIFICHE WHERE IdOrdine = ?");
  if(  !$stmt
    || !$stmt->bind_param("i", $idordine)
    || !$stmt->execute())
  {
    return false;
  }
  return true;
}

function undo(&$conn, $idordine) {
  if(
       !undoModifiche($conn, $idordine)
    || !undoPiatti($conn, $idordine)
    || !undoOrdine($conn, $idordine))
  {
    return "fatal: impossibile rimuovere ordine";
  }
  return "ordine rimosso";
}

require 'login_functions.php';
require 'check_ordine.php';
require 'db_connect.php';
require 'checkAggiornaDati.php';
sec_session_start(); //Avvio sessione php sicura

if($conn->connect_error) {
  print "errore di comunicazione";
  die();
}

if(
     !loggedAs($conn,"clienti")
  || !isset($_SESSION['pendingorder'])
  || !isset($_SESSION['pendingorder']['idfornitore'])
  || !isset($_SESSION['pendingorder']['piatti'])
  || !isset($_POST['orario'])
  || !isset($_POST['piano'])
  || !isset($_POST['pagatoYN'])
  || !isset($_POST['note']))
{
  print "errore";
  closeConnection($conn);
  die();
}

$idfornitore = $_SESSION['pendingorder']['idfornitore'];//$_POST['idfornitore'];
$piatti = $_SESSION['pendingorder']['piatti'];//$_POST['piatti'];
$orario = $_POST['orario'];
$piano = $_POST['piano'];
$pagato = $_POST['pagatoYN'];
$note = $_POST['note'];
$userid = $_SESSION['user_id'];
$data = date("Y-m-d H:i:s");

getInfoFornitore($conn, $idfornitore, $fornitore);
if((isset($fornitore['errore']) && $fornitore['errore']) || empty($fornitore)) {
  print "errore";
  closeConnection($conn);
  die();
}

if(
     !checkOrdine($conn, $idfornitore, $piatti, $output)
  || !checkOrario($orario)
  || !checkOrarioPrecedente($orario)
  || ($orario > $fornitore["ApertoA"])
  || ($orario < $fornitore["ApertoDa"])
  || !is_numeric($piano)
  || !checkRangeNumber($piano, 1, 3)
  || !(strlen($note) < 100))
{
  print "errore";
  closeConnection($conn);
  die();
}

$totale = (float)$output['totale'];

if($pagato) {
  if(
       !isset($_POST['numero'])
    || !isset($_POST['tipo']))
  {
    print $_POST['tipo'];
    closeConnection($conn);
    die();
  }

  $numero = substr($_POST["numero"],-3);
  $tipo = $_POST["tipo"];
  if(
       !checkCarta($tipo, $numero, $userid)
    || checkCartaNonPresente($conn, $tipo.$numero, $userid) != 'yet-present')
  {
    print "errore";
    closeConnection($conn);
    die();
  }
}

if($note == "") {
  $note = NULL;
}

$stmt = $conn->prepare(
  "INSERT INTO ORDINI(OraConsegna, PianoConsegna, Stato, Data, Totale, PagatoYN, Note, IdFornitore, IdUtente)
  VALUES (?, ?, 'In preparazione', ?, ?, ?, ?, ?, ?)");

if(
     !$stmt
  || !$stmt->bind_param("sisdisii", $orario, $piano, $data, $totale, $pagato, $note, $idfornitore, $userid)
  || !$stmt->execute())
{
  print "errore: " . undo($conn, $idordine);
  closeConnection($conn);
  die();
}

$idordine = $stmt->insert_id;
$stmt->close();

foreach($piatti as $piatto) {

  if(!isset($piatto["id"]) || !isset($piatto["quantita"])) { //in realtà già controllati in check ordine
    print "errore: " . undo($conn, $idordine);
    closeConnection($conn);
    die();
  }

  $idpiatto = $piatto["id"];
  $quantita = $piatto["quantita"];

  $stmt = $conn->prepare(
    "INSERT INTO PIATTI_IN_ORDINE(IdOrdine, Quantita, IdFornitore, IdPiattoInFornitore)
    VALUES (?, ?, ?, ?)");
  if(
       !$stmt
    || !$stmt->bind_param("iiii", $idordine, $quantita, $idfornitore, $idpiatto)
    || !$stmt->execute())
  {
    print "errore: " . undo($conn, $idordine);
    closeConnection($conn);
    die();
  }

  $idpiatto_in_ordine = $stmt->insert_id;
  $stmt->close();

  if(isset($piatto["modifiche"]) && !empty($piatto["modifiche"])) {

    foreach($piatto["modifiche"] as $modifica) {
      if(!isset($modifica["id"]) || !isset($modifica["isAddition"])) {
        print "errore: " . undo($conn, $idordine);
        closeConnection($conn);
        die();
      }
      $idmodifica = $modifica["id"];
      $isAddition = $modifica["isAddition"];
      $stmt = $conn->prepare(
        "INSERT INTO MODIFICHE(IdOrdine, IdPiattoInOrdine, IdIngrediente, AggiuntaYN)
        VALUES (?, ?, ?, ?)");
      if(
           !$stmt
        || !$stmt->bind_param("iiii", $idordine, $idpiatto_in_ordine, $idmodifica, $isAddition)
        || !$stmt->execute())
      {
        print "errore: " . undo($conn, $idordine);
        closeConnection($conn);
        die();
      }
      $stmt->close();
    }
  }
}

$notifica_cliente = "Il tuo ordine contenente ";
$first = true;
foreach($piatti as $piatto) {
  if(!$first) $notifica_cliente .= ", ";
  $first = false;
  $notifica_cliente .= $piatto["nome"].' x'.$piatto["quantita"];
  if(isset($piatto["modifiche"]) && !empty($piatto["modifiche"])) {
    $notifica_cliente .= ' (';
    $first2 = true;
    foreach($piatto["modifiche"] as $modifica) {
      if(!$first2) $notifica_cliente .= ", ";
      $first2 = false;
      $notifica_cliente .= (($modifica["isAddition"] == "true") ? "+ " : "- ").$modifica["nome"];
    }
    $notifica_cliente .= ')';
  }
}

$notifica_cliente .= " è in preparazione";
$notifica_fornitore = "Hai un nuovo ordine per le " .$orario;

$stmt = $conn->prepare("INSERT INTO NOTIFICHE(Testo, LettaYN, IdFornitore, IdOrdine) VALUES(?, '0', ?, ?)");
if(
     !$stmt
  || !$stmt->bind_param("sii", $notifica_fornitore, $idfornitore, $idordine)
  || !$stmt->execute())
{
  print "errore: " . undo($conn, $idordine);
  closeConnection($conn);
  die();
}
$stmt->close();

$stmt = $conn->prepare("INSERT INTO NOTIFICHE(Testo, LettaYN, IdUtente, IdOrdine) VALUES(?, '0', ?, ?)");
if(
     !$stmt
  || !$stmt->bind_param("sii", $notifica_cliente, $userid, $idordine)
  || !$stmt->execute())
{
  print "errore: " . undo($conn, $idordine);
  closeConnection($conn);
  die();
}
$stmt->close();

unset($_SESSION['pendingorder']);

print "success";

closeConnection($conn);

?>

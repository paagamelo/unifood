$(document).ready(function(){

  var pathh = window.location.pathname;
  var path = pathh.split("/")
  var isCheckout = path[path.length - 1] == 'checkout.php';
  //console.log(path[path.length - 1]);

  //Salva carta
  $(document).on("click", "#salva-carta", function() {
    event.preventDefault();
    $(".error").remove(); //Rimuovi errori precedentemente visualizzati
    var intestatario = $("input#intestatario").val().trim();
    var numero = $("input#numero").val().trim();
    var mese = $("input#mese").val().trim();
    var anno = $("input#anno").val().trim();
    var errori =
                    checkIntestatario(intestatario)
                  + checkNumeroCarta(numero)
                  + checkMese(mese)
                  + checkAnno(anno);

    if(errori != "") $("#modal-file").append("<div class=\"error\"><ul>" + errori + "</ul></div>");
    else{ //Dati inseriti validi
      numero = numero.slice(-3); //Prese le ultime 3 cifre
      var tipo = $("#tipo-carta").val();
      tipo = $("select option[value="+tipo+"]").html();
      $.post("./aggiorna_impostazioni.php",
      {
        request: "aggiungi-carta",
        numero: numero,
        tipo: tipo
      },
      function(state){
        var messaggio = "";
        if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
        else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
        else if(state == "yet-present") messaggio = "La carta è già stata salvata";
        if(messaggio != "") $("#modal-file").append("<div class=\"error\">"+messaggio+"</div>");
        else{ //Non si sono verificati errori, chiudi la modal e mostra la carta aggiunta
          $(".modal").hide();
          if(isCheckout) {
            var html = '<div class="row"><label for="' + numero + '"></label>'
                      + '<input type="radio" name="pagamento" value="' + numero + '"checked>'
                      + '<span class="symbol card"><i class="far fa-credit-card"></i></span>'
                      + '<span class="numero-carta">***' + numero + '</span>'
                      + '<span class="tipo-carta">' + tipo + '</span><br/></div>';
            $("#pagamento form .row:first").after(html);
          } else {
            $("#modal-file").children().remove();
            var string =
                        "<li class=\"carta-pagamento\">"
                        + "<span class=\"symbol minus\" aria-label=\"Rimuovi\"><i aria-hidden=\"true\" class=\"fas fa-minus-circle fa-lg\"></i></span>"
                        + "<span class=\"symbol card\"><i class=\"far fa-credit-card\"></i></span>"
                        + "<span class=\"numero-carta\">***"+numero+"</span>"
                        + "<span class=\"tipo-carta\">"+tipo+"</span></li>";
            $("div.no-carte").remove();
            $("ul.carte-pagamento").append(string);
          }
        }
      });
    }
  });
});

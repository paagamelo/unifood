var aggiungi_categoria = "<div class=\"form-group\"><label for=\"nuova-categoria\">Nome categoria:</label><input type=\"text\" class=\"form-control\" id=\"nuova-categoria\" name=\"nome-categoria\" maxlength=\"25\"></input></div>";

function aggiornaCategorie(categoria, id){
  var nuova_categoria ="<option value=\""+id+"\">"+categoria+"</option>"
  $("option[value=new]").before(nuova_categoria);
  $("#nuova-categoria").val("");
  categorieIngredienti[id] = categoria;
  var display_categoria = "<li class=\"categoria-ingredienti\" id=\"cat"+id+"\"><span class=\"nome-categoria-ingrediente\">"+categoria+"</span>"+angolodx+"<ul class=\"ingredienti\"></ul>"+no_ingredienti;
  $("#ingredienti-disponibili ul.categorie-ingredienti").append(display_categoria);
  var cucine = $("li.cucina");
  var length = cucine.length;
  for(var i = 0; i<length; i++){
    var idCucina = cucine[i]["id"];
    display_categoria = "<li class=\"categoria-ingrediente\" id=\""+idCucina+"c"+id+"\"><span class=\"nome-categoria-ingrediente\">"+categoria+"</span><ul class=\"ingredienti\"></ul>"+no_ingredienti;
    $("#"+idCucina+" ul.categorie-ingredienti").append(display_categoria);
  }
}

function aggiornaIngredienti(idIngrediente, ingrediente, idCategoria){
  var infoIngrediente = [];
  infoIngrediente["StrutturaleYN"] = (idCategoria == 0 ? 0 : 1);
  infoIngrediente["IdCategoria"] = (idCategoria == 0 ? null : idCategoria);
  infoIngrediente["Nome"] = ingrediente;
  infoIngrediente["Id"] = idIngrediente;
  ingredienti_disponibili[idIngrediente] = infoIngrediente;
  var idcat = (infoIngrediente["StrutturaleYN"] == 0 ? 0 : idCategoria);
  var id = "cat"+ idcat + "i"+ idIngrediente;
  var nome = "<li class=\"ingrediente\" id=\""+id+"\">"+più+"<span class=\"nome-ingrediente\">"+ingrediente+"</span></li>";
  if($("#cat"+idcat).find("li.ingrediente").length == 0) $("#cat"+idcat+" .no-ingredienti").remove();
  $("#cat"+idcat+" ul.ingredienti").append(nome);
  $("#nome-ingrediente").val("");
}

function creazioneIngrediente(ingrediente, categoria){
  $.post("./aggiorna_ingredienti_fornitori.php",
  {
    request: "crea-ingrediente",
    ingrediente: ingrediente,
    categoria: categoria
  },
  function(state){
    var messaggio = "";
    if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
    else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
    else if(state == "yet-present") messaggio = "L'ingrediente è già stato inserito";
    if(messaggio != "") $("#modal-file").append("<div class=\"error\">"+messaggio+"</div>");
    else{ //Non si sono verificati errori, chiudi la modal e mostra la carta aggiunta
      if(!checkNumber(state)){
        $("#modal-file").append("<div class=\"ok\">Inserimento ingrediente avvenuto correttamente!</div>");
        aggiornaIngredienti(state,ingrediente, categoria);
      }
      else $("#modal-file").append("<div class=\"error\">Qualcosa è andato storto, ricaricare la pagina per aggiornare i dati ed evitare errori</div>");
    }
  });
}

//Controlla se bisogna aggiungere/rimuovere la nuova categoria
function checkCategorie(){
  if($("#categoria-ingrediente").val() == "new") $("#crea-ingrediente").before(aggiungi_categoria);
  else if($("#nuova-categoria")[0] != undefined) $("#nuova-categoria").parent().remove();
}

$(document).ready(function(){
  //Aggiungi ingrediente
  $(document).on("click", "#crea-ingrediente", function() {
    event.preventDefault();
    $(".error").remove(); //Rimuovi errori precedentemente visualizzati
    $(".ok").remove(); //Rimuovi errori precedentemente visualizzati
    var nome_ingrediente = $("#nome-ingrediente").val().trim();
    var categoria = $("#categoria-ingrediente").val().trim();
    var nome_categoria = "";
    var errori  = "";
    if(categoria == "new"){
      nome_categoria = $("#nuova-categoria").val().trim();
      errori += checkNomeCategoria(nome_categoria);
    } else errori += checkIdCategoria(categoria);
    errori += checkNomeIngrediente(nome_ingrediente);

    if(errori != "") $("#modal-file").append("<div class=\"error\"><ul>" + errori + "</ul></div>");
    else{ //Dati inseriti validi
      if(nome_categoria != ""){ //Bisogna creare la categoria
        $.post("./aggiorna_ingredienti_fornitori.php",
        {
          request: "crea-categoria",
          categoria: nome_categoria,
        },
        function(state){
          var messaggio = "";
          if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
          else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
          else if(state == "yet-present") messaggio = "La nuova categoria inserita è già esistente";
          if(messaggio != "") $("#modal-file").append("<div class=\"error\">"+messaggio+"</div>");
          else{ //Non si sono verificati nell'inserimento della categoria
            if(!checkNumber(state)){
              $("#modal-file").append("<div class=\"ok\">Inserimento categoria avvenuto correttamente!</div>");
              aggiornaCategorie(nome_categoria, state);
              creazioneIngrediente(nome_ingrediente,state);
            } else $("#modal-file").append("<div class=\"error\">Qualcosa è andato storto, ricaricare la pagina per aggiornare i dati ed evitare errori</div>");
          }
        });
      } else creazioneIngrediente(nome_ingrediente, categoria);
    }
  });
});

<?php
require 'db_connect.php';
require 'login_functions.php';
sec_session_start(); //Avvio sessione php sicura

if($conn->connect_error) {
  die("errore");
}

$logged = loggedAs($conn, 'admin');

if($logged){
  if(isset($_POST['query'])) {
    $result = $conn->query($_POST['query']);//mysql_query($conn, $_POST['query']);//$conn->query($_POST['query']);
    if(!$result) {
      die("errore");
    }
  } else {
    $result = array();
  }

?>
<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- JQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- Font Awesome (for icons) -->
  <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>
  <script src="./admin_richieste_script.js"></script>
  <link rel="stylesheet" type="text/css" href="./topbar/topbar_style.css">
  <link rel="stylesheet" type="text/css" href="./admin_richieste_style.css">
  <link rel="stylesheet" type="text/css" href="./admin_gestisci_style.css">
  <title>Gestisci sito</title>
</head>
<body>
  <div class="main">
  <?php require './topbar/topbar.php'; closeConnection($conn); ?>
  <header><h1>Gestisci sito</h1></header>
  <ul class="breadcrumb">
    <li><a href="./unifood.php">Home</a></li>
    <li>Gestisci sito</li>
  </ul>
  <section id="query">
    <form action="./admin_gestisci.php" method="post">
    <textarea name="query" rows="10" cols="50"></textarea>
    <input type="submit" value="query">
    </form>
  </section>
  <section id="result">
    <?php
    if(isset($_POST['query'])) {
      echo "<table><tr>";
      for($i = 0; $i < mysqli_num_fields($result); $i++) {
        $field_info = mysqli_fetch_field($result);
        echo "<th>{$field_info->name}</th>";
      }

      // Print the data
      while($row = mysqli_fetch_row($result)) {
          echo "<tr>";
          foreach($row as $_column) {
              echo "<td>{$_column}</td>";
          }
          echo "</tr>";
      }
      echo "</table>";
    }
     ?>
  </section>
</div>
</body>
<?php } else header('Location: ./'.closeConnectionAndReturn($conn,"login.php?Atype=admin")); ?>

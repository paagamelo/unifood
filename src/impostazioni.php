<!DOCTYPE html>
<?php
require 'db_connect.php';
require 'login_functions.php';

sec_session_start(); //Avvio sessione php sicura
$logged = login_check($conn);

if($logged) { //Login effettuato
?>
  <html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Font Awesome (for icons) -->
    <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>
    <!-- Topbar style -->
    <link rel="stylesheet" type="text/css" href="./topbar/topbar_style.css">
    <link rel="stylesheet" type="text/css" href="./modal.css">
    <link rel="stylesheet" type="text/css" href="impostazioni_style.css">
    <link rel="stylesheet" type="text/css" href="./breadcrumb.css">
    <script src="./impostazioni.js"></script>
    <script src="./modifica_password.js"></script>
    <script src="./checkDati.js"></script>
    <script src="./sha512.js"></script>
    <?php
    if($_SESSION["atype"] == "clienti"){ ?>
    <script src="./aggiungi_carta.js"></script>
    <?php ;}?>
    <title>Impostazioni</title>
  </head>
  <body>
    <header>
      <h1>Impostazioni</h1>
    <ul class="breadcrumb">
      <li><a href="./unifood.php">Home</a></li>
      <li>Impostazioni</li>
    </ul>
    </header>
    <section id="dati-personali">
      <header><h2>Dati Personali</h2></header>
      <button type="button" class="personal-data modify-btn">Modifica</button>
      <ul class="dati-personali">
      <?php
      if($_SESSION["atype"] != "fornitori" && $_SESSION["atype"] != "admin"){ ?>
        <li class="telefono">
          <span class="telefono-lb label">Telefono:</span><span class="telefono"></span>
        </li>
      <?php ;} ?>
        <li class="username">
          <span class="username-lb label">Username:</span><span class="username"></span>
        </li>
        <?php if($_SESSION["atype"] == "fattorini") { ?>
        <li class="datore-lavoro">
          <span class="datore-lavoro-lb label">Datore di lavoro:</span><span class="datore-lavoro"></span>
        </li>
      <?php } ?>
      </ul>
    </section>
    <?php
    if($_SESSION["atype"] == "clienti"){?>
    <section id="metodi-pagamento">
      <header><h2>Metodi di Pagamento</h2></header>
      <button type="button" class="payment-card modify-btn">Modifica</button>
      <ul class="carte-pagamento">
      <!-- Metodi di pagamento -->
      </ul>
    </section>
    <?php ;} ?>
    <section id="password">
      <header class="hidden"><h2>Password</h2></header>
      <button type="button" class="password modify-btn">Modifica password</button>
    </section>
    <!-- The Modal -->
    <div class="modal">
      <!-- Modal content -->
      <div class="modal-content">
        <span class="close">&times;</span>
        <!-- Load modal file-->
        <div id="modal-file">

        </div>
      </div>
    </div>
    <?php require './topbar/topbar.php'; closeConnection($conn);?>
  </body>
  </html>
<?php
} else header('Location: ./'.closeConnectionAndReturn($conn,"login.php?Atype=clienti")); //Effettua il login per accedere

?>

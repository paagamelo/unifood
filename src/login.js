//Controlla la correttezza dei dati di login
function checkParametri(password){
  var email = document.getElementById("email").value.trim();

  var errori =
            checkEmail(email)
          + checkPassword(password);
  return errori;
}

//Login
function login(atype){
  event.preventDefault();
  $("div.alert").remove();
  var password = document.getElementById("password");
  var errori = checkParametri(password.value);
  var form = document.getElementsByTagName("form")[0];
  addAccountType(form,atype);
  if(errori == "") formhash(form,password); //Invio form
  else $("header").before("<div class=\"alert alert-warning\" role=\"alert\"><h2>Errori nel form!</h2><ul>"+errori+"</ul></div>") //Mostro gli errori
}

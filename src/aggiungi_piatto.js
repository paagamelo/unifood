var ingredienti_selezionabili = [];
var ingredienti_categoria = [];
var ingredienti_selezionati = [];
var categorie_selezionate = [];
var categorie_selezionabili = [];
var cucina_corrente = 1;

function aggiungiNuovoPiatto(nome, cucina, categoria, prezzo, modificabile, ingredienti, id){
  modificabile = modificabile == 1 ? " modificabile" : "";
  ingredienti_piatti[id] = ingredienti;
  var display_ingredienti = getStringaIngredienti(calcolaIngredienti(ingredienti,cucina));
  var display_piatto =
                        "<li class=\"piatto"+modificabile+"\" id=\""+id+"\">"
                      + "<span class=\"nome-piatto\">" + nome + "</span>"
                      + "<span class=\"prezzo-piatto\">" + prezzo +"</span>"
                      + modifica
                      + meno
                      + "<span class=\"ingredienti-piatto\">" + display_ingredienti + "</span></li>";
  var idposizione = "c"+cucina+"c"+categoria;
  $("#"+idposizione+" ul.piatti").append(display_piatto);
  if($("#"+idposizione+" li.piatto").length == 1) $("#"+idposizione+" div.no-piatti").remove();
}

function ingrValidi(ingr){
  return ingr != null;
}

function checkDatiPiatto(cucina,categoria,nome,prezzo,modificabile,ingredienti){
  var errori = "";
  errori +=
              (checkNumber(cucina) ? "<li><span>Cucina</span> non valida</li>" : "")
            + (checkNumber(categoria) ? "<li><span>Categoria</span> non valida</li>" : "")
            + checkNomePiatto(nome)
            + checkPrezzo(prezzo)
            + checkIngredienti(ingredienti);

  return errori;
}

function getDati(){
  var cucina = $("#cucina").val();
  var categoria = $("#categoria").val();
  var nome = $("#nome-piatto").val();
  var prezzo = $("#costo").val();
  var modificabile = $("#modificabile:checked").length == 0 ? 0 : 1;
  var ingredienti = ingredienti_selezionati.filter(ingrValidi);
  var errori = checkDatiPiatto(cucina,categoria,nome,prezzo,modificabile,ingredienti);
  if(errori != ""){ //Dati inseriti non corretti, mostra modal
    $("#modal-file").append("<ul class=\"errori\">" + errori + "</ul>");
  } else {
    $.post("./aggiorna_piatti_fornitori.php",
    {
      request: "aggiungi",
      cucina: cucina,
      categoria: categoria,
      nome: nome,
      prezzo: prezzo,
      modificabile: modificabile,
      ingredienti: ingredienti
    },
    function(state){
      var messaggio = "";
      if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
      else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
      else if(state == "query-error") messaggio = "Qualcosa è andato storto con l'inserimento dei dati, si consiglia di ricaricare la pagina";
      else if(state == "yet-present") messaggio = "E' già presente un piatto con lo stesso nome";
      if(messaggio != "") $("#modal-file").append("<div class=\"errori\">"+messaggio+"</div>");
      else {
        aggiungiNuovoPiatto(nome, cucina, categoria, prezzo, modificabile, ingredienti, state);
        $(".modal").hide();
        $("#modal-file").children().remove();
      }
    });
  }
}

function toggleSalva(attiva){
  if(attiva){
    $("#aggiungi-piatto").removeClass("disabled");
    $("#aggiungi-piatto").removeAttr("disabled");
  } else {
    $("#aggiungi-piatto").addClass("disabled");
    $("#aggiungi-piatto").attr("disabled", "disabled");
  }
}

function toggleAggiungi(attiva){
  if(attiva){
    $("#aggiungi-ingrediente").removeClass("disabled");
    $("#aggiungi-ingrediente").removeAttr("disabled");
  } else {
    $("#aggiungi-ingrediente").addClass("disabled");
    $("#aggiungi-ingrediente").attr("disabled", "disabled");
  }
}

function parseIdIngr(id){
  var risultato = [];
  var c1 = id.indexOf("c");
  var i = id.indexOf("i");
  risultato["IdCategoria"] = Number(id.substring(c1+1,i));
  risultato["IdIngrediente"] = Number(id.substr(i+1));
  return risultato;
}

function resettaIngredienti(){
  ingredienti_selezionabili = [];
  ingredienti_categoria = [];
  ingredienti_selezionati = [];
  categorie_selezionate = [];
  categorie_selezionabili = [];
  $("#ingredienti-selezionati").empty();
  //toggleSalva(false);
}

function getIngredientiCategoria(idCat){
  var display = "";
  var ingredienti = ingredienti_categoria[idCat];
  for(ingr in ingredienti) {
    if(ingredienti_selezionabili.indexOf(Number(ingr)) != -1) display += ingredienti[ingr];
  }
  return display;
}

function caricaIngredienti(idCucina){
  resettaIngredienti();
  var ingredienti = ingredienti_cucine[idCucina];
  var selected = "selected";
  var display_categorie = "";
  var display_ingredienti = "";
  for(ingrediente in ingredienti){
    var ingr = ingredienti[ingrediente];
    var idCategoria = (ingr["StrutturaleYN"] == 0 ? 0 : ingr["IdCategoria"]);
    if(ingredienti_categoria[idCategoria] == undefined) ingredienti_categoria[idCategoria] = [];
    ingredienti_categoria[idCategoria][ingr["Id"]] = "<option class=\""+idCategoria+"\" value=\""+ingr["Id"]+"\" "+selected+" >"+ingr["Nome"]+"</option>";
    selected = "";
    ingredienti_selezionabili.push(ingr["Id"]);
    if(!categorie_selezionabili.includes(idCategoria)) categorie_selezionabili.push(idCategoria);
  }
  if(selected == "selected") return false;
  selected = "selected";
  for(categoria in categorie_selezionabili){
    var idCat = categorie_selezionabili[categoria];
    display_categorie += "<option value=\""+idCat+"\" "+selected+" >"+categorie_ingredienti[idCat]+"</option>"
    selected = "";
  }
  $("#categoria-ingrediente").append(display_categorie);
  $("#ingrediente").append(getIngredientiCategoria($("#categoria-ingrediente").val()));
  return true;
}

/*
function aggiornaDati(cucina, ingrediente, costo){
  $("option[value="+cucina+"]").remove();
  if($("#cucina option").length == 0){
    $("#modal-file").append("<div class=\"ok\">L'ingrediente viene ora fornito per ogni cucina!</div>");
    $("#aggiungi-ingrediente, #cucina, #costo").css("opacity","0.6");
    $("#aggiungi-ingrediente, #cucina, #costo").attr("disabled",true);
  }
  else $("#cucina option:first").attr("selected");
  var infoIngrediente = ingredienti_disponibili[ingrediente];
  var idCat = infoIngrediente["StrutturaleYN"] == 0 ? 0 : infoIngrediente["IdCategoria"];
  var id = "c"+cucina+"c"+idCat+"i"+ingrediente;
  var nome = infoIngrediente["Nome"];
  prezzi[id] = costo;
  var simbolo = (modifying ? meno : "");
  var display_costo  = (modifying ? "<label class =\"hidden\" for=\"costo"+id+"\">Costo aggiunta</label><input type=\"number\" id=\"costo"+id+"\" name=\"costo\" min=\"0\" max=\"99.99\"  step=\"0.1\" value=\""+costo+"\"></input>" : "<span class=\"costo-ingrediente\" >"+costo+"</span></li>");
  var display_ingrediente = "<li class =\"ingrediente\" id=\""+id+"\">"+simbolo+"<span class =\"nome-ingrediente\">" + nome + "</span>"+display_costo;
  if($("#c"+cucina+"c"+idCat+" li.ingrediente").length == 0) $("#c"+cucina+"c"+idCat+" .no-ingredienti").remove();
  $("#c"+cucina+"c"+idCat+" ul.ingredienti").append(display_ingrediente);
}
*/

$(document).ready(function(){

  //Aggiorna categorie
  $(document).on("change", "#cucina", function() {
    $("#categoria").empty();
    $("#categoria").append(getCategorieCucina($("#cucina").val()));
  });

  //Aggiorna ingredienti
  $(document).on("change", "#categoria-ingrediente", function() {
    $("#ingrediente").empty();
    $("#ingrediente").append(getIngredientiCategoria($("#categoria-ingrediente").val()));
  });

  //Passa a pagina ingredienti
  $(document).on("click", "#avanti", function() {
    event.preventDefault();
    var idCucina = $("#cucina").val();
    if(idCucina != cucina_corrente) {
      cucina_corrente = idCucina;
      $("#ingrediente").empty();
      $("#categoria-ingrediente").empty();
      caricaIngredienti(cucina_corrente);
      if(($("#categoria-ingrediente option").length == 0 || $("#ingrediente option").length == 0)) toggleAggiungi(false);
      else toggleAggiungi(true);
    }
    $("#modal-file h4").html("Aggiungi ingredienti");
    $(".form-base").hide();
    $(".form-ingredienti").show();
  });

  //Aggiungi ingrediente
  $(document).on("click", "#aggiungi-ingrediente", function() {
    event.preventDefault();
    var idIngrediente = $("#ingrediente").val();
    ingredienti_selezionati.push(Number(idIngrediente));
    ingredienti_selezionabili[ingredienti_selezionabili.indexOf(Number(idIngrediente))] = -1;
    var idCategoria = $("#categoria-ingrediente").val();
    var codice = "c"+idCategoria+"i"+idIngrediente;
    var display_ingrediente = "<li class=\"ingrediente\" id=\""+codice+"\">"
                              +meno+"<span class=\"nome-ingrediente\">"+$("#ingrediente option[value="+idIngrediente+"]").html()+"</span></li>";
    $("#ingredienti-selezionati").append(display_ingrediente);
    if(idCategoria != 0 || $("#ingrediente option").length == 1){ //E' un ingrediente strutturale oppure è l'ultimo ingrediente comune, non se ne possono selezionare altri
      categorie_selezionabili[categorie_selezionabili.indexOf(Number(idCategoria))] = null;
      $("#ingrediente").empty();
      $("#categoria-ingrediente option[value=\""+idCategoria+"\"]").remove();
      var idCat = $("#categoria-ingrediente").val();
      if(idCat != null && idCat != undefined) $("#ingrediente").append(getIngredientiCategoria(idCat)); //Carico i nuovi ingredienti
    } else $("#ingrediente option[value="+idIngrediente+"]").remove(); //E' un ingrediente comune, se ne possono selezionare altri
    //if($("#ingredienti-selezionati li.ingrediente").length == 1) toggleSalva(true);
    if($("#categoria-ingrediente option").length == 0 || $("#ingrediente option").length == 0) toggleAggiungi(false);
  });


  //Torna al form-base
  $(document).on("click", "#indietro", function() {
    event.preventDefault();
    $("#modal-file h4").html("Dati piatto");
    $(".form-ingredienti").hide();
    $(".form-base").show();
  });

  //Aggiungi piatto
  $(document).on("click", "#aggiungi-piatto", function() {
    event.preventDefault();
    $("#modal-file .errori").remove();
    getDati();
  });

  //Rimuovi ingrediente
  $(document).on("touchend mouseup", ".ingrediente .minus", function() {
    event.preventDefault();
    var id = parseIdIngr($(this).parent()[0]["id"]);
    var idingrediente = id["IdIngrediente"];
    var idcategoria = id["IdCategoria"];
    ingredienti_selezionati[ingredienti_selezionati.indexOf(idingrediente)] = null;
    ingredienti_selezionabili.push(idingrediente);
    if(categorie_selezionabili.indexOf(idcategoria) == -1){
      categorie_selezionabili.push(idcategoria);
      var display_cat = "<option value=\""+idcategoria+"\" >"+categorie_ingredienti[idcategoria]+"</option>";
      $("#categoria-ingrediente").append(display_cat);
    }
    if($("#categoria-ingrediente").val() == idcategoria){
      $("#ingrediente").empty();
      $("#ingrediente").append(getIngredientiCategoria(idcategoria));
    }
    $(this).parent().remove();
    //if($("#ingredienti-selezionati li.ingrediente").length == 0) toggleSalva(false);
    if($("#aggiungi-ingrediente.disabled").length == 1) toggleAggiungi(true);
  });
});

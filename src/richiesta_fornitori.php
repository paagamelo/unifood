<?php
header('Content-Type: application/json');

if(isset($_GET['request'])) {
  require 'db_connect.php';
  require 'common_queries.php';
  if($conn->connect_error) {
    $output["errore"] = "errore";
    die(json_encode($output));
  }

  $rows_per_page = 2;
  $output = array();

  switch($_GET['request']) {
    case 'num_pages': //?request=num_pages&cucina={..}
    {
      if(isset($_GET['cucina']) && $_GET['cucina'] != "all") {
        $idcucina = $_GET['cucina'];
        if(
             !checkCucinaExists($conn, $idcucina)
          || !($stmt = $conn->prepare("SELECT COUNT(*) FROM FORNITURE WHERE IdCucina = ?"))
          || !$stmt->bind_param("i", $idcucina))
        {
          $output["errore"] = "errore";
          print json_encode($output);
          break;
        }
      } else {
        if(!($stmt = $conn->prepare("SELECT COUNT(*) FROM FORNITORI"))) {
          $output["errore"] = "errore";
          print json_encode($output);
          break;
        }
      }
      if(!$stmt->execute()) {
        $output["errore"] = "errore";
      } else {
        $stmt->bind_result($rows);
  			$stmt->fetch();
        $stmt->close();
        $num_pages = ceil($rows / $rows_per_page);
        $output['num_pages'] = $num_pages;
      }
      print json_encode($output);
      break;
    }

    case 'page': //?request=page&cucina={..}&page={..}
    {
      if(isset($_GET['page']) && $_GET['page'] >= 0) {
        $start_row = $_GET['page'] * $rows_per_page;

        if(isset($_GET['cucina']) && $_GET['cucina'] != "all") {
          $idcucina = $_GET['cucina'];
          if(
               !checkCucinaExists($conn, $idcucina)
            || !($stmt = $conn->prepare(
                  "SELECT * FROM FORNITORI, FORNITURE
                  WHERE FORNITORI.Id = FORNITURE.IdFornitore
                  AND FORNITURE.IdCucina = ?
                  ORDER BY FORNITORI.Id
                  LIMIT ?, ?"))
            || !$stmt->bind_param("iii", $idcucina, $start_row, $rows_per_page))
          {
            $output["errore"] = "errore";
            print json_encode($output);
            break;
          }
        } else {
          if(
               !($stmt = $conn->prepare("SELECT * FROM FORNITORI ORDER BY Id LIMIT ?, ?"))
            || !$stmt->bind_param("ii", $start_row, $rows_per_page))
          {
            $output["errore"] = "errore";
            print json_encode($output);
            break;
          }
        }
        if(!$stmt->execute()) {
          $output["errore"] = "errore";
        } else {
          $result = $stmt->get_result();
    			while($row = $result->fetch_assoc()) {
    				$output[] = $row;
    			}
    			$stmt->close();
        }
        print json_encode($output);
        break;
      }
    }
  }
  closeConnection($conn);
}

?>

var page = -1;
var showing = false;
var errore = false;

function displayAlert(type, message) {
  var prestring = "";
  switch(type) {
    case 'info': prestring = "Attenzione! "; break;
    case 'success': prestring = "Successo! "; break;
    case 'error': prestring = 'Errore! '; break;
  }
  $(".modal").css("display", "none");
  $('.alert.' + type + '').remove();
  $(".main").prepend('<div class="alert ' + type + '"><span class="closebtn">&times;</span>'
    + '<strong>' + prestring + '</strong>' + message + '</div>');
}

function error() {
  errore = true;
  $(".breadcrumb").after("<p>Impossibile caricare gli ordini</p>");
  $("#ordini-in-consegna, #ordini-conclusi").remove();
}

function ordiniToHtml(ordini) {
  var html = "";
  for(id in ordini) {
    ordine = ordini[id];

    var html_piatti = '<ul class="piatti">';
    for(piatto in ordine['piatti']) {
      piatto = ordine['piatti'][piatto];
      html_piatti += '<li><div class="piatto">' + piatto["Nome"] + " x" + piatto['Quantita'] + '</div>'
                    + '<ul class="modifiche">';
      for(modifica in piatto['modifiche']) {
        modifica = piatto['modifiche'][modifica];
        html_piatti += '<li><span class="isaddition ' + ((modifica['AggiuntaYN'] == '1') ? 'yes">+</span>' : 'no">-</span>') + modifica["Nome"] + '</li>';
      }
      html_piatti += '</ul>';
    }
    html_piatti += '</ul>';

    html += '<tr data-id="' + ordine["Id"] + '">'
              + '<td>' + ordine['FNome'] + '</td>'
              + '<td>' + html_piatti + '</td>' // piatti
              + '<td>' + (ordine['Note'] == null ? "/" : ordine["Note"]) + '</td>'
              + '<td>' + ordine['Totale'] + '</td>' //totale
              + '<td>' + ((ordine['PagatoYN'] == '1') ? "Si" : "No") + '</td>'
              + '<td>' + ordine['CNome'] + " " + ordine['CCognome'] + '</td>' // cliente
              + '<td>' + ordine['OraConsegna'] + '</td>'
              + '<td>' + ordine['PianoConsegna'] + '</td>';

    if(ordine['Stato'] == 'Consegnato') {
      html += '<td>' + ordine['Data'] + '</td>';
    } else {
      html += '<td><button type="button">Consegna</button></td>';
    }

    html += '</tr>';
  }
  return html;
}

var onpageload = true;

function loadNuoviOrdini() {
  $.getJSON("richiesta_ordini_fattorini.php?request=nuovi", function(data) {
    if(data["errore"] === "errore") {
      error();
    } else if(!errore) {
      var incorso = $("#ordini-in-consegna tr").length - 1;

      if(onpageload || data.length < incorso) {
        $("#ordini-in-consegna table tbody").html(ordiniToHtml(data));
      } else if(data.length > incorso) {
        displayAlert('info', 'Nuovi ordini arrivati');
        $("#ordini-in-consegna table tbody").hide();
        $("#ordini-in-consegna table tbody").html(ordiniToHtml(data)).fadeIn('slow');
      }

      onpageload = false;
      if(data.length == 0) {
        $("#ordini-in-consegna table").css("display", "none");
        if($("#ordini-in-consegna .placeholder").length == 0) {
          $("#ordini-in-consegna table").after('<p class="placeholder">Nessun ordine da mostrare</p>');
        }
      } else {
        $("#ordini-in-consegna .placeholder").remove();
        $("#ordini-in-consegna table").css("display", "table");
      }
    }
  });
}

function checkMoreVecchiOrdini() {
  contesto = "#ordini-conclusi";
  $.getJSON("richiesta_ordini_fattorini.php?request=more&page=" + page,
  function(more) {
    if(more["errore"] === "errore") {
      error();
    } else if(!errore) {
      if(more['more'] === 'true') {
        $(contesto).find(".placeholder").remove();
        if($(contesto).find(".more").length == 0) {
          $(contesto).find("table").after('<a class="more">Visualizza di più ></a>');
        }
      } else {
        $(contesto).find(".more").remove();
        if(!showing && $(contesto).find(".placeholder").length == 0) {
          $(contesto).find("table").after('<p class="placeholder">Nessun ordine da mostrare</p>');
          //$(contesto).find("header").css("display", "none");
        }
      }
    }
  });
}

function loadVecchiOrdini() {
  var url="";
  $.getJSON("richiesta_ordini_fattorini.php?request=page&page=" + page,
  function(data) {
    if(data["errore"] === "errore") {
      error();
    } else if(!errore) {
      $('#ordini-conclusi tbody').append(ordiniToHtml(data));
    }
  });
}

function monitoraVecchiOrdini() {
  contesto = "#ordini-conclusi";
  if(showing) {
    $.getJSON("richiesta_ordini_fattorini.php?request=topage&page=" + page,
    function(data) {
      if(data["errore"] === "errore") {
        error();
      } else if(!errore) {
        $(contesto).find("tbody").html(ordiniToHtml(data));
      }
    });
  }
  checkMoreVecchiOrdini();
}

$(document).ready(function() {

  $("#ordini-conclusi table").css("display", "none");

  loadNuoviOrdini();
  setInterval(function() { loadNuoviOrdini(); }, 5000);

  monitoraVecchiOrdini()
  setInterval(function() { monitoraVecchiOrdini(); }, 30000);

  setInterval(function() { $(".alert.info, .alert.success").remove(); }, 30000);

  $(document).on("click", "#ordini-conclusi .more", function() {
    page++;
    showing = true;

    loadVecchiOrdini();
    checkMoreVecchiOrdini();

    $("#ordini-conclusi table").css("display", "table");
    if($("#ordini-conclusi .less").length == 0) {
      $("#ordini-conclusi table").after('<a class="less">Nascondi</a>');
    }
  });

  $(document).on("click", "#ordini-conclusi .less", function() {
    page = -1;
    showing = false;

    $("#ordini-conclusi table").css("display", "none");
    $("#ordini-conclusi tbody").html("");

    checkMoreVecchiOrdini();
    $(this).remove();
  });

  var idordine;

  $(document).on("click", "td>button", function() {
    idordine = $(this).parents("tr").attr('data-id');
    $.post("./richiesta_ordine_consegnato.php",
    {
      idordine: idordine
    },
    function(state) {
      if(state === 'success') {
        displayAlert('success', 'Ordine consegnato con successo');
        loadNuoviOrdini();
        monitoraVecchiOrdini();
      } else {
        displayAlert('error', 'Qualcosa è andato storto')
      }
    });
  });

  /*
  $(document).on("click", "td>button", function() {
    $(".modal").css("display", "block");
    idordine = $(this).parents("tr").attr('data-id');
  })

  //Chiudi modal
  $(document).on("click", ".close", function(){
    $(".modal").hide();
    $("#modal-file").children().remove();
  });

  $(document).on("click", '.modal input[type="submit"]', function() {
    event.preventDefault();
    var idfattorino = $("input[name='fattorino']:checked").val();
    data = {
      idfattorino: idfattorino,
      idordine: idordine
    }
    console.log(data);
    $.post("./richiesta_ordine_preparato.php",
    data,
    function(state) {
      if(state === 'success') {
        displayAlert('success', 'Ordine preparato con successo');
        loadNuoviOrdini();
      } else {
        displayAlert('error', 'Qualcosa è andato storto')
      }
    });
  });
  */

  $(document).on("click", ".alert .closebtn", function() {
    $(this).parent(".alert").remove();
  })

});

//Controlla la correttezza dei dati di registrazione
function checkParametri(atype,pwd,c_pwd){

  var email = document.getElementById("email").value.trim();
  var username = document.getElementById("username").value.trim();
  if(atype != "fornitori") {
    var nome = document.getElementById("nome").value.trim();
    var cognome = document.getElementById("cognome").value.trim();
    var telefono = document.getElementById("telefono").value.trim();
  }

  var errori =
            (atype == "fornitori" ? "" : checkNome(nome))
          + (atype == "fornitori" ? "" : checkCognome(cognome))
          + checkEmail(email)
          + (atype == "clienti" ? checkTelefono(telefono,false) : atype == "fattorini" ? checkTelefono(telefono,true) : "")
          + checkUsername(username)
          + checkPassword(pwd)
          + checkConfirmPassword(c_pwd, pwd);
  return errori;
}

//Controlla la correttezza dei dati relativi al ristorante
function checkParametriRistorante(){

  var nome = document.getElementById("nome_ristorante").value.trim();
  var telefono = document.getElementById("telefono_ristorante").value.trim();
  var indirizzo = document.getElementById("indirizzo").value.trim();
  var speseConsegna = document.getElementById("speseConsegna").value.trim().replace(",",".");
  var daOra = document.getElementById("orarioApertura").value.trim();
  var aOra = document.getElementById("orarioChiusura").value.trim();

  var edo = checkOrario(daOra).replace("orario","orario di apertura");
  var eao = checkOrario(aOra).replace("orario","orario di chiusura");

  var errori =
            checkRistorante(nome)
          + checkIndirizzo(indirizzo)
          + checkTelefono(telefono)
          + checkSpeseConsegna(speseConsegna)
          + edo
          + eao
          + ((edo+eao) == "" ? checkRangeOrario(daOra,aOra) : ""); //Esegue il controllo del range dell'orario solo se sono stati passati orari validi

  return errori;
}

//Registra
function register(atype){
  event.preventDefault();
  $("div.alert").remove();
  var form = document.getElementsByTagName("form")[0];
  var pwd = document.getElementById("password");
  var c_pwd = document.getElementById("c_password");
  var errori = checkParametri(atype,pwd.value,c_pwd.value);
  addAccountType(form,atype);
  if(errori == ""){
    if(atype == "fornitori"){ //Passa alla pagina "Il tuo ristorante" per il fornitore
      $(".base").hide();
      $(".fornitori").removeClass("hidden");
      $("h1").html("Il tuo ristorante");
      $(".footer").hide();
      $(".invia").attr("onclick","registra_fornitori();");
    } else register_formhash(form,pwd,c_pwd); //Invio form
  }
  else $("header").before("<div class=\"alert alert-warning\" role=\"alert\"><h2>Errori nel form!</h2><ul>"+errori+"</ul></div>") //Mostro gli errori
}
//Ritorna al form principale
function indietro(){
  event.preventDefault();
  $(".fornitori").addClass("hidden");
  $(".base").show();
  $("h1").html("Registazione Fornitori");
  $(".footer").show();
  $(".invia").attr("onclick","register('fornitori');");

}

//Registra i fornitori
function registra_fornitori(){
  event.preventDefault();
  $("div.alert").remove();
  var errori = checkParametriRistorante();
  if(errori == ""){
    var form = document.getElementsByTagName("form")[0];
    var pwd = document.getElementById("password");
    var c_pwd = document.getElementById("c_password");
    register_formhash(form,pwd,c_pwd); //Invio form
  } else $("header").before("<div class=\"alert alert-warning\" role=\"alert\"><h2>Errori nel form!</h2><ul>"+errori+"</ul></div>") //Mostro gli errori
}

//TRUE ERRORE
//L'ggetto non deve essere vuoto
function checkEmpty(obj){
  return obj == null || obj == undefined || obj == "";
}
//La stringa deve avere solo caratteri
function checkOnlyCharacters(string){
  var regex = /[0-9]/;
  return checkEmpty(string) || regex.test(string);
}
//La stringa deve rientrare nel range di caratteri (min-max)
function checkDimension(string, min, max){
  return checkEmpty(string) || string.length < min || string.length > max;
}
//La stringa non deve contenere spazi
function checkNoSpace(string, min, max){
  var regex = / /;
  return checkEmpty(string) || regex.test(string);
}
//Deve essere un numero
function checkNumber(number){
  return checkEmpty(number) || isNaN(number);
}
//Deve essere un numero dentro il range (min-max)
function checkRangeNumber(number, min, max){
  return checkNumber(number) || number < min || number > max;
}
//Deve contenere da 1 a 15 caratteri, senza cifre
function checkNome(nome){
  var errore = "";
  if(checkOnlyCharacters(nome) || checkDimension(nome,1,15)) errore += "<li>Il <span>nome</span> deve essere composto da almeno un carattere fino ad un massimo di 15 (Non sono ammesse cifre)</li>";
  return errore;
}
//Deve contenere da 1 a 15 caratteri, senza cifre
function checkCognome(cognome){
  var errore = "";
  if(checkOnlyCharacters(cognome) || checkDimension(cognome,1,15)) errore += "<li>Il <span>cognome</span> deve essere composto da almeno un carattere fino ad un massimo di 15 (Non sono ammesse cifre)</li>";
  return errore;
}
//Deve essere un email valida, fino ad un massimo di 30 caratteri
function checkEmail(email){
  var errore = "";
  var regex = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
  if(checkDimension(email,1,30) || !regex.test(email)) errore+= "<li>L'<span>email</span> inserita non è valida (lunghezza massima 30 caratteri)</li>";
  return errore;
}
//Deve contenere esattamente 10 cifre (non caratteri)
//Non può essere null se 'obbligatorio è uguale a null'
function checkTelefono(telefono, obbligatorio){
  var errore = "";
  if(obbligatorio){ //Non può essere null
    if(checkEmpty(telefono) || isNaN(telefono) || telefono.length != 10) errore += "<li>Il numero di <span>telefono</span> deve essere di esattamente 10 cifre e deve contenere solo cifre</li>";
  }else { //Può essere null
    if((!checkEmpty(telefono)) && (isNaN(telefono) || telefono.length != 10)) errore += "<li>Il numero di <span>telefono</span> deve essere di esattamente 10 cifre e deve contenere solo cifre</li>";
  }
  return errore;
}
//Deve contenere da 1 a 15 caratteri, senza spazi
function checkUsername(username){
  var errore = "";
  if(checkDimension(username,1,15) || checkNoSpace(username)) errore += "<li>L'<span>username</span> deve essere composta da almeno un carattere fino ad un massimo di 15 (Non sono ammessi spazi)</li>";
  return errore;
}
//Deve contenere da 8 a 15 caratteri, senza spazi
function checkPassword(password){
  var errore = "";
  if(checkDimension(password,8,15) || checkNoSpace(password)) errore += "<li>La <span>password</span> deve essere composta da almeno un carattere fino ad un massimo di 15 (Non sono ammessi spazi)</li>";
  return errore;
}
//La conferma della password deve contenere da 8 a 15 caratteri (senza spazi) e deve essere uguale alla precedente password inserita
function checkConfirmPassword(c_pwd, pwd){
  var errore = checkPassword(c_pwd);
  errore = errore.replace("password", "conferma della password");
  if(c_pwd != pwd) errore += "<li>La <span>conferma della password</span> e la <span>password</span> devono essere uguali</li>";
  return errore;
}
//L'anno deve essere un numero compreso tra 19 e 30
function checkAnno(anno){
  var errore = "";
  if(checkRangeNumber(anno,19,30)) errore += "<li>Inserire un numero di <span>anno</span> valido (19-30)</li>";
  return errore;
}
//Il mese deve essere un numero compreso tra 1 e 12
function checkMese(mese){
  var errore = "";
  if(checkRangeNumber(mese,1,12)) errore += "<li>Inserire un numero di <span>mese</span> valido (1-12)</li>";
  return errore;
}
//Il numero della carta deve contenere solo cifre, minimo 10
function checkNumeroCarta(numero){
  var errore = "";
  if(checkRangeNumber(numero,10,Infinity)) errore += "<li>Il <span>numero di carta</span> deve contenere solo cifre (almeno 10)</li>";
  return errore;
}
//L'intestatario deve contenere almeno 2 stringhe separate da uno spazio
function checkIntestatario(intestatario){
  var errore = "";
  var regex = /[a-zA-Z][ ][a-zA-Z]/;
  if(checkEmpty(intestatario) || !regex.test(intestatario)) errore += "<li>Inserire un <span>nome e cognome</span> valido</li>";
  return errore;
}
//Controlla che sia un indirizzo civico, comprensivo di numero
function checkIndirizzo(indirizzo){
  var errore = "";
  var regex = /[A-Za-z]([,]{1}|[ ])[ ]*[0-9]/;
  if(checkEmpty(indirizzo) || !regex.test(indirizzo)) errore += "<li>Inserire un <span>indirizzo civico</span> valido (compreso il numero)</li>";
  return errore;
}
//Controlla che sia un numero positivo
function checkSpeseConsegna(spese){
  var errore = "";
  if(checkNumber(spese) || spese < 0 || spese > 99.99) errore += "<li>Inserire un numero nel range (0-99,99) per le <span>spese di consegna</span></li>";
  return errore;
}
//Controlla che sia un orario valido
function checkOrario(orario){
  var errore = "";
  var regex = /([0-1][0-9]|[2][0-3])[:][0-5][0-9]/;
  if(!checkEmpty(orario) && orario.length == 4) orario = 0+orario;
  if(checkEmpty(orario) || !regex.test(orario) || orario.length != 5) errore += "<li>Inserire un <span>orario</span> valido (hh:mm)</li>";
  return errore;
}
//Controlla che l'orario 'da', preceda quello 'a'
function checkRangeOrario(da,a){
  var errore = "";
  if(!checkEmpty(da) && da.length == 4) da = 0+da;
  if(!checkEmpty(a) && a.length == 4) a = 0+a;
  if(da >= a) errore +="<li>L'<span>orario di apertura</span> deve precedere quello di <span>chiusura</span></li>";
  return errore;
}
//Controlla che abbia almeno 1 carattere fino ad un massimo di 15
function checkRistorante(nome){
  var errore = "";
  if(checkDimension(nome,1,15)) errore += "<li>Il <span>nome</span> deve essere composto da almeno un carattere fino ad un massimo di 15</li>";
  return errore;
}
//Controlla che sia null o un numero maggiore di 0
function checkMaxOrdini(maxOrdini){
  var errore = "";
  if(!(checkEmpty(maxOrdini)) && checkRangeNumber(maxOrdini,1,Infinity)) errore += "<li>Il <span>numero di ordini</span> accettabili contemporanemente deve essere vuoto o un numero maggiore di 0</li>";
  return errore;
}
//Controlla che siano vuote o con massimo 100 caratteri
function checkNote(note){
  var errore = "";
  if(!(checkEmpty(note)) && checkDimension(note,1,100)) errore += "<li>Le <span>note</span> possono contenere massimo 100 caratteri</li>";
  return errore;
}
//Controlla che i giorni di chiusura siano massimo 4 e siano tutti numeri nel range (1-5)
function checkGiorni(giorni){
  var errore = "";
  var err = false;
  for(giorno in giorni) if(checkRangeNumber(giorni[giorno],1,5)) err = true;
  if(err || giorni.length > 4) errore += "<li>I <span>giorni di chiusura</span> possono essere al massimo 4</li>";
  return errore;
}
//Controlla che il prezzo sia un numero compreso tra 0 e 99.99
function checkPrezzo(prezzo){
  var errore = "";
  if(checkRangeNumber(prezzo,0.00,99.99)) errore += "<li>Inserire un numero nel range (0-99,99) per il <span>prezzo degli ingredienti</span></li>";
  return errore;
}
//Controlla che il nome di categoria abbia un numero di caratteri nel range (1-25)
function checkNomeCategoria(categoria){
  var errore = "";
  if(checkDimension(categoria,1,25)) errore += "<li>Il <span>nome della categoria</span> deve contenere almeno un carattere fino ad un massimo di 25</li>";
  return errore;
}
//Controlla che il nome di categoria abbia un numero di caratteri nel range (1-25)
function checkNomeCucina(cucina){
  var errore = "";
  if(checkDimension(cucina,1,25)) errore += "<li>Il <span>nome della cucina</span> deve contenere almeno un carattere fino ad un massimo di 25</li>";
  return errore;
}
//Controlla che il nome dell'ingrediente abbia un numero di caratteri nel range (1-25)
function checkNomeIngrediente(ingrediente){
  var errore = "";
  if(checkDimension(ingrediente,1,25)) errore += "<li>Il <span>nome dell'ingrediente</span> deve contenere almeno un carattere fino ad un massimo di 25</li>";
  return errore;
}
//Controlla che l'id della categoria sia valido
function checkIdCategoria(id){
  var errore = "";
  if(checkNumber(id)) errore += "<li>La <span>categoria</span> selezionata non è valida, è consigliato ricaricare la pagina</li>";
  return errore;
}

//Controlla che il nome del piatto abbia un numero di caratteri nel range (1-25)
function checkNomePiatto(nome){
  var errore = "";
  if(checkDimension(nome,1,25)) errore += "<li>Il <span>nome del piatto</span> deve contenere almeno un carattere fino ad un massimo di 25</li>";
  return errore;
}
//Controlla che sia un numero
function checkIngrValido(ingr){
  return !checkNumber(ingr);
}

//Controlla che ci sia almeno un ingrediente e che i valori siano validi
function checkIngredienti(ingredienti){
  var errore = "";
  var error = false;
  //if(ingredienti.length == 0) error = true;
  if(ingredienti.length != ingredienti.filter(checkIngrValido).length) error = true;
  if(error) errore += "<li><span>Ingredienti non validi</span></li>";
  return errore;
}
//Controlla che l'id del datore di lavoro sia valido
function checkDatore(id){
  var errore = "";
  if(checkNumber(id)) errore += "<li>Il <span>datore di lavoro</span> selezionato non è valido, è consigliato ricaricare la pagina</li>";
  return errore;
}
//Controlla che l'orario non sia precedente all'orario attuale
function checkOrarioPrecedente(orario){
  var errore = "";
  var data = new Date();
  var ore = data.getHours();
  var minuti = data.getMinutes();
  if(ore < 10) ore = "0"+ore;
  if(minuti < 10) minuti = "0"+minuti;
  errore = checkRangeOrario(ore+":"+minuti,orario);
  if(errore != "") errore = "<li>L'<span>orario</span> di consegna dell'ordine non può essere precedente all'orario attuale</li>";
  return errore;
}

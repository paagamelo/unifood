function rightAlignNotifiche() {
  var carrelloWidth = 0;
  if($("#carrello-topbar").css("display") != "none") {
    carrelloWidth = $("#carrello-topbar").width();
  }
  $("#notifiche-content").css("right", carrelloWidth);
}
function rightAlignOrdini() {
  var carrelloWidth = 0;
  if($("#carrello-topbar").css("display") != "none") {
    carrelloWidth = $("#carrello-topbar").width();
  }
  $("#ordini-content").css("right", $("#notifiche").width() + carrelloWidth);
}
$(document).ready(function() {
  /* opening sidebar */
  $("#menu").on("click", function() {
    $("#sidebar").toggleClass("active");
    $("#menu").toggleClass("slide");
    /* setting correct sidebar header height (just better visual effect) */
    var deltap = ($("#topbar").height() - $("#sidebar-header").height()) / 2;
    $("#sidebar-header").css("padding-top", deltap);
    $("#sidebar-header").css("padding-bottom", deltap);
  });
  /* close open dropdowns before showing the clicked one */
  $(".dropbtn").on("click touchstart", function() {
    $(".dropdown-content").not($(this).next()).hide();
  });
  /* opening dropdown for "carrello" */
  $("#carrello-btn").on("click", function(e) {
    e.stopPropagation();
    $("#carrello-content").toggle();
  });
  /* opening dropdown for "ordini" */
  $("#ordini-btn").on("click", function(e) {
    e.stopPropagation();
    /* aligning dropdown content with button (just better visual effect) */
    if($("#topbar-file .dropdown-content").css("max-width") == "300px") { //desktop
      rightAlignOrdini();
    } else {
      $("#ordini-content").css("right", "0");
    }
    /* actually showing it */
    $("#ordini-content").toggle();
  });
  /* opening dropdown for "notifiche" */
  $("#notifiche-btn").on("click", function(e) {
    e.stopPropagation();
    /* aligning dropdown content with button (just better visual effect) */
    if($("#topbar-file .dropdown-content").css("max-width") == "300px") { //desktop
      rightAlignNotifiche();
    } else {
      $("#notifiche-content").css("right", "0");
    }
    $("#notifiche-content").toggle();
  });
  /* added for mobile - which fires both "click" and "touch" events - */
  $(".dropbtn").on("touchstart", function(e) {
     e.stopPropagation();
  });
  /* closing dropdown when clicking anywhere else but not inside of it*/
  $(document).on("click touchstart", function(e) {
    if(!($(e.target).closest(".dropdown-content").length > 0)) {
      $(".dropdown-content").hide();
    }
  });
  /* but not when clicking inside of it */
  $(".dropdown-content").on("click touchstart", function(e) {
    //e.stopPropagation();
  });
  /* window resize listener to correctly re-align dropdown content */
  $(window).resize(function() {
    if($(".dropdown-content").is(":visible")) { /* only if something is visible re-align it */
      if($("#topbar-file .dropdown-content").css("max-width") != "300px") { //desktop -> mobile
        $(".dropdown-content").css("right", "0"); //remove right
      } else { //mobile -> desktop
        if($(".dropdown-content:visible").is("#notifiche-content")) {
          rightAlignNotifiche();
        }
        if($(".dropdown-content:visible").is("#ordini-content")) {
          rightAlignOrdini();
        }
      }
    }
  });
});

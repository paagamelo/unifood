<!-- HOW TO INCLUDE: require './path/to/topbar.php'; right after your <body> tag, i.e. where
     you want the topbar to load -->

<!-- REQUIRED: JQuery, Awesome Font (for icons), topbar_style.css, $logged boolean, $conn with db -->
<!-- Note that this file has a <script> at his bottom that requires JQuery, so you should
     link that before including this. Didn't find another way to correctly link everything YET -->

<!-- NOTE THAT: This topbar (and sidebar) are fixed and *will* cover your content,
     to avoid this you need to add a top-margin -->
<?php
require 'topbar_functions.php';
if($logged) {
  $idutente = $_SESSION['user_id'];
  $atype = $_SESSION['atype'];
  if($atype != 'admin') {
    getNotifiche($conn, $idutente, $notifiche, $atype);
    getOrdini($conn, $idutente, $ordini, $atype);
    if(!empty($notifiche)) {
      $newnotifications = !empty(array_filter($notifiche, function($n) { return !$n["LettaYN"]; }));
    } else {
      $newnotifications = false;
    }
  }
}
?>
<div id="topbar-file">
  <!-- TOPBAR -->
  <nav id="topbar">
    <?php if($logged) { //display menu only if logged ?>
    <!-- Menu -->
    <a id="menu" class="topbar-item" aria-label="Menu">
      <i class="fas fa-bars fa-fw" aria-hidden="true"></i>
      <span class="btn-text">Menu</span>
    </a>
    <?php } ?>
    <!-- Carrello -->
    <?php if(!$logged || $atype == 'clienti') { //display carrello only if not logged or cliente ?>
    <div id="carrello-topbar" class="topbar-item dropdown">
      <button id="carrello-btn" class="dropbtn" aria-label="Carrello">
        <i aria-hidden="true" class="fas fa-shopping-cart fa-fw"></i>
        <span id="prezzo" class="total">
          <span>€</span>
          <span class="value">0.00</span>
        </span>
      </button>
      <div id="carrello-content" class="dropdown-content carrello">
        <div class="carrello-row">
        <h3>Riepilogo ordine</h3>
        <button type="button" class="modify">Modifica</button>
      </div>
        <ul>
          <li class="content-placeholder">Il carrello è vuoto</li>
        </ul>
        <div class="total">
          <span class="value">0.00</span>
          <span class="currency">€</span>
        </div>
        <button type="button" class="checkout">Vai alla cassa</button>
      </div>
    </div>
    <?php } ?>
    <!-- Accedi -->
    <?php if(!$logged) { ?>
    <a id="registrati" class="topbar-item" href="./registrazione.php?Atype=clienti">
      Registrati
    </a>
    <a id="accedi" class="topbar-item" href="./login.php?Atype=clienti">
      Accedi
    </a>
  <?php } else if($atype != 'admin') { ?>
    <!-- Notifiche -->
    <div id="notifiche" class="topbar-item dropdown">
      <button id="notifiche-btn" class="dropbtn" aria-label="Notifiche">
        <i aria-hidden="true" class="fas fa-bell fa-fw"></i>
        <?php if($newnotifications) {
          echo '<span class="badge"></span>';
        } else {
          echo '<span class="badge" style="display: none"></span>';
        }?>
        <span class="btn-text">Notifiche</span>
      </button>
      <div id="notifiche-content" class="dropdown-content">
        <div>Notifiche</div>
        <ul>
        <?php
          if(isset($notifiche["errore"]) && $notifiche["errore"]) {
            echo '<li class="content-placeholder">Impossibile caricare le notifiche</li>';
          } else if(empty($notifiche)) {
            echo '<li class="content-placeholder">Nessuna nuova notifica</li>';
          } else {
            foreach($notifiche as $key => $value) {
              echo '<li data-id="'.$notifiche[$key]["Id"].'"'
                .(($notifiche[$key]["LettaYN"] == '1') ? '>' : ' class="new">').$notifiche[$key]['Testo']
                .(($notifiche[$key]["LettaYN"] == '1') ? '' : '<div class="circle"></div>').'</li>';
            }
          }
        ?>
      </ul>
      </div>
    </div>
    <!-- Ordini -->
    <?php
      if($atype == 'fattorini') {
        $title = 'Consegne';
      } else {
        $title = 'Ordini';
      }
    ?>
    <div id="ordini-topbar" class="topbar-item dropdown">
      <button id="ordini-btn" class="dropbtn" aria-label="Ordini">
        <i aria-hidden="true" class="fas fa-shopping-basket fa-fw"></i>
        <!--<i class="material-icons">shopping_basket</i>-->
        <span class="btn-text"><?php echo $title; ?></span>
      </button>
      <div id="ordini-content" class="dropdown-content">
        <?php
        if($atype == 'fattorini') {
          echo '<div>Nuove consegne</div><ul>';
        } else {
          echo '<div>Nuovi ordini</div><ul>';
        }
        if(isset($ordini["errore"]) && $ordini["errore"]) {
          echo '<li class="content-placeholder">Impossibile caricare gli ordini</li>';
        } else if(empty($ordini)) {
          echo '<li class="content-placeholder">Nessun nuovo ordine</li>';
        } else {
          $html = "";
          foreach($ordini as $idordine => $ordine) {
            $html .= '<li><div class="header">';
            if($atype == 'clienti'){
              $html .= '<span class="fornitore">Fornitore: '.$ordine['fornitore']."</span>";
            }
            $html .= '<span class="orario">Orario: '.$ordine['OraConsegna'].'</span>'
              .'<span class="piano">Piano: '.$ordine['PianoConsegna'].'</span></div><span class="ordine">';
            $first = true;
            foreach($ordine["piatti"] as $idpiatto => $piatto) {
              if(!$first) $html .= ", ";
              $first = false;
              $html .= $piatto["Nome"];
              $first2 = true;
              foreach($piatto["modifiche"] as $key => $modifica) {
                if($first2) $html .= " (";
                else $html .= ", ";
                $first2 = false;
                $html .= ($modifica["AggiuntaYN"] ? "+ " : "- ") . $modifica["Nome"];
              }
              if(!empty($piatto["modifiche"])) {
                $html .= ")";
              }
              $html .= " x" . $piatto["Quantita"];
            }
            $html .= '</span>';
            if($atype == 'clienti') {
              $html .= '<span class="stato">'.$ordine["Stato"]."</span>";
            }
            if($atype == 'fattorini') {
              $html .= '<span class="cliente">'.$ordine["Nome"]." ".$ordine["Cognome"].'</span>';
            }
            $html .= "</li>";
          }
          echo $html . "";
          switch($atype) {
            case 'clienti': echo '<li><a href="./clienti_ordini.php">Tutti gli ordini ></a></li>'; break;
            case 'fornitori': echo '<li><a href="./fornitori_ordini.php">Gestisci ordini ></a></li>'; break;
            case 'fattorini': echo '<li><a href="./fattorini_ordini.php">Gestisci consegne ></a></li>'; break;
          }
        }
        ?>
      </ul>
      </div>
    </div>
    <?php } ?>
  </nav>
  <?php if($logged) { ?>
  <!-- SIDEBAR -->
  <section id="sidebar">
    <div id="sidebar-header">
      <h3>Menu</h3>
    </div>
    <?php
      $html = '<a id="home" href="./unifood.php">Home</a>';
      switch($atype) {
        case 'clienti':
        {
          $html .= '<a id="tutti_ordini" href="./clienti_ordini.php">I tuoi ordini</a>';
          break;
        }
        case 'fornitori':
        {
          $html .= '<a id="tutti_ordini" href="./fornitori_ordini.php">I tuoi ordini</a>';
          $html .= '<a id="ristorante" href="./fornitori_ristorante.php">Il tuo ristorante</a>';
          $html .= '<a id="tuoi_piatti" href="./fornitori_piatti.php">I tuoi piatti</a>';
          $html .= '<a id="tuoi_ingredienti" href="./fornitori_ingredienti.php">I tuoi ingredienti</a>';
          $html .= '<a id="tue_cucine" href="./fornitori_cucine.php">Le tue cucine</a>';
          break;
        }
        case 'fattorini':
        {
          $html .= '<a id="tutti_ordini" href="./fattorini_ordini.php">Le tue consegne</a>';
          break;
        }
        case 'admin':
        {
          $html .= '<a id="richieste" href="./admin_richieste.php">Le tue richieste</a>';
          $html .= '<a id="gestisci" href="./admin_gestisci.php">Gestisci sito</a>';
          break;
        }
      }
      $html .= '<a id="impostazioni" href="./impostazioni.php">Impostazioni</a><a id="esci" href="./logout.php">Esci</a>';
      echo $html;
    ?>
  </section>
  <?php } ?>
</div>
<script>
function rightAlignNotifiche() {
  var carrelloWidth = 0;
  if($("#carrello-topbar").length > 0 && $("#carrello-topbar").css("display") != "none") {
    carrelloWidth = $("#carrello-topbar").width();
  }
  $("#notifiche-content").css("right", carrelloWidth);
}
function rightAlignOrdini() {
  var carrelloWidth = 0;
  if($("#carrello-topbar").length > 0 && $("#carrello-topbar").css("display") != "none") {
    carrelloWidth = $("#carrello-topbar").width();
  }
  $("#ordini-content").css("right", $("#notifiche").width() + carrelloWidth);
}
$(document).ready(function() {
  var pathh = window.location.pathname;
  var path = pathh.split("/");
  console.log(path[path.length - 1]);
  switch(path[path.length - 1]) {
    case 'unifood.php': $("#home").addClass('topbar-active'); break;
    case 'impostazioni.php': $("#impostazioni").addClass("topbar-active"); break;
    case 'fornitori_ristorante.php': $("#ristorante").addClass("topbar-active"); break;
    case 'clienti_ordini.php': $("#tutti_ordini").addClass("topbar-active"); break;
    case 'fornitori_ordini.php': $("#tutti_ordini").addClass("topbar-active"); break;
    case 'fattorini_ordini.php': $("#tutti_ordini").addClass("topbar-active"); break;
    case 'admin_gestisci.php': $("#gestisci").addClass("topbar-active"); break;
    case 'admin_richieste.php': $("#richieste").addClass("topbar-active"); break;
    case 'fornitori_piatti.php': $("#tuoi_piatti").addClass("topbar-active"); break;
    case 'fornitori_cucine.php': $("#tue_cucine").addClass("topbar-active"); break;
    case 'fornitori_ingredienti.php': $("#tuoi_ingredienti").addClass("topbar-active"); break;
  }

  function notificheToHtml(notifiche) {
    var html = "";
    for(var id in notifiche) {
      html += '<li data-id="' + notifiche[id]["Id"] + '"'
            + ((notifiche[id]["LettaYN"] == '1') ? '>' : ' class="new">') + notifiche[id]["Testo"]
            + ((notifiche[id]["LettaYN"] == '1') ? '' : '<div class="circle"></div>') + '</li>';
    }
    return html;
  }

  function shakyNotifiche() {
    $("#notifiche-btn svg").addClass("shake");
    $("#notifiche-btn").addClass("glow");
    setTimeout (function(){
      $("#notifiche-btn svg").removeClass("shake");
      $("#notifiche-btn").removeClass("glow"); },
    800);
  }

  function loadNotifiche() {
    $.getJSON("./richiesta_notifiche.php", function(data) {
      //console.log(data);
      if(data["errore"] == "errore") {
        $("#notifiche-content ul").html('<li class="content-placeholder">Impossibile caricare le notifiche</li>');
      } else if(Object.keys(data).length == 0) {
        $("#notifiche-content ul").html('<li class="content-placeholder">Nessuna nuova notifica</li>');
      } else {
        var news = $("#notifiche .new").length;
        //console.log("news: " + news);
        $("#notifiche-content ul").html(notificheToHtml(data));
        if($("#notifiche .new").length > 0) {
          //console.log("xxx");
          $("#notifiche .badge").show();
        }
        if($("#notifiche .new").length > news) {
          shakyNotifiche();
        }
      }
    });
  }

  <?php
  if($logged && $atype != 'admin') {
    echo 'setInterval(function() {
      //console.log("B");
      if(!$("#notifiche-content").is(":visible")) {
        //console.log("a");
        loadNotifiche();
      }
    }, 5000);';
  }
  ?>

  function toggleRightElements() {
    $("#ordini-topbar").toggleClass("hidden");
    $("#notifiche").toggleClass("hidden");
    $("#carrello-topbar").toggleClass("hidden");
  }

  /* opening sidebar */
  $("#menu").on("click", function() {
    $("#sidebar").toggleClass("active");
    $("#menu").toggleClass("slide");
    if($("#topbar-file .dropdown-content").css("max-width") != "500px") { //mobile
      if($("#notifiche").hasClass("hidden")) {
        $("#sidebar").one('transitionend', function(e) { toggleRightElements(); });
      } else {
        toggleRightElements();
      }
    }

    /* setting correct sidebar header height (just better visual effect) */
    var deltap = ($("#topbar").height() - $("#sidebar-header").height()) / 2;
    $("#sidebar-header").css("padding-top", deltap);
    $("#sidebar-header").css("padding-bottom", deltap);
  });
  /* close open dropdowns before showing the clicked one */
  $(".dropbtn").on("click touchstart", function() {
    $(".dropdown-content").not($(this).next()).hide();
  });
  /* opening dropdown for "carrello" */
  $("#carrello-btn").on("click", function(e) {
    e.stopPropagation();
    $("#carrello-content").toggle();

    if($("#carrello-content").is(":visible")) {
      $("#carrello-topbar").css("background-color", "#f57c00");
    } else {
      $("#carrello-topbar").css("background-color", "#ef6c00");
    }
    $("#ordini-topbar, #notifiche").css("background-color", "#fb8c00");
  });
  /* opening dropdown for "ordini" */
  $("#ordini-btn").on("click", function(e) {
    e.stopPropagation();
    /* aligning dropdown content with button (just better visual effect) */
    if($("#topbar-file .dropdown-content").css("max-width") == "500px") { //desktop
      rightAlignOrdini();
    } else {
      $("#ordini-content").css("right", "0");
    }
    /* actually showing it */
    $("#ordini-content").toggle();

    if($("#ordini-content").is(":visible")) {
      $("#ordini-topbar").css("background-color", "#f57c00");
    } else {
      $("#ordini-topbar").css("background-color", "#fb8c00");
    }
    $("#notifiche").css("background-color", "#fb8c00");
    $("#carrello-topbar").css("background-color", "#f57c00");
  });

  var first = true;
  /* opening dropdown for "notifiche" */
  $("#notifiche-btn").on("click", function(e) {
    e.stopPropagation();
    /* aligning dropdown content with button (just better visual effect) */
    if($("#topbar-file .dropdown-content").css("max-width") == "500px") { //desktop
      rightAlignNotifiche();
    } else {
      $("#notifiche-content").css("right", "0");
    }
    $("#notifiche-content").toggle();

    if($("#notifiche .new").length > 0) {
      var notifiche = [];
      $("#notifiche .new").each(function() {
        notifiche.push($(this).attr('data-id'));
      });
      if(Object.keys(notifiche).length > 0) {
        $.post("./richiesta_notifiche_lette.php",
        {
          notifiche: notifiche
        });
      }
      first = false;
    }

    if($("#notifiche-content").is(":visible")) {
      $("#notifiche").css("background-color", "#f57c00");
      $("#notifiche .badge").fadeOut('slow');
    } else {
      $("#notifiche").css("background-color", "#fb8c00");
      $("#notifiche .new").find(".circle").remove();
      $("#notifiche .new").removeClass("new");
    }
    $("#ordini-topbar").css("background-color", "#fb8c00");
    $("#carrello-topbar").css("background-color", "#f57c00");
  });
  /* added for mobile - which fires both "click" and "touch" events - */
  $(".dropbtn").on("touchstart", function(e) {
     e.stopPropagation();
  });
  /* closing dropdown when clicking anywhere else but not inside of it*/
  $(document).on("click touchstart", function(e) {
    var isCartControl = $(e.target).hasClass('minus') || $(e.target).hasClass('item-remove');
    if(!($(e.target).closest(".dropdown-content").length > 0) && !isCartControl) {
      $(".dropdown-content").hide();
    }
    $("#ordini-topbar, #notifiche").css("background-color", "#fb8c00");
    $("#carrello-topbar").css("background-color", "#f57c00");
  });
  /* window resize listener to correctly re-align dropdown content */
  $(window).resize(function() {
    if($(".dropdown-content").is(":visible")) { /* only if something is visible re-align it */
      if($("#topbar-file .dropdown-content").css("max-width") != "500px") { //desktop -> mobile
        $(".dropdown-content").css("right", "0"); //remove right
      } else { //mobile -> desktop
        if($(".dropdown-content:visible").is("#notifiche-content")) {
          rightAlignNotifiche();
        }
        if($(".dropdown-content:visible").is("#ordini-content")) {
          rightAlignOrdini();
        }
      }
    }
    /*
    if($("#topbar-file .dropdown-content").css("max-width") == "500px"
      && $("#carrello-topbar .dropdown-content").find("li").length > 0) { //desktop -> mobile
      $("#carrello-topbar .dropdown-content").css("min-width", "50%");
    } else {
      //$("#carrello-topbar .dropdown-content").css("min-width", "40%");
    }
    */
  });
});
</script>

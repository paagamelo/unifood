<?php

require 'db_connect.php';
require 'login_functions.php';

sec_session_start(); //Avvio sessione php sicura
if($conn->connect_error) {
  die("Errore di comunicazione col server");
}

$logged = loggedAs($conn, "fornitori");
//!loggedAs($conn, 'fornitori')) {
if($logged) {

  $stmt = $conn->prepare("SELECT * FROM FATTORINI WHERE IdFornitore = ?");
  if(
       !$stmt
    || !$stmt->bind_param("i", $_SESSION['user_id'])
    || !$stmt->execute())
  {
    die(closeConnectionAndReturn($conn, "errore"));
  }

  $fattorini = array();
  $result = $stmt->get_result();
  $stmt->close();
  while($row = $result->fetch_assoc()) {
    $fattorini[] = $row;
  }
?>

<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- JQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- Font Awesome (for icons) -->
  <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>
  <!-- Topbar style -->
  <link rel="stylesheet" type="text/css" href="./topbar/topbar_style.css">
  <link rel="stylesheet" type="text/css" href="./alert.css">
  <link rel="stylesheet" type="text/css" href="./breadcrumb.css">
  <link rel="stylesheet" href="fornitori_ordini.css">
  <script src="./fornitori_ordini.js"></script>
  <title>Ordini</title>
</head>
<body>
  <div class="main">
  <?php require './topbar/topbar.php'; ?>
  <header>
    <h1>I Tuoi Ordini</h1>
  <ul class="breadcrumb">
    <li><a href="./unifood.php">Home</a></li>
    <li>Ordini</li>
  </ul>
  </header>
  <div id="ordini-in-corso">
    <header><h2>In Corso</h2></header>
    <table class="ordini in-corso">
      <caption class="visuallyhidden">Ordini in corso</caption>
      <thead>
        <th>Piatti</th>
        <th>Orario</th>
        <th>Note</th>
        <th>Totale</th>
        <th>Pagato</th>
        <th>Cliente</th>
        <th>Piano</th>
        <th></th>
      </thead>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
  <div id="ordini-in-consegna">
    <header><h2>In consegna</h2></header>
    <table class="ordini in-consegna">
      <caption class="visuallyhidden">Ordini in consegna</caption>
      <thead>
        <th>Piatti</th>
        <th>Orario</th>
        <th>Note</th>
        <th>Totale</th>
        <th>Pagato</th>
        <th>Cliente</th>
        <th>Piano</th>
        <th>Fattorino</th>
        <th>Data</th>
      </thead>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
  <div id="ordini-conclusi">
    <header><h2>Conclusi<h2></header>
    <table class="ordini conclusi">
      <caption class="visuallyhidden">Ordini conclusi</caption>
      <thead>
        <th>Piatti</th>
        <th>Orario</th>
        <th>Note</th>
        <th>Totale</th>
        <th>Pagato</th>
        <th>Cliente</th>
        <th>Piano</th>
        <th>Fattorino</th>
        <th>Data</th>
      </thead>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
  <div class="modal">
  <div class="modal-content">
      <span class="close">&times;</span>
      <form>
      <fieldset><legend>Seleziona un vettore di consegna</legend>
      <?php
      foreach($fattorini as $fattorino) {
        $html = '<div class="row"><input type="radio" id="'
          .$fattorino["Id"].'" value="'.$fattorino["Id"].'" name="fattorino">'
          .'<label for="'.$fattorino["Id"].'" class="">'
          .$fattorino["Nome"]." ".$fattorino["Cognome"].'</label></div>';
        echo $html;
      }
      ?>
      <div class="row">
        <input type="radio" id="altro" value="altro" name="fattorino" checked/>
        <label for="altro">Altro vettore</label>
      </div>
      <div class="last row">
        <input type="submit" value="Invia"/>
      </div>
      </fieldset>
    </form>
    </div>
  </div>
  </div>
</body>
<?php } else header('Location: ./'.closeConnectionAndReturn($conn,"login.php?Atype=fornitori")); //Effettua il login per accedere
?>

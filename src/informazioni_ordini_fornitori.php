<?php

include_once('common_queries.php');

function countOrdiniFornitore(&$conn, $idfornitore, $filter) {
  if(!checkFornitoreExists($conn, $idfornitore)) {
    return -1;
  }

  $stmt = $conn->prepare(
    "SELECT COUNT(DISTINCT Id)
    FROM ORDINI
    WHERE IdFornitore = ?
    AND Stato = ?");
  if(
       !$stmt
    || !$stmt->bind_param("is", $idfornitore, $filter)
    || !$stmt->execute())
  {
    return -1;
  } else {
    $count = $stmt->get_result()->fetch_assoc()["COUNT(DISTINCT Id)"];
    $stmt->close();
    return $count;
  }
}

function getInfoOrdiniFornitoreLimit(&$conn, $idfornitore, &$ordini, $filter, $start, $end) {
  $ordini = array();
  $stmt = $conn->prepare(
    "SELECT ORDINI.*,
 	  CLIENTI.Username AS CUsername, CLIENTI.Nome AS CNome, CLIENTI.Cognome AS CCognome,
    FATTORINI.Username AS FUsername, FATTORINI.Nome AS FNome, FATTORINI.Cognome AS FCognome
    FROM ORDINI
    INNER JOIN CLIENTI ON ORDINI.IdUtente = CLIENTI.Id
    LEFT OUTER JOIN FATTORINI ON FATTORINI.Id = ORDINI.IdFattorino
    WHERE ORDINI.IdFornitore = ?
    AND ORDINI.Stato = ?
    ORDER BY Stato DESC, Data ASC, OraConsegna ASC
    LIMIT ?, ?");
  if(
       !$stmt
    || !$stmt->bind_param("isii", $idfornitore, $filter, $start, $end)
    || !$stmt->execute())
  {
    $ordini["errore"] = true;
  } else {
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()) {
      $ordini[] = $row;
    }
    $stmt->close();
  }
}

function getPiattiOrdine(&$conn, $idordine, &$piatti) {
  $piatti = array();
  $stmt = $conn->prepare(
    "SELECT IdOrdine, IdPiattoInOrdine, Nome, Quantita
    FROM PIATTI_IN_ORDINE, PIATTI
    WHERE PIATTI_IN_ORDINE.IdFornitore = PIATTI.IdFornitore
    AND PIATTI_IN_ORDINE.IdPiattoInFornitore = PIATTI.IdPiattoInFornitore
    AND PIATTI_IN_ORDINE.IdOrdine = ?");
  if(
       !$stmt
    || !$stmt->bind_param("i", $idordine)
    || !$stmt->execute())
  {
    $piatti["errore"] = true;
  } else {
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()) {
      $piatti[] = $row;
    }
    $stmt->close();
  }
}

function getModifichePiatto(&$conn, $idordine, $idpiatto, &$modifiche) {
  $modifiche = array();
  $stmt = $conn->prepare(
    "SELECT AggiuntaYN, Nome
    FROM MODIFICHE, INGREDIENTI
    WHERE MODIFICHE.IdIngrediente = INGREDIENTI.Id
    AND MODIFICHE.IdOrdine = ?
    AND MODIFICHE.IdPiattoInOrdine = ?");
  if(
       !$stmt
    || !$stmt->bind_param("ii", $idordine, $idpiatto)
    || !$stmt->execute())
  {
    $modifiche["errore"] = true;
  } else {
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()) {
      $modifiche[] = $row;
    }
    $stmt->close();
  }
}

function populateOrdini(&$conn, &$ordini) {
  foreach($ordini as $k => $ordine) {

    $ordini[$k]['piatti'] = array();

    $piatti = array();
    getPiattiOrdine($conn, $ordine["Id"], $piatti);
    if(isset($piatti["errore"]) && $piatti["errore"] || empty($piatti)) {
      $ordini["errore"] = true;
      return;
    }

    foreach($piatti as $piatto) {

      $idpiatto = $piatto["IdPiattoInOrdine"];
      $ordini[$k]['piatti'][$idpiatto] = $piatto;

      $modifiche = array();
      getModifichePiatto($conn, $ordine["Id"], $idpiatto, $modifiche);
      if(isset($modifiche["errore"]) && $modifiche["errore"]) {
        $ordini["errore"] = true;
        return;
      }
      $ordini[$k]['piatti'][$idpiatto]['modifiche'] = $modifiche;
    }
  }
}

function getNuoviOrdiniFornitore(&$conn, $idfornitore, &$ordini) {
  $ordini = array();
  $stmt = $conn->prepare(
    "SELECT ORDINI.*,
 	  CLIENTI.Username AS CUsername, CLIENTI.Nome AS CNome, CLIENTI.Cognome AS CCognome,
    FATTORINI.Username AS FUsername, FATTORINI.Nome AS FNome, FATTORINI.Cognome AS FCognome
    FROM ORDINI
    INNER JOIN CLIENTI ON ORDINI.IdUtente = CLIENTI.Id
    LEFT OUTER JOIN FATTORINI ON FATTORINI.Id = ORDINI.IdFattorino
    WHERE ORDINI.IdFornitore = ?
    AND ORDINI.Stato = 'In preparazione'
    ORDER BY OraConsegna ASC");
  if(
       !$stmt
    || !$stmt->bind_param("i", $idfornitore)
    || !$stmt->execute())
  {
    $ordini["errore"] = true;
  } else {
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()) {
      $ordini[] = $row;
    }
    $stmt->close();
    populateOrdini($conn, $ordini);
  }
}

function getVecchiOrdiniFornitoreLimit(&$conn, $idfornitore, &$ordini_, $filter, $start, $end) {
  if(!checkFornitoreExists($conn, $idfornitore)) {
    $ordini_["errore"] = true;
    return;
  }

  $ordini = array();
  $ordini_ = array();

  getInfoOrdiniFornitoreLimit($conn, $idfornitore, $ordini, $filter, $start, $end);
  if(isset($ordini["errore"]) && $ordini["errore"]) {
    $ordini_["errore"] = true;
    return;
  }

  populateOrdini($conn, $ordini);
  $ordini_ = $ordini;

  /*
  foreach($ordini as $k => $ordine) {
    $ordini[$k]['piatti'] = array();

    $piatti = array();
    getPiattiOrdine($conn, $ordine["Id"], $piatti);
    if(isset($piatti["errore"]) && $piatti["errore"] || empty($piatti)) {
      $ordini_["errore"] = true;
      return;
    }

    foreach($piatti as $piatto) {

      $idpiatto = $piatto["IdPiattoInOrdine"];
      $ordini[$k]['piatti'][$idpiatto] = $piatto;

      $modifiche = array();
      getModifichePiatto($conn, $ordine["Id"], $idpiatto, $modifiche);
      if(isset($modifiche["errore"]) && $modifiche["errore"]) {
        $ordini_["errore"] = true;
        return;
      }
      $ordini[$k]['piatti'][$idpiatto]['modifiche'] = $modifiche;
    }
  }
  $ordini_ = $ordini;
  */
}

?>

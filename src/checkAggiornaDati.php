<?php
require 'checkDati.php';

function checkDati($telefono, $username, $id, $atype){
  return
             checkId($id)
          && checkUsername($username)
          && (($atype == "fornitori" || $atype == "admin") ? true : checkTelefono($telefono,($atype == "clienti" ? false : true)));
}

function checkCarta($tipo, $numero, $id){
  return
             checkId($id)
          && checkTipoCarta($tipo)
          && checkNumeroCarta($numero);
}

function checkCartaNonPresente($conn,$info, $id){

  $state="";

  $stmt = $conn->prepare("SELECT * FROM METODI_DI_PAGAMENTO WHERE IdCliente = ? AND Info = ?");
  $stmt->bind_param("ss", $id, $info);

  if(!$stmt->execute()) $state = "error";
  else {
    $row = $stmt->get_result();
    if($row->num_rows > 0) $state = "yet-present";
  }

  $stmt->close();

  return $state;
}

function checkPasswords($nuova, $attuale, $id){
  return
             checkId($id)
          && checkPassword($nuova)
          && checkPassword($attuale);
}

function checkPasswordCorretta($conn,$pwd,$id, $table){
  $state = "";
  $stmt = $conn->prepare("SELECT Password, Salt FROM ".$table." WHERE Id = ?");
  $stmt->bind_param("s", $id);

  if(!$stmt->execute()) $state = "error";
  else {
    $stmt->bind_result($password, $salt);
    $stmt->fetch();
    $pwd = hash('sha512', $pwd.$salt);
    if($password != $pwd) $state = "not-correct";
  }

  $stmt->close();

  return $state;
}
?>

function aggiornaDati(cucina, ingrediente, costo){
  $("option[value="+cucina+"]").remove();
  if($("#cucina option").length == 0){
    $("#modal-file").append("<div class=\"ok\">L'ingrediente viene ora fornito per ogni cucina!</div>");
    $("#aggiungi-ingrediente, #cucina, #costo").css("opacity","0.6");
    $("#aggiungi-ingrediente, #cucina, #costo").attr("disabled",true);
  }
  else $("#cucina option:first").attr("selected");
  var infoIngrediente = ingredienti_disponibili[ingrediente];
  var idCat = infoIngrediente["StrutturaleYN"] == 0 ? 0 : infoIngrediente["IdCategoria"];
  var id = "c"+cucina+"c"+idCat+"i"+ingrediente;
  var nome = infoIngrediente["Nome"];
  prezzi[id] = costo;
  var simbolo = (modifying ? meno : "");
  var display_costo  = (modifying ? "<label class =\"visuallyhidden\" for=\"costo"+id+"\">Costo aggiunta</label><input type=\"number\" id=\"costo"+id+"\" name=\"costo\" min=\"0\" max=\"99.99\"  step=\"0.1\" value=\""+costo+"\"></input>" : "<span class=\"costo-ingrediente\" >"+costo+"</span></li>");
  var display_ingrediente = "<li class =\"ingrediente\" id=\""+id+"\">"+simbolo+"<span class =\"nome-ingrediente\">" + nome + "</span>"+display_costo;
  if($("#c"+cucina+"c"+idCat+" li.ingrediente").length == 0) $("#c"+cucina+"c"+idCat+" .no-ingredienti").remove();
  $("#c"+cucina+"c"+idCat+" ul.ingredienti").append(display_ingrediente);
}

$(document).ready(function(){

  //Aggiungi ingrediente
  $(document).on("click", "#aggiungi-ingrediente", function() {
    event.preventDefault();
    $(".error").remove(); //Rimuovi errori precedentemente visualizzati
    $(".ok").remove(); //Rimuovi errori precedentemente visualizzati
    var cucina = $("#cucina").val().trim();
    var ingrediente = $("#ingrediente").val().trim();
    var costo = $("#costo").val().trim().replace(",",".");
    var errori  = "";
    if(checkNumber(cucina) || checkNumber(ingrediente)) errori += "<li>Qualcosa è andato storto, è consigliato ricaricare la pagina</li>";
    errori += checkPrezzo(costo);
    if(errori != "") $("#modal-file").append("<div class=\"error\"><ul>" + errori + "</ul></div>");
    else{ //Dati inseriti validi
      $.post("./aggiorna_ingredienti_fornitori.php",
      {
        request: "aggiungi",
        cucina: cucina,
        ingrediente: ingrediente,
        costo: costo
      },
      function(state){
        var messaggio = "";
        if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
        else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
        else if(state == "yet-present") messaggio = "La fornitura era già stata inserita";
        if(messaggio != "") $("#modal-file").append("<div class=\"error\">"+messaggio+"</div>");
        else{ //Non si sono verificati errori, chiudi la modal e mostra la carta aggiunta
          $("#modal-file").append("<div class=\"ok\">Inserimento avvenuto correttamente!</div>");
          aggiornaDati(cucina, ingrediente, costo);
        }
      });
    }
  });
});

<?php
header('Content-Type: application/json');
$output = array();

if(isset($_GET['idfornitore']) && isset($_GET['idpiatto']) && isset($_GET['modificato'])) {
  require 'db_connect.php';
  require 'check_ordine.php';
  if($conn->connect_error) {
    $output["errore"] = "errore";
    die(json_encode($output));
  }

  $idfornitore = $_GET["idfornitore"];
  if(!checkFornitoreExists($conn, $idfornitore)) {
    $output["errore"] = "errore";
    closeConnection($conn);
    die(json_encode($output));
  }

  $idpiatto = $_GET["idpiatto"];
  if(!checkPiattoAppartieneAFornitore($conn, $idfornitore, $idpiatto, $piatto)) {
    $output["errore"] = "errore";
    closeConnection($conn);
    die(json_encode($output));
  }
  $idcucina = $piatto["IdCucina"];

  switch($_GET['modificato']) {

    case '0':
    {

      getIngredientiPiatto($conn, $idfornitore, $idpiatto, $ingredienti_piatto);
      if(isset($ingredienti["errore"]) && $ingredienti["errore"]) {
        $output["errore"] = "errore";
        print json_encode($output);
        break;
      }

      getIngredientiFornitore($conn, $idfornitore, $idcucina, $ingredienti_fornitore);
      if(isset($ingredienti_fornitore["errore"]) && $ingredienti_fornitore["errore"]) {
        $output["errore"] = "errore";
        print json_encode($output);
        break;
      }

      $ingredienti_fornitore = array_filter($ingredienti_fornitore, function($i) use (&$ingredienti_piatto) {
        return !in_array($i, $ingredienti_piatto);
      });

      foreach($ingredienti_piatto as $k => $ingrediente) {
        $ingredienti_piatto[$k]["CostoAggiunta"] = "0.00";
      }

      $strutturali = array_filter($ingredienti_piatto, function($i) { return $i["StrutturaleYN"]; });
      $output['strutturali'] = array();

      foreach($strutturali as $strutturale) {
        $id = $strutturale["Id"];
        $strutturali_stessa_categoria = array_filter($ingredienti_fornitore, function($i) use (&$strutturale) {
          return $i["IdCategoria"] == $strutturale["IdCategoria"];
        });
        $output['strutturali'][$id] = $strutturale;
        $output['strutturali'][$id]['alt'] = $strutturali_stessa_categoria;
      }

      $rimovibili = array_filter($ingredienti_piatto, function($i) { return !$i["StrutturaleYN"]; });
      $output['rimovibili'] = $rimovibili;

      $aggiungibili = array_filter($ingredienti_fornitore, function($i) use (&$rimovibili) {
        return !$i["StrutturaleYN"];
      });

      $output['aggiungibili'] = $aggiungibili;

      print json_encode($output);
      break;
    }

    case '1':
    {
      if(!isset($_GET['modifiche']) || empty($_GET['modifiche'])) {
        $output["errore"] = "errore";
        print json_encode($output);
        break;
      }

      getIngredientiPiatto($conn, $idfornitore, $idpiatto, $ingredienti_piatto);
      if(isset($ingredienti["errore"]) && $ingredienti["errore"]) {
        $output["errore"] = "errore";
        print json_encode($output);
        break;
      }


      getIngredientiFornitore($conn, $idfornitore, $idcucina, $ingredienti_fornitore);
      if(isset($ingredienti_fornitore["errore"]) && $ingredienti_fornitore["errore"]) {
        $output["errore"] = "errore";
        print json_encode($output);
        break;
      }

      $modifiche_ = array();
      foreach($_GET['modifiche'] as $modifica) {
        $modifiche_[] = $modifica;
      }

      getInfoIngredienti($conn, $modifiche_, $idcucina, $idfornitore, $modifiche);
      if(isset($modifiche["errore"]) && $modifiche["errore"]) {
        $output["errore"] = "errore";
        print json_encode($output);
        break;
      }

      if(!checkModificheAppartengonoAFornitore($conn, $ingredienti_fornitore, $modifiche)) {
        $output["errore"] = "modifiche non valide";
        print json_encode($output);
        break;
      }

      if(!checkModificheStrutturaliValide($ingredienti_piatto, $modifiche)) {
        $output["errore"] = "modifiche non valide";
        print json_encode($output);
        break;
      }

      $ingredienti_fornitore = array_filter($ingredienti_fornitore, function($i) use (&$ingredienti_piatto) {
        return !in_array($i, $ingredienti_piatto);
      });

      $output['default'] = $ingredienti_piatto;
      $output['fornitore'] = $ingredienti_fornitore;

      foreach($ingredienti_piatto as $k => $ingrediente) {
        $ingredienti_piatto[$k]["CostoAggiunta"] = "0.00";
      }

      $strutturali_piatto = array_filter($ingredienti_piatto, function($i) { return $i["StrutturaleYN"] == "1"; });
      $strutturali_modifiche = array_filter($modifiche, function($i) { return $i["StrutturaleYN"] == "1"; });

      foreach($strutturali_piatto as $strutturale) {
        $ing;
        $strutturali_alt;

        $strutturali_modifiche_stessa_categoria = array_filter($strutturali_modifiche, function($i) use (&$strutturale) {
          return $i["IdCategoria"] == $strutturale["IdCategoria"];
        });
        if(!empty($strutturali_modifiche_stessa_categoria)) { //modificato
          $ing = $strutturali_modifiche_stessa_categoria[0];
          $strutturali_alt = array_filter($ingredienti_fornitore, function($i) use (&$ing) {
            return $i["IdCategoria"] == $ing["IdCategoria"] && $i["Id"] != $ing["Id"];
          });
          $strutturali_alt[] = $strutturale;
        } else { // non modificato
          $ing = $strutturale;
          $strutturali_alt = array_filter($ingredienti_fornitore, function($i) use (&$ing) {
            return $i["IdCategoria"] == $ing["IdCategoria"];
          });
        }
        $id = $ing["Id"];
        $output['strutturali'][$id] = $ing;
        $output['strutturali'][$id]['alt'] = $strutturali_alt;
      }

      $rimossi = array_filter($ingredienti_piatto, function($i) use(&$modifiche) {
        return !$i["StrutturaleYN"] && in_array($i["Id"], array_map(function($i) { return $i["Id"]; }, $modifiche));
      });

      $aggiunti = array_filter($modifiche, function($i) use(&$ingredienti_piatto) {
        return !$i["StrutturaleYN"] && !in_array($i["Id"], array_map(function($i) { return $i["Id"]; }, $ingredienti_piatto));
      });

      $rimovibili = array_merge(array_filter($ingredienti_piatto, function($i) use(&$rimossi) {
        return !$i["StrutturaleYN"] && !in_array($i["Id"], array_map(function($i) { return $i["Id"]; }, $rimossi));
      }), $aggiunti);

      $output['rimovibili'] = $rimovibili;

      $aggiungibili = array_merge(array_filter($ingredienti_fornitore, function($i) use (&$aggiunti) {
        return !$i["StrutturaleYN"] && !in_array($i["Id"], array_map(function($i) { return $i["Id"]; }, $aggiunti));
      }), $rimossi);

      $output['aggiungibili'] = $aggiungibili;

      print json_encode($output);
      break;
    }

    default:
    {
      $output["errore"] = "errore";
      print json_encode($output);
      break;
    }
  }
  closeConnection($conn);
}

 ?>

<?php
require 'db_connect.php';
require 'login_functions.php';
require 'common_queries.php';

sec_session_start(); //Avvio sessione php sicura
$output = array();

if(isset($_GET["request"])) {
   if ($conn->connect_error) {
     $output["Errore"] = "Errore di comunicazione con il server";
     die(json_encode($output));
   }

   if(!isset($_SESSION["user_id"]) && !isset($_GET["id"])) {
     $output["Errore"] = "Dati inviati invalidi";
     die(json_encode(closeConnectionAndReturn($conn,$output)));
   }
   $id = (isset($_GET["id"]) ? $_GET["id"] : $_SESSION["user_id"]);
   if(!checkFornitoreExists($conn,$id))  {
     $output["Errore"] = "Dati inviati invalidi";
     die(json_encode(closeConnectionAndReturn($conn,$output)));
   }

   switch ($_GET["request"]) {
      case "ingredienti-fornitore": //Richiesti ingredienti di un fornitore

        $categorieIngredienti = getCategorieIngredienti($conn);
        if(isset($categorieIngredienti["Errore"])){
          $output["Errore"] = "Errore di comunicazione con il server";
          die(json_encode(closeConnectionAndReturn($conn,$output)));
        } else if(empty($categorieIngredienti)) $output["CategorieIngredienti"] = "Nessuna categoria disponibile";
        else foreach ($categorieIngredienti as $categoria) $output["CategorieIngredienti"][$categoria["Id"]] = $categoria["Nome"];
        $cucine = getCucineFornitore($conn, $id);
        if(isset($cucine["Errore"])) $output["Errore"] = "Errore nel comunicare con il server";
        else if(empty($cucine)) $output["Errore"] = "Nessuna cucina disponibile";
        else{ //Esistono delle cucine
          foreach ($cucine as $cucina) { //Per ogni cucina
            getIngredientiFornitore($conn, $id, $cucina["Id"], $ingredienti); //Prendo gli ingredienti forniti
            if(isset($ingredienti["errore"])){
              $output["Errore"] = "Errore di comunicazione con il server";
              die(json_encode(closeConnectionAndReturn($conn,$output)));
            }
            if(empty($ingredienti)) $ingredienti = "Nessun ingrediente disponibile";
            $cucina["Ingredienti"]= $ingredienti;
            $output["Cucine"][] = $cucina;
            $ingredienti = [];
          }
        }

        print json_encode($output);
        break;

      case "ingredienti": //Richiesti tutti gli ingredienti ingredienti disponibili
        $ingredienti = getIngredienti($conn);
      if(isset($ingredienti["Errore"])) $output["Errore"] = "Errore di comunicazione con il server";
      else if(empty($ingredienti)) $output["Errore"] = "Nessun ingrediente disponibile";
      else foreach ($ingredienti as $ingrediente) $output["Ingredienti"][$ingrediente["Id"]] = $ingrediente;

      print json_encode($output);
      break;
  }
  closeConnection($conn);
} else { //Richiesta invalida
  $output["Errore"] = "Richiesta invalida";
  die(json_encode(closeConnectionAndReturn($conn,$output)));
}
?>

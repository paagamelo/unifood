var modifying = false; //Stato pagina
var cucine_fornite = [];
var cucine_non_fornite = [];
var cucine = [];
var change_status = true;
var nuove_cucine = [];
var no_cucine = "<div class=\"no-cucine\"> Nessuna cucina da mostrare </div>"
/*
var angolodx = "<span class=\"symbol a-d\"><i class=\"fas fa-angle-right fa-lg\"></i></span>";
var angologiù = "<span class=\"symbol a-g\"><i class=\"fas fa-angle-down fa-lg\"></i></span>";
var più = "<span class=\"symbol plus\"><i class=\"fas fa-plus-circle fa-lg\"></i></span>";
var meno = "<span class=\"symbol minus\"><i class=\"fas fa-minus-circle fa-lg\"></i></span>";
*/
var angolodx = "<span class=\"symbol a-d\" aria-label=\"Espandi\"><i aria-hidden=\"true\" class=\"fas fa-angle-right fa-lg\"></i></span>";
var angologiù = "<span class=\"symbol a-g\" aria-label=\"Riduci\"><i aria-hidden=\"true\" class=\"fas fa-angle-down fa-lg\"></i></span>";
var meno = "<span class=\"symbol minus\" aria-label=\"Rimuovi\"><i aria-hidden=\"true\" class=\"fas fa-minus-circle fa-lg\"></i></span>";
var più = "<span class=\"symbol plus\" aria-label=\"Aggiungi\"><i aria-hidden=\"true\" class=\"fas fa-plus-circle fa-lg\"></i></span>";
//Mostra modal per nuova categoria
function creaRichiesta(){
  var display_cucine = "";
  var selected = "selected";
  for(cucina in cucine) {
    var nome_cucina = $("#"+cucina+" .nome-cucina").html();
    display_cucine += "<option value=\""+cucina+"\" "+selected+" >"+nome_cucina+"</option>";
    selected = "";
  }
  display_cucine += "<option value=\"new\" "+selected+">altro</option>";
  $("#modal-file").load("./crea_richiesta.html",
  function(){
    $("#cucina").append(display_cucine);
    $(".modal").show();
  });
}
//Sposta una cucina nell'altra classe
function spostaCucina(simbolo,lista_cucine){
  var cucina = simbolo.parent();
  $(lista_cucine).append(cucina);
  simbolo.replaceWith(lista_cucine == ".cucine-fornite" ? meno : più);
}
//Inserisce le cucine e torna l'id di quelle inserite
function inserisciCucine(cucine_da_inserire){
  var cucine_inserite = [];
  for(ci in cucine_da_inserire){ //Per ogni cucina
    var cucina = cucine_da_inserire[ci]; //Cucina
    var display_cucina =
                  "<li class=\"cucina\" id="+cucina["Id"]+">"
                + "<span class=\"nome-cucina\">"+cucina["Nome"]+"</span>"
                + "<ul class=\"categorie\">";
    var categorie = cucina["Categorie"];
    if(categorie["Errore"] != undefined) display_cucina += "</ul><div class=\"categorie\">"+cucina["Categorie"]["Errore"]+"</div>"; //Non ci sono Categorie
    else{ //Esiste almeno una categoria
      for(ca in categorie){ //Per ogni categoria
        var categoria = categorie[ca]
        display_cucina += "<li class =\"categoria\"><span class =\"nome-categoria\">" + categoria["Nome"] + "</span></li>";
      }
      display_cucina += "</ul>";
    }
    cucine[cucina["Id"]] = display_cucina;
    cucine_inserite.push(cucina["Id"]);
  }
  return cucine_inserite;
}
//Rimuovi cucine mostrate, se locale è false, ricarica dal db
function ricaricaCucine(locale){
  $("ul.cucine-fornite").empty();
  $("ul.cucine-non-fornite").empty();
  $(".no-cucine").remove();
  if(locale){
    if(cucine_fornite[0] == undefined) $("#cucine-fornite").append(no_cucine);
    if(cucine_non_fornite[0] == undefined) $("#cucine-non-fornite").append(no_cucine);
    mostraCucine();
  } else{
    cucine_fornite = [];
    cucine_non_fornite = [];
    cucine = [];
    loadCucine();
  }
}
//True se è contenuto nelle nuove
function checkEqualNuove(indice_cucina){
  return nuove_cucine.includes(indice_cucina);
}
//True se è contenuto in quelle fornite
function checkEqualFornite(indice_cucina){
  return cucine_fornite.includes(indice_cucina);
}
//True se è stata rimossa
function checkRemoved(indice_cucina){
  return !nuove_cucine.includes(indice_cucina);
}
//True se è una stata aggiunta
function checkAdded(indice_cucina){
  return !cucine_fornite.includes(indice_cucina);
}
//Controlla correttezza dati in form
function checkDati(cucine_da_controllare){
  var errore = false;
  for(cucina in cucine_da_controllare) if (cucine[cucine_da_controllare[cucina]] == undefined) errore = true;
  return errore;
}
//Ottieni le nuove cucine fornite
function ottieniCucineFornite(){
  var nuove_cucine_fornite = $("ul.cucine-fornite .cucina");
  var length = nuove_cucine_fornite.length;
  var result = [];
  for(var i=0; i<length; i++) result.push(parseInt(nuove_cucine_fornite[i].id));
  return result;
}
//Mostra cucine
function mostraCucine(){
  for(cucina in cucine_fornite) $("ul.cucine-fornite").append(cucine[cucine_fornite[cucina]]);
  for(cucina in cucine_non_fornite) $("ul.cucine-non-fornite").append(cucine[cucine_non_fornite[cucina]]);
  $(".nome-cucina").after(angolodx);
}

function toggleCucine(){
  if(modifying){
    var errori = "";
    nuove_cucine = ottieniCucineFornite();
    if(!nuove_cucine.every(checkEqualFornite) || !cucine_fornite.every(checkEqualNuove)){
      var cucine_da_aggiungere = nuove_cucine.filter(checkAdded);
      var cucine_da_rimuovere = cucine_fornite.filter(checkRemoved);
      if(!checkDati(nuove_cucine)){ //Nessun errore
        $.post("./aggiorna_cucine_fornitori.php",
        {
          request: "cucine",
          aggiungi: cucine_da_aggiungere,
          rimuovi: cucine_da_rimuovere
        },
        function(state){
          var messaggio = "";
          if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
          else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
          else if(state == "query-error") messaggio = "Non è stato possibile aggiornare uno o più dati";
          if(messaggio != ""){ //Si è verificato qualche errore, mostra modal
            $("#modal-file").append("<header><h3>Operazione non riuscita</h3></header><div>"+messaggio+"</div>");
            $(".modal").show();
            ricaricaCucine(state != "query-error"); //C'è stato un errore nell'aggiornamento, ricarica i dati
          } else { //Aggiorna valori attuali
            cucine_fornite = nuove_cucine;
            cucine_non_fornite = cucine.filter(checkRemoved);
          }
        });
      }else{ //Errore nelle cucine passate
        change_status = false;
        $("#modal-file").append("<header><h3>Errori in Form</h3></header><ul><li>Una o più cucine non esistono!</li></ul>");
        $(".modal").show();
      }
    }
  }
}


//Pulsante modifica/fine dati cucine premuto
function toggleModify() {
  toggleCucine();
  if(change_status){
    if(modifying){
      $("button.cucine").html("Modifica");
      $("li.categoria").removeClass("modifying");
      $(".plus").remove();
      $(".minus").remove();
      $("#cucine-non-fornite").addClass("hidden");
      $(".modifying").removeClass("modifying");
    } else{
      $("button.cucine").html("Fine");
      $("li.categoria").addClass("modifying");
      $("ul.cucine-fornite li.cucina").prepend(meno);
      $("ul.cucine-non-fornite li.cucina").prepend(più);
      $("#cucine-non-fornite").removeClass("hidden");
    }
    modifying = !modifying;
  }
  change_status = true;
}

//Carica giorni
function loadCucine(){
  $.get("./richiesta_cucine_fornitori.php",
  {
    request: "cucine"
  },
  function(data){
    var res = JSON.parse(data);
    if(res["Errore"] == undefined){ //Non ci sono stati errori
      var c_f = res["Fornite"];
      var c_nf = res["Non-Fornite"];
      if(c_f != undefined) cucine_fornite = inserisciCucine(c_f);
      else $("#cucine-fornite").append(no_cucine);
      if(c_nf != undefined) cucine_non_fornite = inserisciCucine(c_nf);
      else $("#cucine-non-fornite").append(no_cucine);
      mostraCucine();
    } else $("ul.cucine").after("<div class=\"no-cucine\">"+res["Errore"]+"</div>");
  });
}

$(document).ready(function(){

  //Carica dati al primo caricamento della pagina
  loadCucine();

  //Chiudi modal
  $(document).on("touchend mouseup", ".close", function(){
    setTimeout(function() {
      $(".modal").hide();
      $("#modal-file").children().remove();
    }, 100);
  });

  //Cliccato l'angolo destro di una cucina
  $(document).on("touchend mouseup", ".a-d", function(){
    var context = this;
    setTimeout(function() {
      $(context).parent().find(".categorie").show();
      $(context).replaceWith(angologiù);
    }, 100);
  });

  //Cliccato l'angolo giù di una cucina
  $(document).on("touchend mouseup", ".a-g", function(){
    var context = this;
    setTimeout(function() {
      $(context).parent().find(".categorie").hide();
      $(context).replaceWith(angolodx);
    }, 100);
  });

  //Cliccato il meno
  $(document).on("touchend mouseup", ".minus", function(){
    if($("ul.cucine-non-fornite").children().length == 0) $("#cucine-non-fornite div.no-cucine").remove();
    spostaCucina($(this),".cucine-non-fornite");
    if($("ul.cucine-fornite").children().length == 0) $("#cucine-fornite").append(no_cucine);
  });

  //Cliccato il più
  $(document).on("touchend mouseup", ".plus", function(){
    if($("ul.cucine-fornite").children().length == 0) $("#cucine-fornite div.no-cucine").remove();
    spostaCucina($(this),".cucine-fornite");
    if($("ul.cucine-non-fornite").children().length == 0) $("#cucine-non-fornite").append(no_cucine);
  });
});

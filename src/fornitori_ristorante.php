<!DOCTYPE html>
<?php
require 'db_connect.php';
require 'login_functions.php';

sec_session_start(); //Avvio sessione php sicura
$logged = loggedAs($conn,"fornitori"); //La pagina è accessibile solo ai fornitori
if($logged) { //Login effettuato come fornitore
?>
  <html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Font Awesome (for icons) -->
    <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>
    <!-- Topbar style -->
    <link rel="stylesheet" type="text/css" href="./topbar/topbar_style.css">
    <link rel="stylesheet" type="text/css" href="./modal.css">
    <link rel="stylesheet" type="text/css" href="fornitori_ristorante_style.css">
    <link rel="stylesheet" type="text/css" href="./breadcrumb.css">
    <script src="./fornitori_ristorante.js"></script>
    <script src="./checkDati.js"></script>
    <title>Il tuo ristorante</title>
  </head>
  <body>
    <header>
      <h1>Il tuo ristorante</h1>
    <ul class="breadcrumb">
      <li><a href="./unifood.php">Home</a></li>
      <li>Il Tuo Ristorante</li>
    </ul>
    </header>
    <section id="logo">
      <header><h2>Logo</h2></header>
      <div id="logo-area">
        <?php
        $dir = "./images/loghi/";
        $filename = "logo".$_SESSION["user_id"]."*";
        $file = glob($dir.$filename);
        if(empty($file)) $file = $dir."default.png";
        else $file = $file[0];
        ?>
        <img src="<?php print $file; ?>" alt="logo" class=".logo" id="logo-img">
        <span>Clicca per inserire una nuova immagine</span>
      <form id="form-logo" class="visuallyhidden" method="post" enctype="multipart/form-data">
        <label for="seleziona-img">Scegli un immagine come logo</label><input id="seleziona-img" name="logo" accept="image/*" type="file" onchange="loadFile(event)"></input>
      </form>
      </div>
        <button  class = "elimina" onclick = "eliminaLogo()">Elimina</button>
        <button  class = "annulla" onclick="annullaLogo();">Annulla</button>
        <button  class = "salva" onclick="inviaLogo();">Salva</button>
    </section>
    <section id="dati-ristorante">
      <div class="row">
      <header><h2>Dati Ristorante</h2></header>
      <button type="button" class="restaurant-data modify-btn">Modifica</button>
    </div>
      <ul class="dati-ristorante">
        <li class="nome">
          <span class="nome-lb label">Nome:</span><span class="nome"></span>
        </li>
        <li class="indirizzo">
          <span class="indirizzo-lb label">Indirizzo:</span><span class="indirizzo"></span>
        </li>
        <li class="telefono">
          <span class="telefono-lb label">Telefono:</span><span class="telefono"></span>
        </li>
        <li class="speseConsegna">
          <span class="speseConsegna-lb label">Spese di consegna:</span><span class="speseConsegna"></span>
        </li>
        <li class="orarioApertura">
          <span class="orarioApertura-lb label">Orario di apertura:</span><span class="orarioApertura"></span>
        </li>
        <li class="orarioChiusura">
          <span class="orarioChiusura-lb label">Orario di chiusura:</span><span class="orarioChiusura"></span>
        </li>
        <li class="maxOrdini">
          <span class="maxOrdini-lb label">Massimo numero di ordini contemporaneamente:</span><span class="maxOrdini"></span>
        </li>
        <li class="note">
          <span class="note-lb label">Note:</span><span class="note"></span>
        </li>
      </ul>
    </section>
    <section id="chiusura">
      <div class="row">
      <header><h2>Chiusura ristorante</h2></header>
      <button type="button" class="chiusura modify-btn">Modifica</button>
    </div>
      <ul class = chiusura>
        <li class="giorni-chiusura">
          <span class="giorni-lb label">Giorni di chiusura:</span><span class="giorni"></span>
        </li>
      </ul>
    </section>
    <section id="links">
      <header><h2>Gestisci il menù</h2></header>
      <ul class = link>
        <li><a href="fornitori_piatti.php"> I tuoi piatti ></a></li>
        <li><a href="fornitori_ingredienti.php"> I tuoi ingredienti ></a></li>
        <li><a href="fornitori_cucine.php"> Le tue cucine ></a></li>
      </ul>
    </section>
    <!-- The Modal -->
    <div class="modal">
      <!-- Modal content -->
      <div class="modal-content">
        <span class="close">&times;</span>
        <!-- Load modal file-->
        <div id="modal-file">

        </div>
      </div>
    </div>
    <?php require './topbar/topbar.php'; closeConnection($conn);?>
  </body>
  </html>
<?php
} else header('Location: ./'.closeConnectionAndReturn($conn,"login.php?Atype=fornitori")); //Effettua il login come fornitore per accedere
?>

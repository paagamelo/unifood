<?php
header('Content-type: text/html;charset=utf-8');
require 'db_connect.php';
require 'login_functions.php';

sec_session_start(); //Avvio sessione php sicura
if(loggedAs($conn,"fornitori") && isset($_POST["request"])) { //La richiesta può avvenire solo da un fornitore
   if ($conn->connect_error) {
     $output["Errore"] = "Errore di comunicazione con il server";
     die(json_encode($output));
   }

   $output = array();
   $id = $_SESSION["user_id"];

   switch ($_POST["request"]) {
      case "dati": //Richiesti dati ristorante

        $stmt = $conn->prepare("SELECT Nome, Telefono, Indirizzo, SpeseConsegna, MaxOrdiniContemporaneamente, ApertoDa, ApertoA, Note FROM FORNITORI WHERE Id = ?");
        $stmt->bind_param("s", $id);
        if(!$stmt->execute()) $output["Errore"] = "Errore nel comunicare con il server";
        else{ //Non si sono verificati errori
          $stmt->bind_result($output["Nome"], $output["Telefono"], $output["Indirizzo"], $output["SpeseConsegna"], $output["MaxOrdini"], $output["OrarioApertura"], $output["OrarioChiusura"], $output["Note"]);
          $stmt->fetch();
        }

        $stmt->close();
        print json_encode($output);
        break;

      case "giorni" : //Richiesti giorni chiusura

        $stmt = $conn->prepare(
          " SELECT g.Nome
            FROM GIORNI g, GIORNI_LIBERI gl
            WHERE gl.IdFornitore = ?
            AND gl.IdGiorno = g.Id;"
         );

        $stmt->bind_param("s", $id);
        if(!$stmt->execute()) $output["Errore"]= "Errore nel comunicare con il server";
        else { //Non si sono verificati errori

          $result = $stmt->get_result();
          if($result != false) while($row = $result->fetch_assoc()) $output[] = $row["Nome"];
          else $output["Errore"]= "Errore nel comunicare con il server";
          if(empty($output)) $output[] = "Nessuno";
        }

        $stmt->close();
        print json_encode($output);
        break;

  }
  closeConnection($conn);
} else { //Login non effettuato
  $output["Errore"] = "Errore di comunicazione con il server";
  die(json_encode(closeConnectionAndReturn($conn,$output)));
}
?>

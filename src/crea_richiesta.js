var aggiungi_cucina = "<div class=\"form-group\"><label for=\"nuova-cucina\">Nome cucina:</label><input type=\"text\" class=\"form-control\" id=\"nuova-cucina\" name=\"nome-cucina\" maxlength=\"25\"></input></div>";

//Controlla se bisogna aggiungere/rimuovere la nuova categoria
function checkCucine(){
  if($("#cucina").val() == "new") $("#invia-richiesta").before(aggiungi_cucina);
  else if($("#nuova-cucina")[0] != undefined) $("#nuova-cucina").parent().remove();
}

$(document).ready(function(){

  //Invia richiesta
  $(document).on("click", "#invia-richiesta", function() {
    event.preventDefault();
    $(".error").remove(); //Rimuovi errori precedentemente visualizzati
    $(".ok").remove(); //Rimuovi errori precedentemente visualizzati
    var categoria = $("#categoria").val().trim();
    var cucina = $("#cucina").val().trim();
    var nome_cucina = "";
    if(cucina == "new") nome_cucina = $("#nuova-cucina").val().trim();
    else  nome_cucina = $("#cucina option[value="+cucina+"]").html().trim();
    var errori = checkNomeCucina(nome_cucina) + checkNomeCategoria(categoria);

    if(errori != "") $("#modal-file").append("<div class=\"error\"><ul>" + errori + "</ul></div>");
    else{ //Dati inseriti validi
      var descrizione = "Cucina: " + nome_cucina + " Categoria: " + categoria;
      if(cucina == "new") descrizione = "Nuova " + descrizione;
      $.post("./aggiorna_cucine_fornitori.php",
      {
        request: "richiesta",
        descrizione: descrizione
      },
      function(state){
        var messaggio = "";
        if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
        else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
        if(messaggio != "") $("#modal-file").append("<div class=\"error\">"+messaggio+"</div>");
        else $("#modal-file").append("<div class=\"ok\">Richiesta inoltrata agli amministratori!</div>");
      });
    }
  });
});

<?php
header('Content-Type: application/json');

require 'db_connect.php';
require 'login_functions.php';

sec_session_start();
$output = array();

if($conn->connect_error) {
  $output["errore"] = "errore";
  die(json_encode($output));
}

if(!loggedAs($conn, 'admin')) {
  $output["errore"] = "errore";
  closeConnection($conn);
  die(json_encode($output));
}

if(isset($_GET['idrichiesta'])) {

  $idrichiesta = $_GET['idrichiesta'];
  $idadmin = $_SESSION['user_id'];

  $stmt = $conn->prepare("UPDATE RICHIESTE SET ApprovataYN = '1', IdAdmin = ? WHERE Id = ?");
  if(
       !$stmt
    || !$stmt->bind_param("ii", $idadmin, $idrichiesta)
    || !$stmt->execute())
  {
    $output["errore"] = "errore";
    closeConnection($conn);
    die(json_encode($output));
  }
  print json_encode("success");
  closeConnection($conn);
}
?>

<?php
header('Content-Type: application/json');

require 'db_connect.php';
require 'login_functions.php';
include_once('informazioni_ordini_fattorini.php');
sec_session_start();
$output = array();

if($conn->connect_error) {
  $output["errore"] = "errore";
  die(json_encode($output));
}

if(!loggedAs($conn, 'fattorini')) {
  $output["errore"] = "errore";
  closeConnection($conn);
  die(json_encode($output));
}

if(isset($_POST['idordine'])) {

  $idordine = $_POST['idordine'];
  $idfattorino = $_SESSION['user_id'];

  if(!checkFattorinoExists($conn, $idfattorino)) {
    $output["errore"] = "errore";
    closeConnection($conn);
    die(json_encode($output));
  }

  $stmt = $conn->prepare("SELECT * FROM ORDINI WHERE Id = ? AND IdFattorino = ?");
  if(
       !$stmt
    || !$stmt->bind_param("ii", $idordine, $idfattorino)
    || !$stmt->execute())
  {
    $output["errore"] = "errore";
    closeConnection($conn);
    die(json_encode($output));
  }

  $result = $stmt->get_result();
  $stmt->close();

  if($result->num_rows == 0) {
    $output["errore"] = "errore";
    closeConnection($conn);
    die(json_encode($output));
  }

  $ord = $result->fetch_assoc();
  $idcliente = $ord["IdUtente"];
  $idfornitore = $ord["IdFornitore"];

  $stmt = $conn->prepare("UPDATE ORDINI SET Stato='Consegnato', PagatoYN = 1 WHERE Id = ? AND IdFattorino = ?");
  if(
       !$stmt
    || !$stmt->bind_param("ii", $idordine, $idfattorino)
    || !$stmt->execute())
  {
    $output["errore"] = "errore";
    closeConnection($conn);
    die(json_encode($output));
  }

  $stmt = $conn->prepare("DELETE FROM NOTIFICHE WHERE IdOrdine = ? AND IdUtente = ?");
  if(
       !$stmt
    || !$stmt->bind_param("ii", $idordine, $idcliente)
    || !$stmt->execute())
  {
    $output["errore"] = "errore";
    closeConnection($conn);
    die(json_encode($output));
  }
  $stmt->close();

  $stmt = $conn->prepare("DELETE FROM NOTIFICHE WHERE IdOrdine = ? AND IdFattorino = ?");
  if(
       !$stmt
    || !$stmt->bind_param("ii", $idordine, $idfattorino)
    || !$stmt->execute())
  {
    $output["errore"] = "errore";
    closeConnection($conn);
    die(json_encode($output));
  }
  $stmt->close();

  print json_encode("success");
  closeConnection($conn);

}

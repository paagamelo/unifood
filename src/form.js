//Aggiunge al form il tipo di account
function addAccountType(form, a_type){
  var atype = document.createElement("input"); // Creo un elemento di input che verrà usato come campo di output per il tipo di account
  form.appendChild(atype); // Aggiungo un nuovo elemento al form
  atype.name = "atype";
  atype.type = "hidden"
  atype.value = a_type;
}

//Cripta la password e invia il form
function formhash(form, password) {
   var p = document.createElement("input"); // Creo un elemento di input che verrà usato come campo di output per la password criptata
   form.appendChild(p); // Aggiungo un nuovo elemento al form
   p.name = "p";
   p.type = "hidden"
   p.value = hex_sha512(password.value); //Creo la password criptata e la assegno al nuovo campo
   password.value = ""; // Mi assicuro che la password non venga inviata in chiaro
   form.submit();
}

//Cripta la password (anche quella di conferma)
function register_formhash(form,pwd,c_pwd) {
   c_pwd.value = ""; // Mi assicuro che la 'conferma password' non venga inviata in chiaro
   formhash(form,pwd);
}

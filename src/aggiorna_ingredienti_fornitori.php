<?php
require 'db_connect.php';
require 'common_queries.php';
require 'login_functions.php';
require 'checkDati.php';

function undoIngredientiPiatto(&$conn, $ingrediente, $fornitore, $cucina) {
  $stmt = $conn->prepare(
    " DELETE FROM PIATTI_INGREDIENTI
      WHERE IdIngrediente = ?
      AND IdFornitore = ?
      AND IdPiattoInFornitore IN (SELECT p.IdPiattoInFornitore
                                  FROM PIATTI p
                                  WHERE p.IdFornitore = ?
                                  AND p.IdCucina = ?)");
  if(  !$stmt
    || !$stmt->bind_param("ssss", $ingrediente, $fornitore, $fornitore, $cucina)
    || !$stmt->execute())
  {
    return false;
  }
  return true;
}

function checkRimozione($cucine, $ingrediente, $id){
  return checkId($cucine) && checkId($ingrediente) && checkId($id);
}

function checkAggiorna($dati, $id){
  $errore = true;
  foreach ($dati as $idx) {
    $idx["prezzo"] = str_replace(",",".",$idx["prezzo"]);
    if(!checkId($idx["cucina"]) || !checkId($idx["ingrediente"]) || !checkId($id) || !checkCosto($idx["prezzo"])) $errore = false;
  }
  return $errore;
}

function checkAggiungi($cucine, $ingrediente, $costo, $id){
  return checkId($cucine) && checkId($ingrediente) && checkCosto($costo) && checkId($id);
}

sec_session_start(); //Avvio sessione php sicura
if(loggedAs($conn, "fornitori") && isset($_POST["request"])) { //Login come fornitore effettuato e richiesta in post valida

   if ($conn->connect_error) die("error");

   $state = "";
   $id = $_SESSION["user_id"];

   switch ($_POST["request"]) {
      case "rimuovi": //Rimozione ingrediente

        $ingrediente = $_POST["ingrediente"];
        $cucina = $_POST["cucina"];

        if(!checkRimozione($cucina, $ingrediente, $id)) die(closeConnectionAndReturn($conn,"dati-invalidi"));
        if(!undoIngredientiPiatto($conn,$ingrediente,$id,$cucina)) die(closeConnectionAndReturn($conn,"error"));
        $stmt = $conn->prepare("DELETE FROM FORNITURE_INGREDIENTI WHERE IdFornitore = ? AND IdCucina = ? AND IdIngrediente = ?");
        if($stmt == false) die(closeConnectionAndReturn($conn,"error"));
        $stmt->bind_param("sss", $id, $cucina, $ingrediente);
        $state = ($stmt->execute() == false ? "error" : "ok");
        $stmt->close();

        print $state;
        break;

      case "aggiorna": //Rimozione ingrediente

        $dati = $_POST["dati"];
        $state = "ok";
        if(empty($dati)) die(closeConnectionAndReturn($conn,"dati-invalidi"));
        if(!checkAggiorna($dati, $id)) die(closeConnectionAndReturn($conn,"dati-invalidi"));
        $stmt = $conn->prepare("UPDATE FORNITURE_INGREDIENTI SET CostoAggiunta = ? WHERE IdFornitore = ? AND IdCucina = ? AND IdIngrediente = ?");
        if($stmt == false) die(closeConnectionAndReturn($conn,"error"));
        $stmt->bind_param("ssss", $prezzo, $id, $cucina, $ingrediente);
        foreach ($dati as $riga) {
          $prezzo = $riga["prezzo"];
          $cucina = $riga["cucina"];
          $ingrediente = $riga["ingrediente"];
          if($stmt->execute() == false) $state = "query-error";
        }
        $stmt->close();
        print $state;
        break;

        case "aggiungi": //Aggiungi ingrediente

          $cucina = trim($_POST["cucina"]);
          $ingrediente = trim($_POST["ingrediente"]);
          $costo = str_replace(",",".",trim($_POST["costo"]));
          if(!checkAggiungi($cucina,$ingrediente,$costo, $id)) die(closeConnectionAndReturn($conn,"dati-invalidi"));
          $check = checkCucinaInFornitore($conn,$id,$cucina);
          if(isset($check["Errore"])) die(closeConnectionAndReturn($conn,"error"));
          else if(!$check["Result"]) die(closeConnectionAndReturn($conn,"dati-invalidi"));
          $stmt = $conn->prepare("INSERT INTO FORNITURE_INGREDIENTI (IdFornitore, IdCucina, IdIngrediente, CostoAggiunta) VALUES (?,?,?,?)");
          if($stmt == false) die(closeConnectionAndReturn($conn,"error"));
          $stmt->bind_param("ssss", $id, $cucina, $ingrediente, $costo);
          $state = ($stmt->execute() == false ? "error" : "ok");

          $stmt->close();
          print $state;
          break;

        case "crea-categoria": //Crea categoria

          $categoria = trim($_POST["categoria"]);
          if(!(checkDimension($categoria,1,25) && checkId($id))) die(closeConnectionAndReturn($conn,"dati-invalidi"));
          $check = checkCategoriaExists($conn,$categoria);
          if(isset($check["Errore"])) die(closeConnectionAndReturn($conn,"error"));
          else if($check["Result"]) die(closeConnectionAndReturn($conn,"yet-present"));
          $stmt = $conn->prepare("INSERT INTO CATEGORIE_INGREDIENTI (Nome) VALUES (?)");
          if($stmt == false) die(closeConnectionAndReturn($conn,"error"));
          $stmt->bind_param("s", $categoria);
          if($stmt->execute() == false) die(closeConnectionAndReturn($conn,"error"));
          $state = getIdCategoria($conn,$categoria);

          $stmt->close();
          print $state;
          break;

        case "crea-ingrediente": //Crea ingrediente

          $categoria = trim($_POST["categoria"]);
          $ingrediente = trim($_POST["ingrediente"]);
          if(!(checkDimension($ingrediente,1,25) && checkId($id) && checkId($categoria))) die(closeConnectionAndReturn($conn,"dati-invalidi"));
          $check = checkIngredienteExists($conn,$ingrediente);
          if(isset($check["Errore"])) die(closeConnectionAndReturn($conn,"error"));
          else if($check["Result"]) die(closeConnectionAndReturn($conn,"yet-present"));
          $strutturale = ($categoria == 0 ? 0 : 1);
          if(!$strutturale) $categoria = NULL;
          $stmt = $conn->prepare("INSERT INTO INGREDIENTI (Nome, IdCategoria, StrutturaleYN) VALUES (?,?,?)");
          if($stmt == false) die(closeConnectionAndReturn($conn,"error"));
          $stmt->bind_param("sss", $ingrediente, $categoria, $strutturale);
          if($stmt->execute() == false) die(closeConnectionAndReturn($conn,"error"));
          $state = getIdIngrediente($conn,$ingrediente);

          $stmt->close();
          print $state;
          break;
  }
  closeConnection($conn);
} else die(closeConnectionAndReturn($conn,"error"));
?>

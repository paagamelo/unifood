<?php
require 'db_connect.php';
require 'login_functions.php';

sec_session_start(); //Avvio sessione php sicura
if(login_check($conn) == true) { //Login effettuato
  if(isset($_POST["request"])){
   if ($conn->connect_error) {
     $output["Errore"] = "Errore di comunicazione con il server";
     die(json_encode($output));
   }

   $output = array();
   $id = $_SESSION["user_id"];
   $atype = $_SESSION["atype"];
   $table = strtoupper($atype);

   switch ($_POST["request"]) {
      case "dati": //Richiesti username e numero di telefono (solo username se si tratta di un fornitore)

        if($atype == "fornitori" || $atype == "admin"){ //Richiesta fornitori
          $stmt = $conn->prepare("SELECT Username FROM ".$table." WHERE Id = ?");
          $stmt->bind_param("s", $id);
          if(!$stmt->execute()) $output["Errore"] = "Errore";
          else{ //Non si sono verificati errori
            $stmt->bind_result($output["Username"]);
            $stmt->fetch();
          }
        } else if ($atype == "fattorini") { //Richiesta fattorini
          $stmt = $conn->prepare("SELECT IdFornitore, Telefono, Username FROM ".$table." WHERE Id = ?");
          $stmt->bind_param("s", $id);
          if(!$stmt->execute()) $output["Errore"] = "Errore";
          else{ //Non si sono verificati errori
            $stmt->bind_result($output["Datore"], $output["Telefono"], $output["Username"]);
            $stmt->fetch();
            $output["Datore"] = ($output["Datore"] == null ? 0 : $output["Datore"]);
          }
          $stmt->close();
          //Prendo tutti i fornitori
          $output["Datori"][0] = "Nessuno";
          $stmt = $conn->prepare("SELECT Id, Nome FROM FORNITORI");
          if(!$stmt->execute()) $output["Errore"] = "Errore";
          else{ //Non si sono verificati errori
            $result = $stmt->get_result();
            if($result != false) while($row = $result->fetch_assoc()) $output["Datori"][$row["Id"]] = $row["Nome"];
            else $output["Errore"] = "Errore";
          }
        } else { //Richiesta clienti
          $stmt = $conn->prepare("SELECT Telefono, Username FROM ".$table." WHERE Id = ?");
          $stmt->bind_param("s", $id);
          if(!$stmt->execute()) $output["Errore"] = "Errore";
          else{ //Non si sono verificati errori
            $stmt->bind_result($output["Telefono"], $output["Username"]);
            $stmt->fetch();
          }
        }

        $stmt->close();
        print json_encode($output);
        break;

      case "carte": //Richiesti metodi di pagamento

        if($atype == "clienti"){ //Richiesta valida solo per i clienti
          $stmt = $conn->prepare("SELECT Info FROM METODI_DI_PAGAMENTO WHERE IdCliente = ?");
          $stmt->bind_param("s", $id);
          if(!$stmt->execute()) $output["Errore"] = "Errore";
          else{ //Non si sono verificati errori
            $result = $stmt->get_result();
            if($result != false){ //Non si sono verificati errori
              $i = 1;
              while($row = $result->fetch_assoc()) { //Inserimento dati delle carte in un array
                $num = substr($row["Info"],-3);
                $tipo = substr($row["Info"],0,-3);
                $output[$i]["Numero"] = $num;
                $output[$i]["Tipo"] = $tipo;
                $i++;
              }
              //Se la condizione è vera, allora non sono stati salvati mezzi di pagamento
              if($i == 1) $output["Errore"] = "Nessun metodo di pagamento disponibile";
            }
          }
          $stmt->close();
        } else $output["Errore"] = "Errore";

        print json_encode($output);
        break;
    }
    closeConnection($conn);
  }
} else { //Login non effettuato
  $output["Errore"] = "Errore di comunicazione con il server";
  die(json_encode(closeConnectionAndReturn($conn,$output)));
}
?>

<?php
require 'checkDati.php';
//Ritorna TRUE se è stato passato un valore valido per l'errore, altrimenti FALSE
function checkError(){
  $ok = false;
  if(isset($_GET['Errore'])){
    switch($_GET['Errore']){
      case 1: $ok = true; break;
      case 2: $ok = true; break;
      case 3: $ok = true; break;
      default: $ok = false; break;
    }
  }
  return $ok;
}

if(isset($_GET['Atype']) && checkAtype($_GET['Atype'],false)){
  $atype = $_GET['Atype'];
?>
<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="./form_style.css">
  <script src="./sha512.js"></script>
  <script src="./form.js"></script>
  <script src="./login.js"></script>
  <script src="./checkDati.js"></script>

  <title>Login
  <?php
    switch($atype){
      case "clienti":?>Clienti<?php break;
      case "fornitori":?>Fornitori<?php break;
      case "fattorini":?>Fattorini<?php break;
      case "admin":?>Admin<?php break;
    }
  ?>
  </title>
</head>
<body>
  <div class="container">
    <?php
    if(checkError()) {?>
      <div class="alert alert-danger alert-php" role="alert">
        <h2>Operazione non riuscita!</h2>
    <?php
       switch($_GET['Errore']){
         case 1: ?><p>Errore nel comunicare con il server</p><?php break;
         case 2: ?><p>Hai effettuato troppi tentativi sbagliati di login.</br>Account momentaneamente disabilitato</p><?php break;
         case 3: ?><p>Email o password sbagliati</p><?php break;
       }
       ?></div><?php
    }
    ?>
    <header><h1>Login
      <?php
        switch($atype){
          case "clienti":?>Clienti<?php break;
          case "fornitori":?>Fornitori<?php break;
          case "fattorini":?>Fattorini<?php break;
          case "admin":?>Admin<?php break;
        }
      ?></h1></header>
    <form action="process_login.php" method="post" name="login_form">
      <div class="form-group">
        <label for="email">E-mail:</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="e-mail" maxlength="30"/>
      </div>
      <div class="form-group">
        <label for="password">Password:</label>
        <input type="password" class="form-control" id="password" name="password" placeholder="" maxlength="15"/>
      </div>

      <?php
        switch($atype){
          case "clienti":?><button type="submit" class="btn btn-default" onclick="login('clienti');"><?php break;
          case "fornitori":?><button type="submit" class="btn btn-default" onclick="login('fornitori');"><?php break;
          case "fattorini":?><button type="submit" class="btn btn-default" onclick="login('fattorini');"><?php break;
          case "admin":?><button type="submit" class="btn btn-default" onclick="login('admin');"><?php break;
        }
      ?>
      Login</button>
    </form>
  </div>
  <?php
    if($atype != "admin"){?>
  <div>
        <p>Non sei ancora registrato?
        <?php
        switch($atype){
          case "clienti":?><a href="./registrazione.php?Atype=clienti"><?php break;
          case "fornitori":?><a href="./registrazione.php?Atype=fornitori"><?php break;
          case "fattorini":?><a href="./registrazione.php?Atype=fattorini"><?php break;
        }
        ?>
        Registrati!</a></p>
        <p>Non sei un
      <?php
      switch($atype){
        case "clienti":?>cliente?<?php break;
        case "fornitori":?>fornitore?<?php break;
        case "fattorini":?>fattorino?<?php break;
      }
      ?>
      Accedi come
      <?php
      $num_print = 0;

      if($atype != "clienti"){ $num_print++; ?> <a href="./login.php?Atype=clienti">cliente</a><?php }
      if($num_print == 1){ $num_print++; ?> o <?php }
      if($atype != "fornitori") { $num_print++; ?> <a href="./login.php?Atype=fornitori">fornitore</a><?php }
      if($num_print == 1){ $num_print++; ?> o <?php }
      if($atype != "fattorini"){ $num_print++; ?> <a href="./login.php?Atype=fattorini">fattorino</a><?php }
      if($num_print  == 1){ $num_print++; ?> o <?php }
      ?>
    </p>
  </div>
<?php } ?>
</body>
</html>
<?php } else print 'Invalid request'; ?>

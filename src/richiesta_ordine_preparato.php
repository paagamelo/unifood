<?php
header('Content-Type: application/json');

require 'db_connect.php';
require 'login_functions.php';
require 'informazioni_ordini_fornitori.php';

sec_session_start();
$output = array();

if($conn->connect_error) {
  $output["errore"] = "errore";
  die(json_encode($output));
}

if(!loggedAs($conn, 'fornitori')) {
  $output["errore"] = "errore";
  closeConnection($conn);
  die(json_encode($output));
}

if(isset($_POST['idordine']) && isset($_POST['idfattorino'])) {

  $idfornitore = $_SESSION['user_id'];
  $idordine = $_POST['idordine'];
  $idfattorino = $_POST['idfattorino'];

  if(!checkFornitoreExists($conn, $idfornitore)) {
    $output["errore"] = "errore";
    closeConnection($conn);
    die(json_encode($output));
  }

  $stmt = $conn->prepare("SELECT * FROM ORDINI WHERE Id = ? AND IdFornitore = ?");
  if(
       !$stmt
    || !$stmt->bind_param("ii", $idordine, $idfornitore)
    || !$stmt->execute())
  {
    $output["errore"] = "errore";
    closeConnection($conn);
    die(json_encode($output));
  }

  $result = $stmt->get_result();
  $stmt->close();

  if($result->num_rows == 0) {
    $output["errore"] = "errore";
    closeConnection($conn);
    die(json_encode($output));
  }

  $ord = $result->fetch_assoc();

  $idcliente = $ord["IdUtente"];
  $orario = $ord["OraConsegna"];

  if($idfattorino == 'altro') {
    $stmt = $conn->prepare("UPDATE ORDINI SET Stato='Consegnato' WHERE Id = ? AND IdFornitore = ?");
    if(
         !$stmt
      || !$stmt->bind_param("ii", $idordine, $idfornitore)
      || !$stmt->execute())
    {
      $output["errore"] = "aerrore";
      closeConnection($conn);
      die(json_encode($output));
    }

    $stmt = $conn->prepare("DELETE FROM NOTIFICHE WHERE IdOrdine = ?");
    if(
         !$stmt
      || !$stmt->bind_param("i", $idordine)
      || !$stmt->execute())
    {
      $output["errore"] = "4errore";
      closeConnection($conn);
      die(json_encode($output));
    }
    $stmt->close();

    print json_encode("success");
    closeConnection($conn);

  } else {

    $stmt = $conn->prepare("SELECT * FROM FATTORINI WHERE Id = ? AND IdFornitore = ?");
    if(
         !$stmt
      || !$stmt->bind_param("ii", $idfattorino, $idfornitore)
      || !$stmt->execute())
    {
      $output["errore"] = "errore";
      closeConnection($conn);
      die(json_encode($output));
    }

    if($stmt->get_result()->num_rows == 0) {
      $stmt->close();
      $output["errore"] = "errore";
      closeConnection($conn);
      die(json_encode($output));
    }
    $stmt->close();

    $stmt = $conn->prepare("UPDATE ORDINI SET IdFattorino = ?, Stato = 'In consegna' WHERE Id = ? AND IdFornitore = ?");
    if(
         !$stmt
      || !$stmt->bind_param("iii", $idfattorino, $idordine, $idfornitore)
      || !$stmt->execute())
    {
      $output["errore"] = "errore";
      closeConnection($conn);
      die(json_encode($output));
    }
    $stmt->close();

    /* manage notifications */

    $stmt = $conn->prepare("SELECT * FROM NOTIFICHE WHERE IdOrdine = ? AND IdUtente = ?");
    if(
         !$stmt
      || !$stmt->bind_param("ii", $idordine, $idcliente)
      || !$stmt->execute())
    {
      $output["errore"] = "1errore";
      closeConnection($conn);
      die(json_encode($output));
    }

    $result = $stmt->get_result();
    $stmt->close();
    if($result->num_rows > 0) {
      $notifica = $result->fetch_assoc();

      $newtext = substr($notifica["Testo"], 0, strlen($notifica["Testo"])-16)." in consegna";
      $idnotifica = $notifica["Id"];

      $stmt = $conn->prepare("UPDATE NOTIFICHE SET Testo = ?, LettaYN  = '0' WHERE Id = ?");
      if(
           !$stmt
        || !$stmt->bind_param("si", $newtext, $idnotifica)
        || !$stmt->execute())
      {
        $output["errore"] = "2errore";
        closeConnection($conn);
        die(json_encode($output));
      }
      $stmt->close();
    }

    $notifica_fattorino = "Hai una nuova consegna per le ".$orario;

    $stmt = $conn->prepare("INSERT INTO NOTIFICHE(Testo, LettaYN, IdFattorino, IdOrdine) VALUES(?, '0', ?, ?)");
    if(
         !$stmt
      || !$stmt->bind_param("sii", $notifica_fattorino, $idfattorino, $idordine)
      || !$stmt->execute())
    {
      $output["errore"] = "3errore";
      closeConnection($conn);
      die(json_encode($output));
    }
    $stmt->close();

    $stmt = $conn->prepare("DELETE FROM NOTIFICHE WHERE IdOrdine = ? AND IdFornitore = ?");
    if(
         !$stmt
      || !$stmt->bind_param("ii", $idordine, $idfornitore)
      || !$stmt->execute())
    {
      $output["errore"] = "4errore";
      closeConnection($conn);
      die(json_encode($output));
    }
    $stmt->close();


    print json_encode("success");
    closeConnection($conn);
  }
}

?>

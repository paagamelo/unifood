<?php
//header('Content-Type: application/json');

require 'informazioni_ordini_clienti.php';
require 'db_connect.php';
require 'login_functions.php';

sec_session_start(); //Avvio sessione php sicura

if(login_check($conn) == true) { //Login effettuato
  if(isset($_POST["request"]))
  {
   if ($conn->connect_error) {
     $output["Errore"] = "Errore di comunicazione con il server";
     die(json_encode($output));
   }

   $output = array();
   $id = $_SESSION["user_id"];

   switch ($_POST["request"]) {
      case "ordini":

       $ordini = getOrdini($id,$conn);
       if(!empty($ordini["Errore"])) die(json_encode($ordini));
       $piatti = getPiatti($id,$conn);
       if(!empty($piatti["Errore"])) die(json_encode($piatti));
       $modifiche = getModifiche($id,$conn);
       if(!empty($modifiche["Errore"])) die(json_encode($modifiche));

       foreach ($ordini as $ordine) { //Per ogni ordine
         $id = $ordine["Id"];
         unset($ordine["Id"]); //Elimino voci inutili dagli ordini
         $output[$id] = $ordine; //Creo array Ordini in array output
         //$output[$id]["Prezzo"] = 0;
       }

       foreach ($piatti as $piatto ) { //Per ogni piatto
         $ordine = $piatto["Ordine"];
         $id = $piatto["IdPiatto"];
         //if(empty($output[$ordine]["Fornitore"]))
           //$output[$ordine]["Fornitore"] = $piatto["Fornitore"]; //Indico il nome del fornitore dell'ordine, se non è già noto
         //$output[$ordine]["Prezzo"] += ($piatto["Prezzo"] * $piatto["Quantita"]); //Aggiunto il costo all'ordine
         unset($piatto["Ordine"],$piatto["IdPiatto"]); //Elimino voci inutili dai piatti
         $output[$ordine]["Piatti"][$id] = $piatto; //Creo array piatti per ogni ordine
        }

       foreach ($modifiche as $modifica ) { //Per ogni modifica
         $ordine = $modifica["Ordine"];
         $piatto = $modifica["IdPiatto"];
         $segno = ", ".($modifica["Aggiunta"] ? "+" : "-"); //Prefisso della modifica
         //$prezzo = $modifica["CostoAggiunta"] * $output[$ordine]["Piatti"][$piatto]["Quantita"]; //Calcolo prezzo per aggiunta
         $stringa_modifica = $segno." ".$modifica["Ingrediente"];
         if(empty($output[$ordine]["Piatti"][$piatto]["Modifiche"])) //Se non esistevano già modifiche al piatto, assegno ad essa il valore di questa modifica
           $output[$ordine]["Piatti"][$piatto]["Modifiche"] = $stringa_modifica;
         else $output[$ordine]["Piatti"][$piatto]["Modifiche"].= $stringa_modifica; //Aggiungo la modifica al piatto
         //if($modifica["Aggiunta"]) $output[$ordine]["Prezzo"] += $prezzo; //Aumento il prezzo totale dell'ordine
        }

        print json_encode($output);
        break;
     }
   $conn->close();
 }
} else { //Login non effettuato
  $output["Errore"] = "Errore di comunicazione con il server";
  die(json_encode($output));
}
?>

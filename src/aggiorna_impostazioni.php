<?php
require 'checkAggiornaDati.php';
require 'db_connect.php';
require 'login_functions.php';

sec_session_start(); //Avvio sessione php sicura
if(login_check($conn) && isset($_POST["request"])) { //Login effettuato e richiesta in post valida

   if ($conn->connect_error) die("error");

   $state = "";
   $id = $_SESSION["user_id"];
   $atype = $_SESSION["atype"];
   $table = strtoupper($atype);

   switch ($_POST["request"]) {
      case "dati": //Aggiornamento di username e/o telefono

        $telefono = (($atype == "fornitori" || $atype == "admin") ? null : $_POST["telefono"]);
        $username = $_POST["username"];

        if(!checkDati($telefono,$username,$id,$atype)) die(closeConnectionAndReturn($conn,"dati-invalidi"));
        if($atype == "fornitori" || $atype == "admin"){ //Richiesta fornitori o admin
          $stmt = $conn->prepare("UPDATE ".$table." SET Username = ? WHERE Id = ?");
          $stmt->bind_param("ss", $username, $id);
        } else if($atype == "fattorini"){ //Richiesta fattorini
          $datore = $_POST["datore"];
          if($datore != null && !is_numeric($datore)) die(closeConnectionAndReturn($conn,"dati-invalidi"));
          $stmt =  ($datore == null ?
                    $conn->prepare("UPDATE ".$table." SET Username = ?, Telefono = ?, IdFornitore = NULL WHERE Id = ?") :
                    $conn->prepare("UPDATE ".$table." SET Username = ?, Telefono = ?, IdFornitore = ? WHERE Id = ?"));
          if ($datore == null) $stmt->bind_param("sss", $username, $telefono, $id);
          else $stmt->bind_param("ssss", $username, $telefono, $datore, $id);
        } else { //Richiesta clienti
          $stmt = $conn->prepare("UPDATE ".$table." SET Telefono = ?, Username = ? WHERE Id = ?");
          $stmt->bind_param("sss", $telefono, $username, $id);
        }

        $state = ($stmt->execute() == false ? "error" : "ok");

        $stmt->close();
        print $state;
        break;

      case "rimuovi-carta": //Rimozione di un metodo di pagamento

        if($atype == "clienti"){ //Richiesta valida solo per i clienti
          $numero = substr($_POST["numero"],-3);
          $tipo = $_POST["tipo"];
          if(!checkCarta($tipo, $numero, $id)) die(closeConnectionAndReturn($conn,"dati-invalidi"));

          $info = $tipo.$numero;
          $stmt = $conn->prepare("DELETE FROM METODI_DI_PAGAMENTO WHERE IdCliente = ? AND Info = ?");
          $stmt->bind_param("ss", $id, $info);
          $state = ($stmt->execute() == false ? "error" : "ok");

          $stmt->close();
        } else $state = "error";

        print $state;
        break;

      case "aggiungi-carta": //Aggiungi carta

        if($atype == "clienti"){ //Richiesta valida solo per i clienti

          $numero = substr($_POST["numero"],-3);
          $tipo = $_POST["tipo"];
          if(!checkCarta($tipo, $numero, $id)) die(closeConnectionAndReturn($conn,"dati-invalidi"));
          $info = $tipo.$numero;
          $state = checkCartaNonPresente($conn,$info, $id); //Controlla se la carta è già stata inserita
          if($state != "") die(closeConnectionAndReturn($conn,$state));
          $stmt = $conn->prepare("INSERT INTO METODI_DI_PAGAMENTO (IdCliente, Info) VALUES (?,?)");
          $stmt->bind_param("ss", $id, $info);
          $state = ($stmt->execute() == false ? "error" : "ok");

          $stmt->close();
        } else $state = "error";

        print $state;
        break;

      case "password": //Modifica password

        $nuova = $_POST["nuova"];
        $attuale = $_POST["attuale"];
        if(!checkPasswords($nuova, $attuale, $id)) die(closeConnectionAndReturn($conn,"dati-invalidi"));
        $state = checkPasswordCorretta($conn,$attuale,$id,$table); //Controlla se la password inserita è corretta
        if($state != "") die(closeConnectionAndReturn($conn,$state));
        $result = cripta_password($nuova);
        $stmt = $conn->prepare("UPDATE ".$table." SET Password = ?, Salt = ? WHERE Id = ?");
        $stmt->bind_param("sss", $result["Password"], $result["Salt"], $id);
        $state = ($stmt->execute() == false ? "error" : "ok");

        $stmt->close();
        print $state;
        break;
    }
    closeConnection($conn);
} else die(closeConnectionAndReturn($conn,"error"));
?>

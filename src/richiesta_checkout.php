<?php

function getInfoOrdine(&$conn, $idfornitore, $piatti) {
  $info_piatti = array();
  foreach($piatti as $piatto_) {
    getInfoPiatto($conn, $idfornitore, $piatto_["id"], $piatto);
    $k = count($info_piatti);//$piatto["IdPiattoInFornitore"];
    $info_piatti[$k] = array();
    $info_piatti[$k]["id"] = $piatto["IdPiattoInFornitore"];
    $info_piatti[$k]["nome"] = $piatto["Nome"];
    $info_piatti[$k]["quantita"] = $piatto_["quantita"];
    //$info_piatti[$k]["modifiche"] = array();
    if(isset($piatto_["modifiche"])) {
      $info_piatti[$k]["modifiche"] = array();
      $ingredienti = array();
      getIngredientiPiatto($conn, $idfornitore, $piatto["IdPiattoInFornitore"], $ingredienti);
      $idingredienti = array_map(function($i) { return $i["Id"]; }, $ingredienti);
      $modifiche = array();
      getInfoIngredienti($conn, array_map(function($i) { return $i["id"]; },$piatto_["modifiche"]), $piatto["IdCucina"], $idfornitore, $modifiche);
      foreach($modifiche as $modifica) {
        $id = $modifica["Id"];
        $info_piatti[$k]["modifiche"][$id] = array();
        $info_piatti[$k]["modifiche"][$id]["id"] = $id;
        $info_piatti[$k]["modifiche"][$id]["nome"] = $modifica["Nome"];
        $info_piatti[$k]["modifiche"][$id]["isAddition"] = !in_array($id, $idingredienti);
      }
    }
  }
  $_SESSION['pendingorder']['piatti'] = $info_piatti;
}

require 'login_functions.php';
require 'check_ordine.php';
require 'db_connect.php';
sec_session_start(); //Avvio sessione php sicura

if($conn->connect_error) {
  print "errore di comunicazione";
  die();
}

if($_SERVER['REQUEST_METHOD'] == 'POST'  && isset($_POST['idfornitore']) && isset($_POST['piatti'])) {

  $idfornitore = $_POST['idfornitore'];
  $piatti = $_POST['piatti'];

  if(!checkOrdine($conn, $idfornitore, $piatti, $output)) {
    print $output["errore"];
    closeConnection($conn);
    die();
  }

  $logged = login_check($conn);

  $_SESSION['pendingorder']['idfornitore'] = $idfornitore;
  //$_SESSION['pendingorder']['piatti'] = $piatti;
  getInfoOrdine($conn, $idfornitore, $piatti);

  if(!$logged) {
    $_SESSION['pendingorder']['pendingcheckout'] = true;
    print "login";
  } else {
    print "success";
  }
}
closeConnection($conn);
?>

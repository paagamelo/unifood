<?php
require 'db_connect.php';
require 'login_functions.php';
require 'common_queries.php';

sec_session_start(); //Avvio sessione php sicura
if(isset($_GET["request"])) {
   if ($conn->connect_error) {
     $output["Errore"] = "Errore di comunicazione con il server";
     die(json_encode($output));
   }

   $output = array();
   if(!isset($_SESSION["user_id"]) && !isset($_GET["id"])) {
     $output["Errore"] = "Dati inviati invalidi";
     die(json_encode(closeConnectionAndReturn($conn,$output)));
   }
   $id = (isset($_GET["id"]) ? $_GET["id"] : $_SESSION["user_id"]);
   if(!checkFornitoreExists($conn,$id))  {
     $output["Errore"] = "Dati inviati invalidi";
     die(json_encode(closeConnectionAndReturn($conn,$output)));
   }

   switch ($_GET["request"]) {
      case "cucine": //Richieste cucine di un fornitore

        getCucine($conn,$cucine);
        if(isset($cucine["errore"])) $output["Errore"] = "Errore nel comunicare con il server";
        else if(empty($cucine)) $output["Errore"] = "Nessuna cucina disponibile";
        else{ //Esistono delle cucine
          foreach ($cucine as $cucina) { //Per ogni cucina
            $fornitura = checkCucinaInFornitore($conn,$id,$cucina["Id"]); //Verifico se il fornitore fornisce questa cucina
            if(isset($fornitura["Errore"])){
              $output["Errore"] = "Errore di comunicazione con il server";
              die(json_encode(closeConnectionAndReturn($conn,$output)));
            }
            $categorie = getCategorieCiboCucina($conn,$cucina["Id"]);
            $cucina["Categorie"] = $categorie;
            if($fornitura["Result"]) $output["Fornite"][] = $cucina;
            else $output["Non-Fornite"][] = $cucina;
          }
        }

        print json_encode($output);
        break;
  }
  closeConnection($conn);
} else { //Login non effettuato
  $output["Errore"] = "Errore di comunicazione con il server";
  die(json_encode(closeConnectionAndReturn($conn,$output)));
}
?>

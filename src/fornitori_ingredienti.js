var modifying = false; //Stato pagina
var change_status = true;
var categorieIngredienti = [];
var listeCategorie = [];
var ingredienti_disponibili = [];
var prezzi = [];
var nuovi_prezzi = [];
var no_ingredienti = "<div class=\"no-ingredienti\"> Nessun ingrediente da mostrare </div>"
var angolodx = "<span class=\"symbol a-d\" aria-label=\"Espandi\"><i aria-hidden=\"true\" class=\"fas fa-angle-right fa-lg\"></i></span>";
var angologiù = "<span class=\"symbol a-g\" aria-label=\"Riduci\"><i aria-hidden=\"true\" class=\"fas fa-angle-down fa-lg\"></i></span>";
var meno = "<span class=\"symbol minus\" aria-label=\"Rimuovi\"><i aria-hidden=\"true\" class=\"fas fa-minus-circle fa-lg\"></i></span>";
var più = "<span class=\"symbol plus\" aria-label=\"Aggiungi\"><i aria-hidden=\"true\" class=\"fas fa-plus-circle fa-lg\"></i></span>";
//Carica la pagina di creazione degli ingredienti
function creaIngrediente(){
  var display_categorie = "<option value=\"0\" selected>ingredienti comuni</option>";
  for(categoria in categorieIngredienti) display_categorie += "<option value=\""+categoria+"\" >"+categorieIngredienti[categoria]+"</option>";
  display_categorie += "<option value=\"new\">altro</option>";
  $("#modal-file").load("./crea_ingrediente.html",
  function(){
    $("#categoria-ingrediente").append(display_categorie);
    $(".modal").show();
  });
}

//Aggiungi un ingrediente esistente
function aggiungiIngrediente(ingrediente){
  var valoriIngrediente = parseIdForniti(ingrediente[0]["id"]);
  var ingr = valoriIngrediente["IdIngrediente"];
  var cat = valoriIngrediente["IdCategoria"];
  var nome_ingr = ingredienti_disponibili[ingr]["Nome"];
  var nome_cat = (cat == 0 ? "ingredienti comuni" : categorieIngredienti[cat]);
  var id = "c"+cat+"i"+ingr;
  var cucine = $("li.cucina");
  var length = cucine.length;
  var cucine_inserite = 0;
  var display_cucine = "";
  for(var i = 0; i<length; i++){
    var idcucina = cucine[i]["id"];
    if($("#"+idcucina+id).length == 0){
      cucine_inserite++;
      var nome = $("#"+idcucina).find(".nome-cucina").html();
      var selected = cucine_inserite == 1 ? "selected" : "";
      display_cucine += "<option value=\""+idcucina.substr(1)+"\""+selected+">"+nome+"</option>";
    }
  }
  if(cucine_inserite > 0){
    $("#modal-file").load("./aggiungi_ingrediente.html",
    function(){
      $("#cucina").append(display_cucine);
      $("#nome-ingrediente").html(nome_ingr);
      $("#nome-categoria").html(nome_cat);
      $("#ingrediente").val(ingr);
      $("#categoria").val(cat);
      $(".modal").show();
    });
  } else {
    $("#modal-file").children().remove();
    $("#modal-file").append("<header><h3>Operazione non consentita</h3></header><div>L'ingrediente viene già fornito per tutte le tue cucine</div>");
    $(".modal").show();
  }
}

//Rimposta prezzi
function ripristinaPrezzi(){
  var ingredienti = $("#ingredienti-forniti li.ingrediente");
  var length = ingredienti.length;
  for(var i = 0; i<length; i++){ //Per ogni ingrediente
    var id = ingredienti[i]["id"];
    $("#"+id).find("span.costo-ingrediente").html(prezzi[id]);
  }
}
//Controlla se il prezzo è cambiato
function checkPrezzoCambiato(){
  var result = [];
  for(idIngrediente in nuovi_prezzi)
    if(prezzi[idIngrediente] != nuovi_prezzi[idIngrediente]) result[idIngrediente] = nuovi_prezzi[idIngrediente];
  return result;
}
//Rimuovi rimuovi fornitura
function rimuoviFornitura(ingrediente){
  var valoriIngrediente = parseId(ingrediente[0]["id"]);
  $.post("./aggiorna_ingredienti_fornitori.php",
  {
    request: "rimuovi",
    cucina : valoriIngrediente["IdCucina"],
    ingrediente : valoriIngrediente["IdIngrediente"]
  },
  function(state){
    var messaggio = "";
    if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
    else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
    if(messaggio != ""){ //Si è verificato qualche errore, mostra modal
      $("#modal-file").append("<header><h3>Operazione non riuscita</h3></header><div>"+messaggio+"</div>");
      $(".modal").show();
    } else {
      var valoriIngrediente = parseId(ingrediente[0]["id"]);
      ingrediente.remove();
      var idSezione = "c"+valoriIngrediente["IdCucina"]+"c"+valoriIngrediente["IdCategoria"];
      var items_categoria = $("#"+idSezione).find("li.ingrediente");
      if($("#"+idSezione).find("li.ingrediente").length == 0) $("#"+idSezione).after(no_ingredienti);
    }
  });
}
//Ottiene i dati relativi alla fornitura dell'ingrediente
function parseIdForniti(id){
  var risultato = [];
  var cat = id.indexOf("cat");
  var i = id.indexOf("i");
  risultato["IdCategoria"] = id.substring(cat+3,i);
  risultato["IdIngrediente"] = id.substr(i+1);
  return risultato;
}

//Ottiene i dati relativi alla fornitura dell'ingrediente
function parseId(id){
  var risultato = [];
  var c1 = id.indexOf("c");
  var c2 = id.lastIndexOf("c");
  var i = id.indexOf("i");
  risultato["IdCucina"] = id.substring(c1+1,c2);
  risultato["IdCategoria"] = id.substring(c2+1,i);
  risultato["IdIngrediente"] = id.substr(i+1);
  return risultato;
}
//Ottiene il valore di tutti i prezzi e li restituisce
function ottieniPrezzi(){
  var risultato = [];
  var ingredienti = $("#ingredienti-forniti li.ingrediente");
  var length = ingredienti.length;
  var errore = false;
  for(var i = 0; !errore && i<length; i++){ //Per ogni ingrediente
    var id = ingredienti[i]["id"];
    var prezzo = $("#"+id).find("input[name=costo]").val().trim();
    var res = checkPrezzo(prezzo);
    if(res != ""){
      risultato["Errore"] = res;
      errore = true;
    } else risultato[id] = prezzo;
  }
  return risultato;
}
//Modifica la visualizzazione dei prezzi
function togglePrezzi(){
  var ingredienti = $("#ingredienti-forniti li.ingrediente");
  var length = ingredienti.length;
  for(var i = 0; i< length; i++){ //Per ogni ingrediente
    var id = ingredienti[i]["id"];
    if(modifying){
      $("#"+id).find("label").remove();
      var costo = $("#"+id).find("input[name=costo]");
      costo.replaceWith("<span class=\"costo-ingrediente\">"+costo.val()+"</span>");
    } else {
      var costo = $("#"+id).find("span.costo-ingrediente").html();
      var nome = $("#"+id).find("span.nome-ingrediente");
      nome.after("<label class =\"visuallyhidden\" for=\"costo"+id+"\">Costo aggiunta</label>");
      $("#"+id+" .costo-ingrediente").replaceWith("<input type=\"number\" id=\"costo"+id+"\" name=\"costo\" min=\"0\" max=\"99.99\"  step=\"0.1\" value=\""+costo+"\"></input>");
    }
  }
}

//Ricarica il valore dei prezzi
function ricaricaIngredientiFornitore(locale){
  if(locale) ripristinaPrezzi();
  else{
    $("ul.cucine").empty();
    $("#ingredienti-disponibili ul.categorie-ingredienti").empty();
    categorieIngredienti = [];
    listeCategorie = [];
    prezzi = [];
    nuovi_prezzi = [];
    ingredienti_disponibili = [];
    loadIngredientiCucineFornitore();
  }
}

function toggleIngredienti(){
  if(modifying){
    var errori = "";
    nuovi_prezzi = ottieniPrezzi();
    if(nuovi_prezzi["Errore"] == undefined){ //Prezzi inseriti correttamente
      nuovi_prezzi = checkPrezzoCambiato();
      togglePrezzi();
      var elementoPresente = false;
      for(prezzo in nuovi_prezzi){
        elementoPresente = true;
        break;
      }
      if(elementoPresente){ //Almeno un prezzo cambiato
        var risultato = [];
        for(id in nuovi_prezzi){//Per ogni ingrediente modificato
          var riga = parseId(id);
          riga["Prezzi"] = nuovi_prezzi[id];
          var dati = {
            prezzo : nuovi_prezzi[id],
            cucina : riga["IdCucina"],
            ingrediente : riga["IdIngrediente"]
          };
          risultato.push(dati);

        }

        $.post("./aggiorna_ingredienti_fornitori.php",
        {
          request: "aggiorna",
          dati: risultato
        },
        function(state){
          var messaggio = "";
          if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
          else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
          else if(state == "query-error") messaggio = "Non è stato possibile aggiornare uno o più dati";
          if(messaggio != ""){ //Si è verificato qualche errore, mostra modal
            $("#modal-file").append("<header><h3>Operazione non riuscita</h3></header><div>"+messaggio+"</div>");
            $(".modal").show();
            ricaricaIngredientiFornitore(state != "query-error"); //C'è stato un errore nell'aggiornamento, ricarica i dati
          } else prezzi = nuovi_prezzi
        });
      }
    } else{ //Errore nei prezzi forniti
      change_status = false;
      $("#modal-file").append("<header><h3>Errori in Form</h3></header><ul><li>Uno o più prezzi inseriti non sono validi (0.00-99.99)!</li></ul>");
      $(".modal").show();
    }
  } else togglePrezzi();
}

//Pulsante modifica/fine dati cucine premuto
function toggleModify() {
  toggleIngredienti();
  if(change_status){
    if(modifying){
      $("button.ingredienti").html("Modifica");
      $(".minus").remove();
    } else{
      $("button.ingredienti").html("Fine");
      $("#ingredienti-forniti span.nome-ingrediente").before(meno);
    }
    modifying = !modifying;
  }
  change_status = true;
}
//Carica tutti gli ingredienti possibili
function loadIngredienti(){
  $.get("./richiesta_ingredienti_fornitori.php",
  {
    request: "ingredienti"
  },
  function(data){
    var res = JSON.parse(data);
    if(res["Errore"] == undefined){ //Non ci sono stati errori
      ingredienti_disponibili = res["Ingredienti"];
      $("#ingredienti-disponibili ul.categorie-ingredienti").append("<li class=\"categoria-ingredienti\" id=\"cat0\"><span class=\"nome-categoria-ingrediente\">ingredienti comuni</span><ul class=\"ingredienti\"></ul>");
      for(categoria in categorieIngredienti){
        var nome = "<li class=\"categoria-ingredienti\" id=\"cat"+categoria+"\" ><span class=\"nome-categoria-ingrediente\">"+categorieIngredienti[categoria]+"</span><ul class=\"ingredienti\"></ul>";
        $("#ingredienti-disponibili ul.categorie-ingredienti").append(nome);
      }
      for(ingrediente in ingredienti_disponibili){
        var infoIngrediente = ingredienti_disponibili[ingrediente];
        var idcat = (infoIngrediente["StrutturaleYN"] == 0 ? 0 : infoIngrediente["IdCategoria"]);
        var id = "cat"+ idcat + "i"+ ingrediente;
        var nome = "<li class=\"ingrediente\" id=\""+id+"\"><span class=\"nome-ingrediente\">"+infoIngrediente["Nome"]+"</span></li>";
        $("#cat"+idcat+" ul.ingredienti").append(nome);
      }
      var categorie = $("#ingredienti-disponibili li.categoria-ingredienti");
      for(categoria in categorie){
        var idcat = categorie[categoria]["id"];
        if($("#"+idcat).find("li.ingrediente").length == 0) $("#"+idcat).append(no_ingredienti);
        $("#"+idcat).find("span.nome-categoria-ingrediente").after(angolodx);
      }
      $("#ingredienti-disponibili .nome-ingrediente").before(più);
    } else ingredienti_disponibili["Errore"] = res["Errore"];
  });
}
//Carica ingredienti per ogni cucina del fornitore
function loadIngredientiCucineFornitore(){
  $.get("./richiesta_ingredienti_fornitori.php",
  {
    request: "ingredienti-fornitore"
  },
  function(data){
    var res = JSON.parse(data);
    if(res["Errore"] == undefined){ //Non ci sono stati errori
      categorieIngredienti = res["CategorieIngredienti"];

      var cucine = res["Cucine"];
      var display_cucine = "";
      for(ci in cucine){ //Per ogni cucina
        var cucina = cucine[ci]; //Cucina
        var display_cucina =
                      "<li class=\"cucina\" id=\"c"+cucina["Id"]+"\">"
                    + "<span class=\"nome-cucina\">"+cucina["Nome"]+"</span>"
                    + "<ul class=\"categorie-ingredienti\">";
        var ingredienti = cucina["Ingredienti"];
        if(!(Array.isArray(ingredienti))){ //Non ci sono Ingredienti
          display_cucina +=    "<li class=\"categoria-ingrediente\" id=\"c"+cucina["Id"]+"c0\">"
                                  + "<span class=\"nome-categoria-ingrediente\">ingredienti comuni</span>"
                                  + "<ul class=\"ingredienti\"></ul>"
                                  + no_ingredienti + "</li>";
          for(categoria in categorieIngredienti)
          display_cucina +=    "<li class=\"categoria-ingrediente\" id=\"c"+cucina["Id"]+"c"+categoria+"\">"
                                  + "<span class=\"nome-categoria-ingrediente\">"+categorieIngredienti[categoria]+"</span>"
                                  + "<ul class=\"ingredienti\"></ul>"
                                  + no_ingredienti + "</li>";
          display_cucina += "</ul></li>";
        } else{ //Esiste almeno un ingrediente
          for(i in ingredienti){ //Per ogni ingrediente
            var ingrediente = ingredienti[i];
            var idCategoria = (ingrediente["StrutturaleYN"] == 1 ? ingrediente["IdCategoria"] : 0);
            var idIngrediente = "c"+cucina["Id"]+"c"+idCategoria+"i"+ingrediente["Id"];
            var prezzo = ingrediente["CostoAggiunta"];
            prezzi[idIngrediente] = prezzo;
            var display_ingrediente = "<li class =\"ingrediente\" id=\""+idIngrediente+"\"><span class =\"nome-ingrediente\">" + ingrediente["Nome"] + "</span><span class=\"costo-ingrediente\" >"+prezzo+"</span></li>";
            if(listeCategorie[idCategoria] == undefined) listeCategorie[idCategoria] = display_ingrediente;
            else listeCategorie[idCategoria] += display_ingrediente;
          }
          if(listeCategorie[0] != undefined) display_cucina +=
                                                              "<li class=\"categoria-ingrediente\" id=\"c"+cucina["Id"]+"c0\">"
                                                            + "<span class=\"nome-categoria-ingrediente\">ingredienti comuni</span>"
                                                            + "<ul class=\"ingredienti\">"
                                                            + listeCategorie[0]
                                                            + "</ul></li>";
          for(categoria in categorieIngredienti){
            if(listeCategorie[categoria] != undefined) display_cucina +=
                                                                "<li class=\"categoria-ingrediente\" id=\"c"+cucina["Id"]+"c"+categoria+"\">"
                                                              + "<span class=\"nome-categoria-ingrediente\">"+categorieIngredienti[categoria]+"</span>"
                                                              + "<ul class=\"ingredienti\">"
                                                              + listeCategorie[categoria]
                                                              + "</ul></li>";
            else display_cucina +=    "<li class=\"categoria-ingrediente\" id=\"c"+cucina["Id"]+"c"+categoria+"\">"
                                    + "<span class=\"nome-categoria-ingrediente\">"+categorieIngredienti[categoria]+"</span>"
                                    + "<ul class=\"ingredienti\"></ul>"
                                    + no_ingredienti + "</li>";
          }

          display_cucina += "</ul></li>";
        }
        display_cucine += display_cucina;
        listeCategorie = [];
      }
      $("ul.cucine").append(display_cucine);
      $("span.nome-cucina").after(angolodx);
      loadIngredienti();
    } else $("ul.cucine").after("<div class=\"no-cucine\">"+res["Errore"]+"</div>");
  });
}

$(document).ready(function(){

  //Carica dati al primo caricamento della pagina
  loadIngredientiCucineFornitore();

  //Chiudi modal
  $(document).on("touchend mouseup", ".close", function(){
    setTimeout(function() {
      $(".modal").hide();
      $("#modal-file").children().remove();
    }, 100);
  });

  //Cliccato l'angolo destro di una cucina
  $(document).on("touchend mouseup", "ul.cucine .a-d", function(){
    var context = this;
    setTimeout(function() {
      $(context).parent().find("ul.categorie-ingredienti").show();
      $(context).replaceWith(angologiù);
    }, 100);
  });

  //Cliccato l'angolo giù di una cucina
  $(document).on("touchend mouseup", "ul.cucine .a-g", function(){
    var context = this;
    setTimeout(function() {
      $(context).parent().find("ul.categorie-ingredienti").hide();
      $(context).replaceWith(angolodx);
    }, 100);
  });

  //Cliccato l'angolo destro di una categoria
  $(document).on("touchend mouseup", "ul.categorie-ingredienti .a-d", function(){
    var context = this;
    setTimeout(function() {
      $(context).parent().find("ul.ingredienti, div.no-ingredienti").show();
      $(context).replaceWith(angologiù);
    }, 100);
  });

  //Cliccato l'angolo giù di una categoria
  $(document).on("touchend mouseup", "ul.categorie-ingredienti .a-g", function(){
    var context = this;
    setTimeout(function() {
      $(context).parent().find("ul.ingredienti, div.no-ingredienti").hide();
      $(context).replaceWith(angolodx);
    }, 100);
  });

  //Cliccato il meno
  $(document).on("touchend mouseup", ".minus", function(){
    rimuoviFornitura($(this).parent());
  });

  //Cliccato il più
  $(document).on("touchend mouseup", ".plus", function(){
    aggiungiIngrediente($(this).parent());
  });
});

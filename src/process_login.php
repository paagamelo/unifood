<?php

require 'db_connect.php';
require 'login_functions.php';
require 'checkDati.php';

sec_session_start(); // Avvio sessione php sicura
if(isset($_POST['email'], $_POST['p'], $_POST['atype']) && checkAtype($_POST['atype'],false)) {
   $email = $_POST['email'];
   $password = $_POST['p'];
   $atype = $_POST['atype'];


   $result = login($email, $password, $atype, $conn);
   closeConnection($conn);
   switch($result){
     case 0: // Login eseguito
     /*
      switch($atype){
        case "clienti" : header('Location: ./unifood.php'); break;
        case "fornitori" : print "Home in costruzione per i fornitori!"; break;
        case "fattorini" : print "Home in costruzione per i fattorini!"; break;
        case "admin" : print "Home in costruzione per gli admin!"; break;
      }*/
      if(
           $atype == "clienti"
        && isset($_SESSION['pendingorder'])
        && isset($_SESSION['pendingorder']['idfornitore'])
        && isset($_SESSION['pendingorder']['piatti'])
        && isset($_SESSION['pendingorder']['pendingcheckout'])
        && $_SESSION['pendingorder']['pendingcheckout'])
      {
        header('Location: ./checkout.php');
      } else {
        header('Location: ./unifood.php');
      }

      break;

     default: // Login fallito

      header('Location: ./login.php?Atype='.$atype.'&Errore='.$result);
      break;
  }
} else echo 'Invalid Request'; // Non sono stati passati i giusti parametri
?>

<?php
if(!isset($_GET['search'])) {
  die("Errore");
}
require 'db_connect.php';
require 'login_functions.php';
require 'common_queries.php';

sec_session_start(); //Avvio sessione php sicura
if($conn->connect_error) {
  die("Impossibile connettersi al database");
}
$logged = login_check($conn);

$search = "%".$_GET['search']."%";

$stmt = $conn->prepare("SELECT * FROM FORNITORI WHERE Nome LIKE ?");
if(
     !$stmt
  || !$stmt->bind_param("s", $search)
  || !$stmt->execute())
{
  die(closeConnectionAndReturn("errore"));
}
$result = $stmt->get_result();
$stmt->close();
$fornitori = array();
while($row = $result->fetch_assoc()) {
  $fornitori[] = $row;
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- JQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- Font Awesome (for icons) -->
  <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>
  <!-- Topbar style -->
  <link rel="stylesheet" type="text/css" href="./topbar/topbar_style.css">
  <link rel="stylesheet" href="cerca.css">
  <link rel="stylesheet" href="breadcrumb.css">
  <title>Cerca</title>
</head>
<body style="margin-top: 50px">
  <?php require "./topbar/topbar.php"; closeConnection($conn); ?>
  <header><h1>Cerca</h1>
    <ul class="breadcrumb">
      <li><a href="./unifood.php">Home</a></li>
      <li>Cerca</li>
    </ul>
  </header>
  <div class="search-container">
    <form action="./cerca.php" method='get'>
      <label for="search">Cerca:</label>
      <input type="text" placeholder="Cerca.." name="search" id="search">
      <button type="submit"><i class="fa fa-search"></i></button>
    </form>
  </div>
  <section id="fornitori">
    <?php
    if(empty($fornitori)) {
      echo "<p>Non ci sono fornitori da mostrare</p>";
    } else {
      foreach($fornitori as $k => $fornitore) {
        echo '<a href="./fornitore.php?id=' .$fornitore["Id"]. '">'
          .$fornitore["Nome"]. '<i class="fas fa-angle-right fa-lg"></i></a>';
      }
    }
    ?>
  </section>
</body>
</html>

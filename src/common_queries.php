<?php
  /* This script collects some functions used to get from DB some very common data */
  /* each function set $output["errore"] to true in case of error (query gone wrong) */

  /* NOTE THAT: these functions do not check for null parameters or other things like that */
  function getInfoCucina(&$conn, $idcucina, &$cucina) {
    $cucina = array();
    $stmt = $conn->prepare("SELECT * FROM CUCINE WHERE Id = ?");
    if(!$stmt || !$stmt->bind_param("i", $idcucina) || !$stmt->execute()) {
      $cucina["errore"] = true;
    } else {
      $result = $stmt->get_result();
      if($result->num_rows > 0) {
        $cucina = $result->fetch_assoc();
      }
      $stmt->close();
    }
  }

  function getCategorieCiboCucina($conn, $idCucina){
    $output = array();
    $stmt = $conn->prepare(
      " SELECT cc.Nome, cc.IdCategoriaInCucina as Id
        FROM CUCINE c, CATEGORIE_CIBO cc
        WHERE c.Id = ?
        AND c.Id = cc.IdCucina;");
    $stmt->bind_param("s", $idCucina);
    if(!$stmt->execute()) $output["Errore"]= "Errore nel comunicare con il server";
    else { //Non si sono verificati errori
      $result = $stmt->get_result();
      if($result != false) while($row = $result->fetch_assoc()) $output[]=$row;
      if(empty($output)) $output["Errore"] = "Nessuna categoria disponibile";
    }
    $stmt->close();
    return $output;
  }

  function checkCucinaInFornitore($conn,$idFornitore, $idCucina){
    $output = array();
    $stmt = $conn->prepare(
      " SELECT *
        FROM FORNITURE
        WHERE IdFornitore = ?
        AND IdCucina = ?
        LIMIT 1");
    $stmt->bind_param("ss", $idFornitore,$idCucina);
    if(!$stmt->execute()) $output["Errore"] = "Errore nel comunicare con il server";
    else { //Non si sono verificati errori
      $result = $stmt->get_result();
      if($result != false) $output["Result"] = !empty($result->fetch_assoc());
      else $output["Errore"] = "Errore nel comunicare con il server";
    }
    $stmt->close();
    return $output;
  }

  function getCucine(&$conn, &$cucine) {
    $cucine = array();
    $stmt = $conn->prepare("SELECT * FROM CUCINE");
    if(!$stmt || !$stmt->execute()) {
      $cucine["errore"] = true;
    } else {
      $result = $stmt->get_result();
      while($cucina = $result->fetch_assoc()) {
        $cucine[] = $cucina;
      }
      $stmt->close();
    }
  }

  function getCucineFornitore($conn, $fornitore) {
    $output = array();
    $stmt = $conn->prepare(
      " SELECT c.*
        FROM FORNITURE f, CUCINE c
        WHERE f.IdFornitore = ?
        AND c.Id = f.IdCucina");
    $stmt->bind_param("s", $fornitore);
    if(!$stmt->execute()) $output["Errore"] = "Errore nel comunicare con il server";
    else { //Non si sono verificati errori
      $result = $stmt->get_result();
      while($cucina = $result->fetch_assoc()) $output[] = $cucina;
    }
    $stmt->close();
    return $output;
  }

  function checkCucinaExists(&$conn, $idcucina) {
    getInfoCucina($conn, $idcucina, $cucina);
    return !((isset($cucina["errore"]) && $cucina["errore"]) || empty($cucina));
  }

  function getInfoFornitore(&$conn, $idfornitore, &$fornitore) {
    $fornitore = array();
    $stmt = $conn->prepare("SELECT * FROM FORNITORI WHERE Id = ?");
    if(
         !$stmt
      || !$stmt->bind_param("i", $idfornitore)
      || !$stmt->execute())
    {
      $fornitore["errore"] = true;
    } else {
      $result = $stmt->get_result();
      $stmt->close();
      while($row = $result->fetch_assoc()) {
        $fornitore = $row;
      }
    }
  }
  
  function checkPiattoExists($conn,$fornitore, $cucina, $categoria, $nome){
    $output = array();
    $stmt = $conn->prepare(
      " SELECT IdPiattoInFornitore
        FROM PIATTI
        WHERE IdFornitore = ?
        AND IdCucina = ?
        AND IdCategoriaInCucina = ?
        AND Nome = ?
        LIMIT 1");
    $stmt->bind_param("ssss", $fornitore, $cucina, $categoria, $nome);
    if(!$stmt->execute()) $output["Errore"] = "Errore nel comunicare con il server";
    else { //Non si sono verificati errori
      $stmt->bind_result($output["Result"]);
      $stmt->fetch();
    }
    $stmt->close();
    return $output;
  }

  function checkFornitoreExists(&$conn, $idfornitore) {
    getInfoFornitore($conn, $idfornitore, $fornitore);
    return !((isset($fornitore["errore"]) && $fornitore["errore"]) || empty($fornitore));
  }

  function getFornitoriInterval(&$conn, &$fornitori, $idcucina, $start, $end) {
    $fornitori = array();
    if($idcucina === "all") {
      $stmt = $conn->prepare("SELECT * FROM FORNITORI ORDER BY Id LIMIT ?, ?");
      if(!$stmt || !$stmt->bind_param("ii", $start, $end)) {
        $fornitori["errore"] = true;
        return;
      }
    } else {
      if(!checkCucinaExists($conn, $idcucina)) {
        $fornitori["errore"] = true;
        return;
      } else {
        $stmt = $conn->prepare(
          "SELECT * FROM FORNITORI, FORNITURE
          WHERE FORNITORI.Id = FORNITURE.IdFornitore
          AND FORNITURE.IdCucina = ?
          ORDER BY FORNITORI.Id
          LIMIT ?, ?");
        if(!$stmt || !$stmt->bind_param("iii", $idcucina, $start, $end)) {
          $fornitori["errore"] = true;
          return;
        }
      }
    }
    if(!$stmt->execute()) {
      $fornitori["errore"] = true;
      return;
    } else {
      $result = $stmt->get_result();
      while($fornitore = $result->fetch_assoc()) {
        $fornitori[] = $fornitore;
      }
      $stmt->close();
    }
  }
  
  function getPiattiInFornituraFornitore(&$conn, $idfornitore, &$categorie) {
    $stmt = $conn->prepare(
      "SELECT PIATTI.Nome AS NomePiatto, CATEGORIE_CIBO.Nome AS NomeCategoria, PIATTI.*
      FROM PIATTI, CATEGORIE_CIBO, FORNITURE
      WHERE PIATTI.IdFornitore = FORNITURE.IdFornitore
      AND PIATTI.IdCucina = FORNITURE.IdCucina
      AND PIATTI.IdCategoriaInCucina = CATEGORIE_CIBO.IdCategoriaInCucina
      AND PIATTI.IdCucina = CATEGORIE_CIBO.IdCucina
      AND PIATTI.IdFornitore = ?
      ORDER BY CATEGORIE_CIBO.IdCucina"
    );
    if(
         !$stmt
      || !$stmt->bind_param("i", $idfornitore)
      || !$stmt->execute()
    )
    {
      $categorie["errore"] = true;
      return;
    }
    $piatti = $stmt->get_result();
    $stmt->close();
    while($piatto = $piatti->fetch_assoc()) {
      $stmt = $conn->prepare(
        "SELECT *
        FROM INGREDIENTI, PIATTI_INGREDIENTI
        WHERE INGREDIENTI.Id = PIATTI_INGREDIENTI.IdIngrediente
        AND PIATTI_INGREDIENTI.IdFornitore = ?
        AND PIATTI_INGREDIENTI.IdPiattoInFornitore = ?");
      if(
           !$stmt
        || !$stmt->bind_param("ii", $idfornitore, $piatto["IdPiattoInFornitore"])
        || !$stmt->execute())
      {
        $categorie["errore"] = true;
        return;
      }
      $result = $stmt->get_result();
      $stmt->close();
      while($ingrediente = $result->fetch_assoc()) {
        $piatto["ingredienti"][] = $ingrediente;
      }
      $categorie[$piatto["NomeCategoria"]][] = $piatto;
      //$categorie[$piatto["NomeCategoria"]]["idcucina"] = $piatto["Id"];
    }
  }

  function getPiattiFornitore(&$conn, $idfornitore, &$categorie) {
    $categorie = array();
    $stmt = $conn->prepare(
      "SELECT PIATTI.Nome AS NomePiatto, CATEGORIE_CIBO.Nome AS NomeCategoria, PIATTI.*
      FROM PIATTI, CATEGORIE_CIBO, FORNITURE
      WHERE PIATTI.IdFornitore = FORNITURE.IdFornitore
      AND PIATTI.IdCucina = FORNITURE.IdCucina
      AND PIATTI.IdCategoriaInCucina = CATEGORIE_CIBO.IdCategoriaInCucina
      AND PIATTI.IdCucina = CATEGORIE_CIBO.IdCucina
      AND PIATTI.IdFornitore = ?
      ORDER BY CATEGORIE_CIBO.IdCucina"
    );
    if(
         !$stmt
      || !$stmt->bind_param("i", $idfornitore)
      || !$stmt->execute()
    )
    {
      $categorie["errore"] = true;
      return;
    }
    $piatti = $stmt->get_result();
    $stmt->close();
    while($piatto = $piatti->fetch_assoc()) {
      $stmt = $conn->prepare(
        "SELECT *
        FROM INGREDIENTI, PIATTI_INGREDIENTI
        WHERE INGREDIENTI.Id = PIATTI_INGREDIENTI.IdIngrediente
        AND PIATTI_INGREDIENTI.IdFornitore = ?
        AND PIATTI_INGREDIENTI.IdPiattoInFornitore = ?");
      if(
           !$stmt
        || !$stmt->bind_param("ii", $idfornitore, $piatto["IdPiattoInFornitore"])
        || !$stmt->execute())
      {
        $categorie["errore"] = true;
        return;
      }
      $result = $stmt->get_result();
      $stmt->close();
      while($ingrediente = $result->fetch_assoc()) {
        $piatto["ingredienti"][] = $ingrediente;
      }
      $categorie[$piatto["NomeCategoria"]][] = $piatto;
      //$categorie[$piatto["NomeCategoria"]]["idcucina"] = $piatto["Id"];
    }
  }

  function getIngredienti($conn) {
    $output = array();
    $stmt = $conn->prepare("SELECT * FROM INGREDIENTI");
    if(!$stmt->execute()) $output["Errore"] = "Errore nel comunicare con il server";
    else { //Non si sono verificati errori
      $result = $stmt->get_result();
      while($ingrediente = $result->fetch_assoc()) $output[] = $ingrediente;
    }
    $stmt->close();
    return $output;
  }

  function checkIngredienteExists($conn,$ingrediente){
    $output = array();
    $stmt = $conn->prepare(
      " SELECT *
        FROM INGREDIENTI
        WHERE Nome = ?
        LIMIT 1");
    $stmt->bind_param("s", $ingrediente);
    if(!$stmt->execute()) $output["Errore"] = "Errore nel comunicare con il server";
    else { //Non si sono verificati errori
      $result = $stmt->get_result();
      if($result != false) $output["Result"] = !empty($result->fetch_assoc());
      else $output["Errore"] = "Errore nel comunicare con il server";
    }
    $stmt->close();
    return $output;
  }

  function getIdIngrediente($conn,$ingrediente){
    $output = "";
    $stmt = $conn->prepare(
      " SELECT Id
        FROM INGREDIENTI
        WHERE Nome = ?
        LIMIT 1");
    $stmt->bind_param("s", $ingrediente);
    if(!$stmt->execute()) $output = "error";
    else { //Non si sono verificati errori
      $stmt->bind_result($output);
      $stmt->fetch();
    }
    $stmt->close();
    return $output;
  }

  function getIngredientiFornitore(&$conn, $idfornitore, $idcucina, &$ingredienti) {
    $ingredienti = array();
    $stmt = $conn->prepare(
      "SELECT Id, Nome, StrutturaleYN, IdCategoria, CostoAggiunta
      FROM FORNITURE_INGREDIENTI, INGREDIENTI
      WHERE FORNITURE_INGREDIENTI.IdIngrediente = INGREDIENTI.Id
      AND FORNITURE_INGREDIENTI.IdFornitore = ?
      AND FORNITURE_INGREDIENTI.IdCucina = ?");
    if(
         !$stmt
      || !$stmt->bind_param("ii", $idfornitore, $idcucina)
      || !$stmt->execute())
    {
      $ingredienti["errore"] = true;
      return;
    }
    $result = $stmt->get_result();
    $stmt->close();
    while($row = $result->fetch_assoc()) {
      $ingredienti[] = $row;
    }
  }

  function checkCategoriaExists($conn,$categoria){
    $output = array();
    $stmt = $conn->prepare(
      " SELECT *
        FROM CATEGORIE_INGREDIENTI
        WHERE Nome = ?
        LIMIT 1");
    $stmt->bind_param("s", $categoria);
    if(!$stmt->execute()) $output["Errore"] = "Errore nel comunicare con il server";
    else { //Non si sono verificati errori
      $result = $stmt->get_result();
      if($result != false) $output["Result"] = !empty($result->fetch_assoc());
      else $output["Errore"] = "Errore nel comunicare con il server";
    }
    $stmt->close();
    return $output;
  }

  function getIdCategoria($conn,$categoria){
    $output = "";
    $stmt = $conn->prepare(
      " SELECT Id
        FROM CATEGORIE_INGREDIENTI
        WHERE Nome = ?
        LIMIT 1");
    $stmt->bind_param("s", $categoria);
    if(!$stmt->execute()) $output = "error";
    else { //Non si sono verificati errori
      $stmt->bind_result($output);
      $stmt->fetch();
    }
    $stmt->close();
    return $output;
  }

  function getCategorieIngredienti($conn) {
    $output = array();
    $stmt = $conn->prepare("SELECT * FROM CATEGORIE_INGREDIENTI");
    if(!$stmt->execute()) $output["Errore"] = "Errore nel comunicare con il server";
    else { //Non si sono verificati errori
      $result = $stmt->get_result();
      while($categoria = $result->fetch_assoc()) $output[] = $categoria;
    }
    $stmt->close();
    return $output;
  }

  function getIngredientiPiatto(&$conn, $idfornitore, $idpiatto, &$ingredienti) {
    $ingredienti = array();
    $stmt = $conn->prepare(
      "SELECT Id, INGREDIENTI.Nome, StrutturaleYN, IdCategoria, CostoAggiunta
      FROM PIATTI, PIATTI_INGREDIENTI, INGREDIENTI, FORNITURE_INGREDIENTI
      WHERE PIATTI.IdFornitore = PIATTI_INGREDIENTI.IdFornitore
      AND PIATTI.IdPiattoInFornitore = PIATTI_INGREDIENTI.IdPiattoInFornitore
      AND PIATTI.IdCucina = FORNITURE_INGREDIENTI.IdCucina
      AND FORNITURE_INGREDIENTI.IdIngrediente = INGREDIENTI.Id
      AND PIATTI_INGREDIENTI.IdFornitore = FORNITURE_INGREDIENTI.IdFornitore
      AND PIATTI_INGREDIENTI.IdIngrediente = INGREDIENTI.Id
      AND PIATTI_INGREDIENTI.IdFornitore = ?
      AND PIATTI_INGREDIENTI.IdPiattoInFornitore = ?");
    if(
         !$stmt
      || !$stmt->bind_param("ii", $idfornitore, $idpiatto)
      || !$stmt->execute())
    {
      $ingredienti["errore"] = true;
      return;
    }
    $result = $stmt->get_result();
    $stmt->close();
    while($row = $result->fetch_assoc()) {
      $ingredienti[] = $row;
    }
  }

  function getInfoIngredienti(&$conn, $ids, $idcucina, $idfornitore, &$ingredienti) {
    $ingredienti = array();
    foreach($ids as $id) {
      $stmt = $conn->prepare(
        "SELECT Id, Nome, StrutturaleYN, IdCategoria, CostoAggiunta
        FROM INGREDIENTI, FORNITURE_INGREDIENTI
        WHERE INGREDIENTI.Id = FORNITURE_INGREDIENTI.IdIngrediente
        AND FORNITURE_INGREDIENTI.IdCucina = ?
        AND FORNITURE_INGREDIENTI.IdFornitore = ?
        AND Id = ?");
      if(
           !$stmt
        || !$stmt->bind_param("iii", $idcucina, $idfornitore, $id)
        || !$stmt->execute())
      {
        $ingredienti["errore"] = true;
        return;
      }
      $result = $stmt->get_result();
      $stmt->close();
      if($result->num_rows == 0) {
        $ingredienti["errore"] = true;
        return;
      }
      $ingredienti[] = $result->fetch_assoc();
    }
  }

  function getInfoPiatto(&$conn, $idfornitore, $idpiatto, &$piatto) {
    $piatto = array();
    $stmt = $conn->prepare("SELECT * FROM PIATTI WHERE IdFornitore = ? AND IdPiattoInFornitore = ?");
    if(
         !$stmt
      || !$stmt->bind_param("ii", $idfornitore, $idpiatto)
      || !$stmt->execute())
    {
      $piatto["errore"] = true;
    } else {
      $result = $stmt->get_result();
      if($result->num_rows > 0) {
        $piatto = $result->fetch_assoc();
      }
      $stmt->close();
    }
  }

?>

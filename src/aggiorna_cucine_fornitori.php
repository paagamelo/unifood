<?php
require 'db_connect.php';
require 'common_queries.php';
require 'login_functions.php';
require 'checkDati.php';


function checkDati($cucine,$aggiungere,$rimuovere){
  $id_cucine = array();
  foreach ($cucine as $cucina) $id_cucine[] = $cucina["Id"];
  foreach($aggiungere as $cucina) if(array_search($cucina,$id_cucine) === false) return false;
  foreach($rimuovere as $cucina) if(array_search($cucina,$id_cucine) === false) return false;

  return true;
}

sec_session_start(); //Avvio sessione php sicura
if(loggedAs($conn, "fornitori") && isset($_POST["request"])) { //Login come fornitore effettuato e richiesta in post valida

   if ($conn->connect_error) die("error");

   $state = "";
   $id = $_SESSION["user_id"];

   switch ($_POST["request"]) {
      case "cucine": //Aggiornamento cucine del fornitore

        $aggiungere = (isset($_POST["aggiungi"]) ? $_POST["aggiungi"] : array());
        $rimuovere = (isset($_POST["rimuovi"]) ? $_POST["rimuovi"] : array());

        getCucine($conn, $cucine);
        if(isset($cucine["error"])) die(closeConnectionAndReturn($conn,"dati-invalidi"));
        if(!checkDati($cucine, $aggiungere, $rimuovere)) die(closeConnectionAndReturn($conn,"dati-invalidi"));

        $stmt = $conn->prepare("INSERT INTO FORNITURE (IdFornitore, IdCucina) VALUES (?,?)");
        if($stmt == false) die(closeConnectionAndReturn($conn,"query-error"));
        $stmt->bind_param("ss", $id, $cucina);
        foreach ($aggiungere as $cucina) if($stmt->execute() == false) $state = "query-error";
        $stmt->close();

        $stmt = $conn->prepare("DELETE FROM FORNITURE WHERE IdFornitore = ? AND IdCucina = ?");
        if($stmt == false) die(closeConnectionAndReturn($conn,"query-error"));
        $stmt->bind_param("ss", $id, $cucina);
        foreach ($rimuovere as $cucina) if($stmt->execute() == false) $state = "query-error";
        $stmt->close();

        print $state;
        break;

      case "richiesta": //Richiesta nuova categoria

        $descrizione = $_POST["descrizione"];

        if(!checkDescrizione($descrizione)) die(closeConnectionAndReturn($conn,"dati-invalidi"));

        $stmt = $conn->prepare("INSERT INTO RICHIESTE (IdFornitore, Descrizione, ApprovataYN) VALUES (?,?,'0')");
        if($stmt == false) die(closeConnectionAndReturn($conn,"error"));
        $stmt->bind_param("ss", $id, $descrizione);
        $state = ($stmt->execute() == false ? "error" : "ok");
        $stmt->close();

        print $state;
  }
  closeConnection($conn);
} else die(closeConnectionAndReturn($conn,"error"));
?>

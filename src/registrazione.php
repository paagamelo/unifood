<!DOCTYPE html>
<?php
require 'checkDati.php';
//Ritorna TRUE se è stato passato un valore valido per l'errore, altrimenti FALSE
function checkError(){
  $ok = false;
  if(isset($_GET['Errore'])){
    switch($_GET['Errore']){
      case 1: $ok = true; break;
      case 2: $ok = true; break;
      case 3: $ok = true; break;
      default: $ok = false; break;
    }
  }
  return $ok;
}

if(isset($_GET['Atype']) && checkAtype($_GET['Atype'],true)){
  $atype = $_GET['Atype'];
?>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="./form_style.css">
  <script src="./sha512.js"></script>
  <script src="./form.js"></script>
  <script src="./registrazione.js"></script>
  <script src="./checkDati.js"></script>


  <title>Registrazione
    <?php
      switch($atype){
        case "clienti":?>Clienti<?php break;
        case "fornitori":?>Fornitori<?php break;
        case "fattorini":?>Fattorini<?php break;
      }
    ?>
</title>
</head>
<body>
  <div class="container">
    <?php
    if(isset($_GET['Errore'])) {?>
      <div class="alert alert-danger alert-php" role="alert">
        <h2>Operazione non riuscita!</h2>
    <?php
       switch($_GET['Errore']){
         case 1: ?><p>Errore nel comunicare con il server</p><?php ; break;
         case 2: ?><p>Email già registrata per un altro utente</p><?php ; break;
         case 3: ?><p>Dati di registrazione invalidi</p><?php ; break;
       }
       ?></div><?php
    }
    ?>
  <header><h1>Registrazione
    <?php
      switch($atype){
        case "clienti":?>Clienti<?php break;
        case "fornitori":?>Fornitori<?php break;
        case "fattorini":?>Fattorini<?php break;
      }
    ?>
  </h1></header>
  <form action="registra.php" method="post" name="registra_form">
  <?php
    if($atype != "fornitori"){ ?>
  <div class="form-group base">
    <label for="nome">Nome:</label>
    <input type="text" class="form-control" id="nome" name="nome" placeholder="nome" maxlength="15"  required/>
  </div>
  <div class="form-group base">
    <label for="cognome">Cognome:</label>
    <input type="text" class="form-control" id="cognome" name="cognome" placeholder="cognome" maxlength="15" required/>
  </div>
  <div class="form-group base">
    <label for="telefono">Telefono:</label>
    <input type="tel" class="form-control" id="telefono" name="telefono" placeholder="telefono" maxlength="10"/>
  </div>
  <?php } ?>
  <div class="form-group base">
    <label for="email">E-mail:</label>
    <input type="email" class="form-control" id="email" name="email" placeholder="e-mail" maxlength="30" required/>
  </div>
  <div class="form-group base">
    <label for="username">Username:</label>
    <input type="text" class="form-control" id="username" name="username" placeholder="username" maxlength="15" required/>
  </div>
  <div class="form-group base">
    <label for="password">Password:</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="" maxlength="15" required/>
  </div>
  <div class="form-group base">
    <label for="c_password">Conferma password:</label>
    <input type="password" class="form-control" id="c_password" name="c_password" placeholder="" maxlength="15" required/>
  </div>
  <?php
    if($atype == "fornitori"){
  ?>
  <div class="form-group fornitori hidden">
    <label for="nome_ristorante">Nome:</label>
    <input type="text" class="form-control" id="nome_ristorante" name="nome" placeholder="nome" maxlength="15"  required/>
  </div>
  <div class="form-group fornitori hidden">
    <label for="indirizzo">Indirizzo:</label>
    <input type="text" class="form-control" id="indirizzo" name="indirizzo" placeholder="indirizzo" maxlength="30"/>
  </div>
  <div class="form-group fornitori hidden">
    <label for="telefono_ristorante">Telefono:</label>
    <input type="tel" class="form-control" id="telefono_ristorante" name="telefono" placeholder="telefono" maxlength="10"/>
  </div>
  <div class="form-group fornitori hidden">
    <label for="speseConsegna">Spese di consegna:</label>
    <input type="number" class="form-control" id="speseConsegna" name="speseConsegna" value=0.00 />
  </div>
  <div class="form-group fornitori hidden">
    <label for="orarioApertura">Orario di apertura:</label>
    <input type="time" class="form-control" id="orarioApertura" name="orarioApertura" value="09:00"/>
  </div>
  <div class="form-group fornitori hidden">
    <label for="orarioChiusura">Orario di chiusura:</label>
    <input type="time" class="form-control" id="orarioChiusura" name="orarioChiusura" value="23:00"/>
  </div>
  <button class="btn btn-default fornitori hidden" onclick="indietro();">Indietro</button>
  <?php }
    switch($atype){
      case "clienti": ?><button type="submit" class="btn btn-default" onclick="register('clienti');"><?php break;
      case "fornitori":?><button type="submit" class="btn btn-default invia" onclick="register('fornitori');"><?php break;
      case "fattorini":?><button type="submit" class="btn btn-default" onclick="register('fattorini');"><?php break;
    }
  ?>
  Invia</button>
  </form>
  </div>
  <div class="footer">
    <p>
    Sei già registrato?
    <?php
    switch($atype){
      case "clienti":?><a href="./login.php?Atype=clienti"><?php break;
      case "fornitori":?><a href="./login.php?Atype=fornitori"><?php break;
      case "fattorini":?><a href="./login.php?Atype=fattorini"><?php break;
    }
    ?>
    Accedi!</a></p>
    <p>Non sei un
  <?php
  switch($atype){
    case "clienti":?>cliente?<?php break;
    case "fornitori":?>fornitore?<?php break;
    case "fattorini":?>fattorino?<?php break;
  }
  ?>
  Registrati come
  <?php
  $num_print = 0;

  if($atype != "clienti"){ $num_print++; ?> <a href="./registrazione.php?Atype=clienti">cliente</a><?php }
  if($num_print == 1){ $num_print++; ?> o <?php }
  if($atype != "fornitori") { $num_print++; ?> <a href="./registrazione.php?Atype=fornitori">fornitore</a><?php }
  if($num_print == 1){ $num_print++; ?> o <?php }
  if($atype != "fattorini"){ $num_print++; ?> <a href="./registrazione.php?Atype=fattorini">fattorino</a><?php }
  if($num_print  == 1){ $num_print++; ?> o <?php }
  ?>
</p>
  </div>
</body>
</html>
<?php } else print 'Invalid request'; ?>

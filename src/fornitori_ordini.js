var page_conclusi = -1;
var page_inconsegna = -1;
var showing_conclusi = false;
var showing_inconsegna = false;
var errore = false;

function displayAlert(type, message) {
  var prestring = "";
  switch(type) {
    case 'info': prestring = "Attenzione! "; break;
    case 'success': prestring = "Successo! "; break;
    case 'error': prestring = 'Errore! '; break;
  }
  $(".modal").css("display", "none");
  $('.alert.' + type + '').remove();
  $(".main").prepend('<div class="alert ' + type + '"><span class="closebtn">&times;</span>'
    + '<strong>' + prestring + '</strong>' + message + '</div>');
}

function error() {
  errore = true;
  $(".breadcrumb").after("<p>Impossibile caricare gli ordini</p>");
  $("#ordini-in-corso, #ordini-in-consegna, #ordini-conclusi").remove();
}

function ordiniToHtml(ordini) {
  var html = "";
  for(id in ordini) {
    ordine = ordini[id];

    var html_piatti = '<ul class="piatti">';
    for(piatto in ordine['piatti']) {
      piatto = ordine['piatti'][piatto];
      html_piatti += '<li><div class="piatto">' + piatto["Nome"] + " x" + piatto['Quantita'] + '</div>';

      if(Object.keys(piatto['modifiche']).length > 0) {
        html_piatti += '<ul class="modifiche">';
        for(modifica in piatto['modifiche']) {
          modifica = piatto['modifiche'][modifica];
          html_piatti += '<li><span class="isaddition ' + ((modifica['AggiuntaYN'] == '1') ? 'yes">+</span>' : 'no">-</span>') + modifica["Nome"] + '</li>';
        }
        html_piatti += '</ul>';
      }
    }
    html_piatti += '</ul>';

    html += '<tr data-id="' + ordine["Id"] + '">'
              + '<td>' + html_piatti + '</td>' // piatti
              + '<td>' + ordine['OraConsegna'] + '</td>'
              + '<td>' + (ordine['Note'] == null ? "/" : ordine["Note"]) + '</td>'
              + '<td>' + ordine['Totale'] + '</td>' //totale
              + '<td>' + ((ordine['PagatoYN'] == '1') ? "Si" : "No") + '</td>'
              + '<td>' + ordine['CNome'] + " " + ordine['CCognome'] + '</td>' // cliente
              + '<td>' + ordine['PianoConsegna'] + '</td>';

    switch(ordine['Stato']) {
      case 'In preparazione': {
        html += '<td><button type="button">Consegna</button></tr>';
        break;
      }
      case 'In consegna': {
        html += '<td>' + (ordine['FNome'] == null ? "/" : ordine['FNome']) + " "
              + (ordine['FCognome'] == null ? "/" : ordine['FCognome']) + '</td>'
              + '<td>' + ordine['Data'] + '</td>';
        break;
      }
      case 'Consegnato': {
        html += '<td>' + (ordine['FNome'] == null ? "/" : ordine['FNome']) + " "
              + (ordine['FCognome'] == null ? "/" : ordine['FCognome']) + '</td>'
              + '<td>' + ordine['Data'] + '</td>';
        break;
      }
    }

    html += '</tr>';
  }
  return html;
}

var onpageload = true;

function loadNuoviOrdini() {
  $.getJSON("richiesta_ordini_fornitori.php?request=nuovi", function(data) {
    if(data["errore"] === "errore") {
      error();
    } else if(!errore) {
      var incorso = $("#ordini-in-corso tr").length - 1;
      if(onpageload || data.length < incorso) {
        $("#ordini-in-corso table tbody").html(ordiniToHtml(data));
      } else if(data.length > incorso) {
        displayAlert('info', 'Nuovi ordini arrivati');
        $("#ordini-in-corso table tbody").hide();
        $("#ordini-in-corso table tbody").html(ordiniToHtml(data)).fadeIn('slow');
      }
      /*
      if(!onpageload && (data.length > incorso)) {
        displayAlert('info', 'Nuovi ordini arrivati');
      }
      */
      onpageload = false;
      if(data.length == 0) {
        $("#ordini-in-corso table").css("display", "none");
        if($("#ordini-in-corso .placeholder").length == 0) {
          $("#ordini-in-corso table").after('<p class="placeholder">Nessun ordine da mostrare</p>');
        }
      } else {
        $("#ordini-in-corso .placeholder").remove();
        $("#ordini-in-corso table").css("display", "table");
      }
    }
  });
}

function checkMoreVecchiOrdini(filter) {
  var contesto = "";
  var showing;
  var page;
  switch(filter) {
    case 'consegna':
      contesto = "#ordini-in-consegna";
      showing = showing_inconsegna;
      page = page_inconsegna;
      break;
    case 'conclusi':
      contesto = "#ordini-conclusi";
      showing = showing_conclusi;
      page = page_conclusi;
      break;
  }
  $.getJSON("richiesta_ordini_fornitori.php?request=more&filter=" + filter + "&page=" + page,
  function(more) {
    if(more["errore"] === "errore") {
      error();
    } else if(!errore) {
      if(more['more'] === 'true') {
        $(contesto).find(".placeholder").remove();
        if($(contesto).find(".more").length == 0) {
          $(contesto).find("table").after('<a class="more">Visualizza di più ></a>');
        }
      } else {
        $(contesto).find(".more").remove();
        $(contesto).find(".less").remove();
        if(!showing && $(contesto).find(".placeholder").length == 0) {
          $(contesto).find("table").after('<p class="placeholder">Nessun ordine da mostrare</p>');
          //$(contesto).find("header").css("display", "none");
        }
      }
    }
  });
}

function loadVecchiOrdini(filter, page) {
  var url="";
  $.getJSON("richiesta_ordini_fornitori.php?request=page&filter=" + filter + "&page=" + page,
  function(data) {
    if(data["errore"] === "errore") {
      error();
    } else if(!errore) {
      var posizione = "";
      switch(filter) {
        case 'consegna': posizione = '#ordini-in-consegna tbody'; break;
        case 'conclusi': posizione = '#ordini-conclusi tbody'; break;
      }
      $(posizione).append(ordiniToHtml(data));
    }
  });
}

function monitoraVecchiOrdini(filter) {
  var contesto = "";
  var showing;
  var page;
  switch(filter) {
    case 'consegna':
      contesto = "#ordini-in-consegna";
      showing = showing_inconsegna;
      page = page_inconsegna;
      break;
    case 'conclusi':
      contesto = "#ordini-conclusi";
      showing = showing_conclusi;
      page = page_conclusi;
      break;
  }
  if(showing) {
    $.getJSON("richiesta_ordini_fornitori.php?request=topage&filter=" + filter + "&page=" + page,
    function(data) {
      if(data["errore"] === "errore") {
        error();
      } else if(!errore) {
        console.log(data);
        $(contesto).find("tbody").html(ordiniToHtml(data));
      }
    });
  }
  checkMoreVecchiOrdini(filter);
}

function monitoraAllVecchiOrdini() {
  monitoraVecchiOrdini('consegna');
  monitoraVecchiOrdini('conclusi');
}

$(document).ready(function() {

  //displayAlert('info', '');

  $("#ordini-in-consegna table").css("display", "none");
  $("#ordini-conclusi table").css("display", "none");

  loadNuoviOrdini();
  setInterval(function() { loadNuoviOrdini(); }, 5000);

  monitoraAllVecchiOrdini()
  setInterval(function() { monitoraAllVecchiOrdini(); }, 30000);

  setInterval(function() { $(".alert.info, .alert.success").remove(); }, 30000);

  $(document).on("click", "#ordini-in-consegna .more", function() {
    page_inconsegna++;
    showing_inconsegna = true;

    loadVecchiOrdini('consegna', page_inconsegna);
    checkMoreVecchiOrdini('consegna');

    $("#ordini-in-consegna table").css("display", "table");
    if($("#ordini-in-consegna .less").length == 0) {
      $("#ordini-in-consegna table").after('<a class="less">Nascondi</a>');
    }
  });

  $(document).on("click", "#ordini-conclusi .more", function() {
    page_conclusi++;
    showing_conclusi = true;

    loadVecchiOrdini('conclusi', page_conclusi);
    checkMoreVecchiOrdini('conclusi');

    $("#ordini-conclusi table").css("display", "table");
    if($("#ordini-conclusi .less").length == 0) {
      $("#ordini-conclusi table").after('<a class="less">Nascondi</a>');
    }
  });

  $(document).on("click", "#ordini-in-consegna .less", function() {
    page_inconsegna = -1;
    showing_inconsegna = false;

    $("#ordini-in-consegna table").css("display", "none");
    $("#ordini-in-consegna tbody").html("");

    checkMoreVecchiOrdini('consegna');
    $(this).remove();
  });

  $(document).on("click", "#ordini-conclusi .less", function() {
    page_conclusi = -1;
    showing_conclusi = false;

    $("#ordini-conclusi table").css("display", "none");
    $("#ordini-conclusi tbody").html("");

    checkMoreVecchiOrdini('conclusi');
    $(this).remove();
  });

  var idordine;

  $(document).on("click", "td>button", function() {
    $(".modal").css("display", "block");
    idordine = $(this).parents("tr").attr('data-id');
  })

  //Chiudi modal
  $(document).on("click", ".close", function(){
    $(".modal").hide();
    $("#modal-file").children().remove();
  });

  $(document).on("click", '.modal input[type="submit"]', function() {
    event.preventDefault();
    var idfattorino = $("input[name='fattorino']:checked").val();
    data = {
      idfattorino: idfattorino,
      idordine: idordine
    }
    console.log(data);
    $.post("./richiesta_ordine_preparato.php",
    data,
    function(state) {
      if(state === 'success') {
        displayAlert('success', 'Ordine preparato con successo');
        loadNuoviOrdini();
        monitoraAllVecchiOrdini();
      } else {
        console.log(state);
        displayAlert('error', 'Qualcosa è andato storto')
      }
    });
  });

  $(document).on("click", ".alert .closebtn", function() {
    $(this).parent(".alert").remove();
  })

});

<?php
require 'db_connect.php';
require 'login_functions.php';
require 'common_queries.php';

sec_session_start(); //Avvio sessione php sicura
if($conn->connect_error) {
  die("Impossibile connettersi al database");
}
$logged = login_check($conn);
getCucine($conn, $cucine);
getFornitoriInterval($conn, $fornitori, "all", 0, 50);
?>
<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- JQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- Font Awesome (for icons) -->
  <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- Topbar style -->
  <link rel="stylesheet" type="text/css" href="./topbar/topbar_style.css">
  <link rel="stylesheet" href="unifood.css">
  <link rel="stylesheet" href="breadcrumb.css">
  <title>UniFood</title>
</head>
<body>
  <?php require './topbar/topbar.php'; closeConnection($conn); ?>
  <header>
    <h1>UniFood</h1>
    <ul class="breadcrumb">
      <li>Home</li>
    </ul>
  </header>
  <div class="search-container">
    <form action="./cerca.php" method='get'>
      <label for="search">Cerca:</label>
      <input type="text" placeholder="Cerca.." name="search" id="search">
      <button type="submit"><i class="fa fa-search"></i></button>
    </form>
  </div>
  <section id="cucine">
    <header><a href="./cucine.php"><h2>Cucine<i class="fas fa-angle-right fa-lg"></i></h2></a></header>
    <?php
    if(isset($cucine["errore"]) && $cucine["errore"]) {
      echo "<p>Impossibile caricare le cucine</p>";
    } else if(empty($cucine)){
      echo "<p>Nessuna cucina da mostrare</p>";
    } else {
      foreach($cucine as $key => $cucina) {
        echo'<a href="./fornitori.php?cucina='.$cucina["Id"].'">'
            .$cucina["Nome"].'<i class="fas fa-angle-right fa-lg"></i></a>';
      }
    }
    ?>
  </section>
  <section id="fornitori">
    <header><a href="./fornitori.php"><h2>Fornitori<i class="fas fa-angle-right fa-lg"></i></h2></a></header>
    <?php
    if(isset($fornitori["errore"]) && $fornitori["errore"]) {
      echo "<p>Impossibile caricare i fornitori</p>";
    } else if(empty($fornitori)){
      echo "<p>Nessun fornitore da mostrare</p>";
    } else {
      foreach($fornitori as $key => $fornitore) {
        echo'<a href="./fornitore.php?id='.$fornitore["Id"].'">'
            .$fornitore["Nome"].'<i class="fas fa-angle-right fa-lg"></i></a>';
      }
    }
    ?>
  </section>
</body>
</html>

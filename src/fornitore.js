/* This script handles the showing of the 'piatti' list for the selected category */
/* NOTE THAT: on mobile more than one category can be showed at the same time, on
   desktop ONLY one is showed at the same time */


function toggleHeight(content) {
  var visible = $(content).hasClass("visible");
  content = $(content).get(0);
  if(visible) {
    content.style.maxHeight = content.scrollHeight + "px";
  } else {
    content.style.maxHeight = null;
  }
}
  /*
  if (content.style.maxHeight && content.style.maxHeight != '0px'){
    content.style.maxHeight = null;
  } else {
    content.style.maxHeight = content.scrollHeight + "px";
  }
  */


$(document).ready(function() {
  /* loading the topbar */
  /*
  $("#topbar-file").load("./topbar/topbar.html", function() { // callback
    if($("#carrello").css("display") == "none") { //mobile
      $("#carrello-topbar").css("display","block");
    }
  });
  */
  /* show/hide 'piatti' list for the clicked category */
  $("#categorie-cibo>a").click(function() {
    $(this).toggleClass("active");
    $(this).next().toggleClass("visible");

    if($("#piatti").css("display") != "none") {
      if($(this).siblings(".active").length > 0) {
        $(this).siblings(".active").next().removeClass("visible");
        $(this).siblings(".active").next().css("max-height", "0px");
        $(this).siblings(".active").removeClass("active");
      }

      $("#piatti").find("ul").remove();
      $("#piatti>p").css("display", "none");
      $("#piatti>header").after($(this).next()[0].outerHTML);

      toggleHeight($("#piatti ul"));
    } else {
      toggleHeight($(this).next());
    }

    /*
    if($("#piatti").css("display") != "none") {
      toggleHeight($("#piatti ul"));
    } else {
      toggleHeight($(this).next());
    }
    */

    /*
    if($("#piatti").css("display") == "none") {
      $(this).toggleClass("active");
      $(this).next().toggleClass("visible");
      toggleHeight($(this).next());
    } else {
      if($(this).siblings(".active").length > 0) {
        $(this).siblings(".active").next().removeClass("visible");
        $(this).siblings(".active").next().css("max-height", "0px");
        $(this).siblings(".active").removeClass("active");
      }

      $(this).toggleClass("active");
      $(this).next().toggleClass("visible");

      $("#piatti").find("ul").remove();
      $("#piatti>p").css("display", "none");
      $("#piatti>header").after($(this).next()[0].outerHTML);

      //toggleHeight($("#piatti ul"));
    }
    */
    if($("#piatti").find(".visible").length == 0) {
      $("#piatti>p").css("display", "block");
    }
  });

  var isDesktop = $("#piatti").css("display") != "none";

  $(window).resize(function() {
    if($(".visible").length > 0) {
      if(isDesktop && $("#piatti").css("display") == "none") { //desktop->mobile
        //toggleHeight();
        //console.log($("#categorie-cibo .visible").get(0). + "px");
        $("#piatti ul").remove();
        $("#piatti>p").css("display", "block");
        $("#categorie-cibo .visible").css("max-height", $("#categorie-cibo .visible").get(0).scrollHeight + "px");
        isDesktop = false;
      }
      if(!isDesktop && $("#piatti").css("display") != "none") { //mobile->desktop
        if($(".active").length > 1) {
          $(".visible").each(function() {
            $(this).css("max-height", "0px");
          })
          $(".visible").removeClass("visible");
          $(".active").removeClass("active");
          $("#piatti").find("ul").remove();
          $("#piatti>p").css("display", "block");
        } else if($(".active").length == 1) {
          $(".active").next().css("max-height", "0px");
          $("#piatti").find("ul").remove();
          $("#piatti>p").css("display", "none");
          $("#piatti>header").after($(".active").next()[0].outerHTML);
          $("#piatti .visible").css("max-height", $("#piatti .visible").get(0).scrollHeight + "px");
        } else if($(".active").length == 0) {
          $("#piatti").find("ul").remove();
          $("#piatti>p").css("display", "block");
        }
        isDesktop = true;
      }
    }
  });

});

var page;
var idcucina;
var num_pages;
var errore = false;

function error() {
  errore = true;
  $(".pagination").html("");
  $("#fornitori").html("<p>Impossibile caricare i fornitori</p>");
}

function manage_pages() {
  $(".pagination a").removeClass("disabled");
  $(".pagination a").removeClass("active");
  if(page == 1) {
    $(".pagination a:first-child").addClass("disabled");
  }
  if(page == num_pages){
    $(".pagination a:last-child").addClass("disabled");
  }
  $(".pagination a:nth-child("+(parseInt(page)+1)+")").addClass("active");
}

function loadPage() {
  var effective_page = page - 1;
  $.getJSON("richiesta_fornitori.php?request=page&cucina=" + idcucina + "&page=" + effective_page,
  function(data) {
    if(data["errore"] === "errore") {
      error();
    } else {
      var fornitori = "";
      for(var i=0; i<data.length; i++) {
        fornitori += '<a href="./fornitore.php?id=' + data[i]["Id"] + '">'
        + data[i]["Nome"] + '<i class="fas fa-angle-right fa-lg"></i></a>';
      }
      if(data.length == 0) {
        fornitori += "<p>Non ci sono fornitori da mostrare</p>";
      }
      $("#fornitori").html(fornitori);
      manage_pages();
    }
  });
}

$(document).ready(function() {
  idcucina = new URL(window.location.href).searchParams.get("cucina");
  if(idcucina === null) {
    idcucina = "all";
  }

  page = 1;
  loadPage();

  $.getJSON("richiesta_fornitori.php?request=num_pages&cucina=" + idcucina, function(data) {
    if(data["errore"] === "errore") {
      error();
    } else if(!errore) {
      num_pages = data['num_pages'];
      if(data['num_pages'] > 1) {
        var html = '<a href="#">&laquo;</a>';
        for(var i=1; i<=data['num_pages']; i++) {
          html += '<a href="#">' + i + '</a>';
        }
        html += '<a href="#">&raquo;</a>';
        $(".pagination").html(html);
        manage_pages();
      }
    }
  });

  $(document).on("click", ".pagination a", function() {
    if(!$(this).hasClass("disabled") && !$(this).hasClass("active") && !errore) {
      switch($(this).html()) {
        case '«': page--; break;
        case '»': page++; break;
        default: page = $(this).html();
      }
      loadPage();
    }
  })
});

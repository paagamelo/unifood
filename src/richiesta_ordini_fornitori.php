<?php
header('Content-Type: application/json');

require 'db_connect.php';
require 'login_functions.php';
require 'informazioni_ordini_fornitori.php';
sec_session_start();

if($conn->connect_error) {
  $output["errore"] = "errore";
  die(json_encode($output));
}

if(!loggedAs($conn, 'fornitori')) {
  $output["errore"] = "errore";
  closeConnection($conn);
  die(json_encode($output));
}

if(isset($_GET['request'])) {

  $idfornitore = $_SESSION['user_id'];
  $rows_per_page = 1;
  $output = array();

  switch($_GET['request']) {

    case 'nuovi':
    {
      getNuoviOrdiniFornitore($conn, $idfornitore, $ordini);
      if(isset($ordini["errore"]) && $ordini["errore"]) {
        $output["errore"] = "errore";
        closeConnection($conn);
        die(json_encode($output));
      }

      print json_encode($ordini);
      break;
    }

    /* Is there more? */
    case 'more': //?request=more&filter=consegna/conclusi&page={..}
    {
      if(!isset($_GET['filter']) || !isset($_GET['page']) || $_GET['page'] < -1) { //-1 = nothing is being displayed
        $output["errore"] = "errore";
        closeConnection($conn);
        die(json_encode($output));
      }

      $filter = "";

      switch($_GET['filter']) {
        case 'consegna': $filter = "In consegna"; break;
        case 'conclusi': $filter = "Consegnato"; break;
        default: {
          $output["errore"] = "errore";
          closeConnection($conn);
          die(json_encode($output));
        }
      }

      $count = countOrdiniFornitore($conn, $idfornitore, $filter);
      if($count < 0) {
        $output["errore"] = "errore";
        closeConnection($conn);
        die(json_encode($output));
      }

      $more = ($count > (($_GET['page'] + 1) * $rows_per_page)) ? "true" : "false";
      $output['more'] = $more;
      print json_encode($output);
      break;
    }

    case 'page': //?request=page&filter={..}&page={..}
    {
      if(!isset($_GET['filter']) || !isset($_GET['page']) || $_GET['page'] < 0) {
        $output["errore"] = "errore";
        closeConnection($conn);
        die(json_encode($output));
      }

      $start_row = $_GET['page'] * $rows_per_page;
      $filter = "";

      switch($_GET['filter']) {
        case 'consegna': $filter = "In consegna"; break;
        case 'conclusi': $filter = "Consegnato"; break;
        default: {
          $output["errore"] = "errore";
          closeConnection($conn);
          die(json_encode($output));
        }
      }

      getVecchiOrdiniFornitoreLimit($conn, $idfornitore, $ordini, $filter, $start_row, $rows_per_page);
      if(isset($ordini["errore"]) && $ordini["errore"]) {
        $output["errore"] = "errore";
        closeConnection($conn);
        die(json_encode($output));
      }

      print json_encode($ordini);
      break;
    }

    case 'topage':
    {
      if(!isset($_GET['filter']) || !isset($_GET['page']) || $_GET['page'] < 0) {
        $output["errore"] = "errore";
        closeConnection($conn);
        die(json_encode($output));
      }

      switch($_GET['filter']) {
        case 'consegna': $filter = "In consegna"; break;
        case 'conclusi': $filter = "Consegnato"; break;
        default: {
          $output["errore"] = "errore";
          closeConnection($conn);
          die(json_encode($output));
        }
      }

      $to = ($_GET['page'] * $rows_per_page) + $rows_per_page;

      getVecchiOrdiniFornitoreLimit($conn, $idfornitore, $ordini, $filter, 0, $to);
      if(isset($ordini["errore"]) && $ordini["errore"]) {
        $output["errore"] = "errore";
        closeConnection($conn);
        die(json_encode($output));
      }

      print json_encode($ordini);
      break;
    }

    default:
    {
      $output["errore"] = "errore";
      closeConnection($conn);
      die(json_encode($output));
    }
  }
  closeConnection($conn);
}

?>

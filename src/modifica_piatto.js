
function aggiungiPiattoModificato(nome, cucina, categoria, prezzo, modificabile, ingredienti, id, idsezione){
  modificabile = modificabile == 1 ? " modificabile" : "";
  ingredienti_piatti[id] = ingredienti;
  var display_ingredienti = getStringaIngredienti(calcolaIngredienti(ingredienti,cucina));
  var display_piatto =
                        "<li class=\"piatto"+modificabile+"\" id=\""+id+"\">"
                      + "<span class=\"nome-piatto\">" + nome + "</span>"
                      + "<span class=\"prezzo-piatto\">" + prezzo +"</span>"
                      + modifica
                      + meno
                      + "<span class=\"ingredienti-piatto\">" + display_ingredienti + "</span></li>";
  var idposizione = "c"+cucina+"c"+categoria;
  if(idsezione == idposizione)  $("#"+idPiattoModifica).replaceWith(display_piatto);
  else { //Bisogna cambiare la categoria
    if($("#"+idposizione+" li.piatto").length == 1) $("#"+idsezione).append(no_piatti);
    $("#"+idPiattoModifica).remove();
    $("#"+idposizione+" ul.piatti").append(display_piatto);
    if($("#"+idposizione+" li.piatto").length == 1) $("#"+idposizione+" div.no-piatti").remove();
  }
}

function checkDatiPiattoModificato(categoria,nome,prezzo,modificabile,ingredienti){
  var errori = "";
  errori +=
              (checkNumber(categoria) ? "<li><span>Categoria</span> non valida</li>" : "")
            + checkNomePiatto(nome)
            + checkPrezzo(prezzo)
            + checkIngredienti(ingredienti);

  return errori;
}

function ottieniModifichePiatto(){
  var categoria = $("#categoria").val();
  var nome = $("#nome-piatto").val();
  var cucina = cucinaPiattoModifica;
  var prezzo = $("#costo").val();
  var modificabile = $("#modificabile:checked").length == 0 ? 0 : 1;
  var ingredienti = ingredienti_selezionati.filter(ingrValidi);
  var errori = checkDatiPiattoModificato(categoria,nome,prezzo,modificabile,ingredienti);
  if(errori != ""){ //Dati inseriti non corretti, mostra modal
    $("#modal-file").append("<ul class=\"errori\">" + errori + "</ul>");
  } else {
    $.post("./aggiorna_piatti_fornitori.php",
    {
      request: "modifica",
      cucina: cucina,
      categoria: categoria,
      nome: nome,
      prezzo: prezzo,
      modificabile: modificabile,
      ingredienti: ingredienti,
      piatto: idPiattoModifica
    },
    function(state){
      var messaggio = "";
      if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
      else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
      else if(state == "yet-present") messaggio = "Esiste già un altro tuo piatto con questo nome";
      else if(state == "query-error") messaggio = "Qualcosa è andato storto con l'inserimento dei dati, si consiglia di ricaricare la pagina";
      if(messaggio != "") $("#modal-file").append("<div class=\"errore\">"+messaggio+"</div>");
      else {
        var idsezione = "c"+cucina+"c"+categoriaPiattoModifica;
        aggiungiPiattoModificato(nome, cucina, categoria, prezzo, modificabile, ingredienti, idPiattoModifica, idsezione);
        $(".modal").hide();
        $("#modal-file").children().remove();
      }
    });
  }
}
  //Aggiorna categorie
  $(document).on("click", "#salva-modifiche-piatto", function() {
    event.preventDefault();
    ottieniModifichePiatto();
  });

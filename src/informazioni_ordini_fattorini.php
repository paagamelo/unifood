<?php

include_once('informazioni_ordini_fornitori.php');

function checkFattorinoExists(&$conn, $idfattorino) {
  $stmt = $conn->prepare("SELECT * FROM FATTORINI WHERE Id = ?");
  if(
       !$stmt
    || !$stmt->bind_param("i", $idfattorino)
    || !$stmt->execute())
  {
    return false;
  }

  $result = $stmt->get_result();
  $stmt->close();
  return $result->num_rows > 0;
}

function countVecchieConsegneFattorino(&$conn, $idfattorino) {
  if(!checkFattorinoExists($conn, $idfattorino)) {
    return -1;
  }

  $stmt = $conn->prepare(
    "SELECT COUNT(DISTINCT Id)
    FROM ORDINI
    WHERE IdFattorino = ?
    AND Stato = 'Consegnato'");
  if(
       !$stmt
    || !$stmt->bind_param("i", $idfattorino)
    || !$stmt->execute())
  {
    return -1;
  } else {
    $count = $stmt->get_result()->fetch_assoc()["COUNT(DISTINCT Id)"];
    $stmt->close();
    return $count;
  }
}

function getNuoveConsegneFattorino(&$conn, $idfattorino, &$consegne) {

  $consegne = array();
  if(!checkFattorinoExists($conn, $idfattorino)) {
    $consegne["errore"] = true;
    return;
  }

  $stmt = $conn->prepare(
    "SELECT ORDINI.*,
    CLIENTI.Username AS CUsername, CLIENTI.Nome AS CNome, CLIENTI.Cognome AS CCognome,
    FORNITORI.Username AS FUsername, FORNITORI.Nome AS FNome
    FROM ORDINI
    INNER JOIN FORNITORI ON ORDINI.IdFornitore = FORNITORI.Id
    INNER JOIN CLIENTI ON ORDINI.IdUtente = CLIENTI.Id
    WHERE ORDINI.IdFattorino = ?
    AND ORDINI.Stato = 'In consegna'
    ORDER BY OraConsegna ASC");
  if(
       !$stmt
    || !$stmt->bind_param("i", $idfattorino)
    || !$stmt->execute())
  {
    $consegne["errore"] = true;
    return;
  }
  $result = $stmt->get_result();
  $stmt->close();
  while($row = $result->fetch_assoc()) {
    $consegne[] = $row;
  }
  populateOrdini($conn, $consegne);
}

function getVecchieConsegneFattorinoLimit(&$conn, $idfattorino, &$consegne, $start, $end) {
  $consegne = array();
  if(!checkFattorinoExists($conn, $idfattorino)) {
    $consegne["errore"] = true;
    return;
  }

  $stmt = $conn->prepare(
    "SELECT ORDINI.*,
    CLIENTI.Username AS CUsername, CLIENTI.Nome AS CNome, CLIENTI.Cognome AS CCognome,
    FORNITORI.Username AS FUsername, FORNITORI.Nome AS FNome
    FROM ORDINI
    INNER JOIN CLIENTI ON ORDINI.IdUtente = CLIENTI.Id
    INNER JOIN FORNITORI ON ORDINI.IdFornitore = FORNITORI.Id
    WHERE ORDINI.IdFattorino = ?
    AND ORDINI.Stato = 'Consegnato'
    ORDER BY OraConsegna ASC
    LIMIT ?, ?");
  if(
       !$stmt
    || !$stmt->bind_param("iii", $idfattorino, $start, $end)
    || !$stmt->execute())
  {
    $consegne["errore"] = true;
    return;
  }
  $result = $stmt->get_result();
  $stmt->close();
  while($row = $result->fetch_assoc()) {
    $consegne[] = $row;
  }
  populateOrdini($conn, $consegne);
}



 ?>

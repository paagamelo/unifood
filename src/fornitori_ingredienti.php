<!DOCTYPE html>
<?php
require 'db_connect.php';
require 'login_functions.php';

sec_session_start(); //Avvio sessione php sicura
$logged = loggedAs($conn,"fornitori");
if($logged) { //Login effettuato
?>
  <html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Font Awesome (for icons) -->
    <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>
    <!-- Topbar style -->
    <link rel="stylesheet" type="text/css" href="./topbar/topbar_style.css">
    <link rel="stylesheet" type="text/css" href="fornitori_ingredienti_style.css">
    <link rel="stylesheet" type="text/css" href="modal.css">
    <link rel="stylesheet" type="text/css" href="breadcrumb.css">
    <script src="./fornitori_ingredienti.js"></script>
    <script src="./aggiungi_ingrediente.js"></script>
    <script src="./crea_ingrediente.js"></script>
    <script src="./checkDati.js"></script>
    <title>I Tuoi Ingredienti</title>
  </head>
  <body>
    <header>
      <h1>I Tuoi Ingredienti</h1>
    <ul class="breadcrumb">
      <li><a href="./unifood.php">Home</a></li>
      <li>I tuoi ingredienti</li>
    </ul>
    </header>
    <section id="ingredienti-forniti">
      <header><h2>Ingredienti Forniti</h2>
      <button type="button" class="ingredienti modify-btn" onclick="toggleModify();">Modifica</button>
      </header>
      <ul class="cucine">
        <!-- Cucine Fornite -->
      </ul>
    </section>
    <section id="ingredienti-disponibili">
      <header><h2>Ingredienti Disponibili</h2>
      <button type="button" onclick="creaIngrediente();">Crea nuovo</button>
      </header>
      <ul class="categorie-ingredienti">
        <!-- Categorie Ingredienti -->
      </ul>

    </section>
  <!-- The Modal -->
  <div class="modal">
    <!-- Modal content -->
    <div class="modal-content">
      <span class="close">&times;</span>
      <!-- Load modal file-->
      <div id="modal-file">

      </div>
    </div>
  </div>
    <?php require './topbar/topbar.php'; closeConnection($conn);?>
  </body>
  </html>
<?php
} else header('Location: ./'.closeConnectionAndReturn($conn,"login.php?Atype=fornitori")); //Effettua il login per accedere
?>

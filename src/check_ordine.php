<?php

require 'common_queries.php';

function checkPiattoAppartieneAFornitore(&$conn, $idfornitore, $idpiatto, &$piatto) {
  getInfoPiatto($conn, $idfornitore, $idpiatto, $piatto);
  if((isset($piatto["errore"]) && $piatto["errore"]) || empty($piatto)) {
    return false;
  }
  return true;
}

function checkModificheAppartengonoAFornitore(&$conn, $ingredienti_fornitore, $modifiche) {
  $id_ingredienti_fornitore = array_map(function($i) { return $i["Id"]; }, $ingredienti_fornitore);
  foreach($modifiche as $modifica) {
    if(!in_array($modifica["Id"], $id_ingredienti_fornitore)) {
      return false;
    }
  }
  return true;
}

function checkModificheStrutturaliValide($ingredienti_piatto, $ingredienti_modificati) {
  $ingredienti_strutturali_piatto = array_filter($ingredienti_piatto, function($i) { return $i["StrutturaleYN"] == "1"; });
  $categorie_strutturali = array();
  foreach($ingredienti_strutturali_piatto as $ingrediente_strutturale) {
    $categorie_strutturali[$ingrediente_strutturale["IdCategoria"]] = 0;
  }
  $modifiche_strutturali = array_filter($ingredienti_modificati, function($i) { return $i["StrutturaleYN"] == "1"; });
  foreach($modifiche_strutturali as $modifica_strutturale) {
    if(!isset($categorie_strutturali[$modifica_strutturale["IdCategoria"]])) {
      return false;
    } else {
      $categorie_strutturali[$modifica_strutturale["IdCategoria"]]++;
      if($categorie_strutturali[$modifica_strutturale["IdCategoria"]] > 1) {
        return false;
      }
    }
  }
  return true;
}

function checkOrdine(&$conn, $idfornitore, $piatti, &$output) {
  $output = array();
  if(
       $conn->connect_error
    || $idfornitore == null
    || $piatti  == null
    || empty($idfornitore)
    || empty($piatti))
  {
    $output["errore"] = "argomenti non validi";
    return false;
  }

  getInfoFornitore($conn, $idfornitore, $fornitore);
  if((isset($fornitore['errore']) && $fornitore['errore']) || empty($fornitore)) {
    $output["errore"] = "fornitore inesistente";
    return false;
  }

  $totale = (float)$fornitore['SpeseConsegna'];

  $ingredienti_fornitore = array();

  foreach($piatti as $piatto_in_ordine) {
    if(!isset($piatto_in_ordine["id"]) || !isset($piatto_in_ordine["quantita"])) {
      $output["errore"] = "argomenti non validi";
      return false;
    }

    if($piatto_in_ordine["quantita"] < 1) {
      $output["errore"] = "quantità non valida";
      return false;
    }

    getInfoPiatto($conn, $idfornitore, $piatto_in_ordine["id"], $piatto);
    if((isset($piatto["errore"]) && $piatto["errore"]) || empty($piatto)) {
      $output["errore"] = "piatto non valido";
      return false;
    }

    $prezzo_modifiche = 0.00;

    if(
         isset($piatto_in_ordine["modifiche"])
      && $piatto_in_ordine["modifiche"] != null
      && !empty($piatto_in_ordine["modifiche"]))
    {
      if(!$piatto["ModificabileYN"]) {
        $output["errore"] = "piatto non modificabile";
        return false;
      }

      $modifiche = $piatto_in_ordine["modifiche"];
      $idcucina = $piatto["IdCucina"];

      if(!isset($ingredienti_fornitore[$idcucina])) {
        getIngredientiFornitore($conn, $idfornitore, $idcucina, $ingredienti_fornitore[$idcucina]);
        if(isset($ingredienti_fornitore[$idcucina]["errore"]) && $ingredienti_fornitore[$idcucina]["errore"]) {
          $output["errore"] = "errore";
          return false;
        }
      }

      /* ingredienti di default */
      $ingredienti_piatto = array();

      getIngredientiPiatto($conn, $idfornitore, $piatto["IdPiattoInFornitore"], $ingredienti_piatto);
      if(isset($ingredienti_piatto["errore"]) && $ingredienti_piatto["errore"]) {
        $output["errore"] = "errore";
        return false;
      }

      /* ingredienti modificati */
      $ingredienti_modificati = array();

      getInfoIngredienti($conn, array_map(function($m) { return $m["id"]; }, $modifiche), $idcucina, $idfornitore, $ingredienti_modificati);
      if(isset($ingredienti_piatto["errore"]) && $ingredienti_piatto["errore"]) {
        $output["errore"] = "errore";
        return false;
      }

      if(!checkModificheAppartengonoAFornitore($conn, $ingredienti_fornitore[$idcucina], $ingredienti_modificati)) {
        $output["errore"] = $piatto["Nome"];//print_r($ingredienti_fornitore[$idcucina]);//"modifiche non valide";
        return false;
      }

      if(!checkModificheStrutturaliValide($ingredienti_piatto, $ingredienti_modificati)) {
        $output["errore"] = "modifiche strutturali non valide";
        return false;
      }

      $ingredienti_aggiunti = array_filter($ingredienti_modificati, function($i) use(&$ingredienti_piatto) { return !in_array($i, $ingredienti_piatto); });

      foreach($ingredienti_aggiunti as $ing) {
        $prezzo_modifiche += $ing["CostoAggiunta"];
      }
    }
    $totale += ($piatto["Prezzo"] + $prezzo_modifiche) * $piatto_in_ordine["quantita"];
  }
  //everything is ok
  $output['totale'] = $totale;
  return true;
}
?>

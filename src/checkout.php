<?php
require 'db_connect.php';
require 'login_functions.php';
require 'check_ordine.php';
sec_session_start(); //Avvio sessione php sicura

if($conn->connect_error) {
  die("errore");
}

if(!loggedAs($conn, 'clienti')) {
  header('Location: ./'.closeConnectionAndReturn($conn,"login.php?Atype=clienti"));
}

if(  !isset($_SESSION['pendingorder'])
  || !isset($_SESSION['pendingorder']['idfornitore'])
  || !isset($_SESSION['pendingorder']['piatti']))
{
  die(closeConnectionAndReturn($conn, "errore"));
}

$idfornitore = $_SESSION['pendingorder']['idfornitore'];
$piatti = $_SESSION['pendingorder']['piatti'];

if(!checkOrdine($conn, $idfornitore, $piatti, $output)) {
  print_r($piatti);
  die(closeConnectionAndReturn($conn, $output["errore"]));
}

$totale = $output['totale'];

getInfoFornitore($conn, $idfornitore, $fornitore);
if(isset($fornitore["errore"]) && $fornitore["errore"] || empty($fornitore)) {
  die(closeConnectionAndReturn($conn, "errore"));
}

?>
<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- JQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- Font Awesome (for icons) -->
  <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>
  <script src="./checkout.js"></script>
  <script src="./checkDati.js"></script>
  <script src="./aggiungi_carta.js"></script>
  <link rel="stylesheet" type="text/css" href="./checkout.css">
  <title>Checkout</title>
</head>
<body>
  <section id="completamento">
    <header><h1>Completa il tuo ordine</h1></header>
    <section id="riepilogo" <?php echo 'data-idfornitore="'.$fornitore["Id"].'">'?>
      <header><h2>Il tuo ordine da <?php echo $fornitore["Nome"]; ?></h2></header>
      <ul>
        <?php
        $html = "";
        foreach($piatti as $piatto) {
          $html .= '<li class="piatto"><span class="nome">'.$piatto["nome"].' x'.$piatto["quantita"].'</span>';
          if(isset($piatto["modifiche"]) && !empty($piatto["modifiche"])) {
            $html .= '<ul class="modifiche">';
            foreach($piatto["modifiche"] as $modifica) {
              $html .= '<li>'.($modifica["isAddition"] == "true" ? "+ " : "- ").$modifica["nome"].'</li>';
            }
            $html .= '</ul>';
          }
          $html .= '</li>';
        }
        echo $html;
        ?>
      </ul>
      <div class="totale">Totale:<div class="prezzo"><span class="currency">€</span>
        <span class="value"><?php echo number_format((float)$totale, 2, '.', ''); ?></span>
      </div>
    </section>
    <form id="completa">
      <label for="orario">Orario:</label>
      <input type="time" name="orario" id="orario" min="09:00" max="19:00" required>
      <label for="piano">Piano:</label>
      <select name="piano" id="piano">
        <option value="1">Primo piano</option>
        <option value="2">Secondo piano</option>
        <option value="3">Terzo piano</option>
      </select>
      <label for="note">Note:</label>
      <textarea name="note" id="note" rows="4" cols="50" placeholder="Note.." maxlength="100"></textarea>
      <input type="submit" value="Salva"/>
      <input type="button" value="Indietro"/>
    </form>
  </section>
  <section id="pagamento">
    <header><h1>Pagamento</h1></header>
    <form>
    <fieldset><legend class="visuallyhidden">Pagamento</legend>
      <div class="row">
        <label for="pagaconsegna" class="visuallyhidden">Paga alla consegna</label>
        <input type="radio" id="pagaconsegna" name="pagamento" value="paga alla consegna" checked>
        Paga alla consegna<br/>
      </div>
      <div class="row">
        <div class="aggiungi-carta"><span class="symbol plus"><i class="fas fa-plus-circle"></i> Aggiungi carta</span></div>
      </div>
    </fieldset>
    <div class="last row">
      <input type="submit" value="Invia"/>
      <input type="button" value="Indietro"/>
    </div>
    </form>
  </section>
  <section id="esito">
    <header><h1>Unifood</h1></header>
    <p>Il tuo ordine è stato ricevuto correttamente, <a href="./unifood.php">torna alla home</a></p>
  </section>
  <!-- The Modal -->
  <div class="modal">
    <!-- Modal content -->
    <div class="modal-content">
      <span class="close" onmouseover = "..." onmouseout  = "...">&times;</span>
      <!-- Load modal file-->
      <div id="modal-file">

      </div>
    </div>
  </div>
</body>
</html>

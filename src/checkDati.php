<?php
//TRUE OK
//La stringa deve avere solo caratteri
function checkOnlyCharacters($string){
  return
              !empty($string)
          &&  !preg_match('/[0-9]/', $string);
}
//La stringa non deve contenere spazi
function checkNoSpaces($string){
  return
              !empty($string)
          &&  !preg_match('/ /', $string);
}
//La stringa deve rientrare nel range di caratteri (min-max)
function checkDimension($string, $min, $max){
  $len = strlen($string);
  return
             !empty($string)
          && !($len < $min)
          && !($len > $max);
}
//Il numero deve rientrare nel range (min-max)
function checkRangeNumber($number, $min, $max){
  return
             is_numeric($number)
          && !($number < $min)
          && !($number > $max);
}
//Deve contenere da 1 a 15 caratteri, senza cifre
function checkNome($nome){
  return
             checkDimension($nome,1,15)
          && checkOnlyCharacters($nome);
}
//Deve contenere da 1 a 15 caratteri, senza cifre
function checkCognome($cognome){
  return
              checkDimension($cognome,1,15)
          &&  checkOnlyCharacters($cognome);
}
//Deve essere un email valida, fino ad un massimo di 30 caratteri
function checkEmail($email){
  return
             !empty($email)
          && preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $email)
          && checkDimension($email,1,30);
}
//Se non è null, deve contenere esattamente 10 cifre (non caratteri)
function checkTelefono($telefono,$obbligatorio){
  if($obbligatorio) return  !empty($telefono) && is_numeric($telefono) && strlen($telefono) == 10;
  else return empty($telefono) || (is_numeric($telefono) && strlen($telefono) == 10);
}
//Deve contenere da 1 a 15 caratteri, senza spazi
function checkUsername($username){
  return
              checkNoSpaces($username)
          &&  checkDimension($username,1,15);
}
//Deve contenere esattamente 128 caratteri (è criptata)
function checkPassword($password){
  return checkDimension($password,128,128);
}
//Deve essere un numero
function checkId($id){
  return is_numeric($id);
}
//Controlla il tipo della carta
function checkTipoCarta($tipo){
  return checkDimension($tipo,1,22);
}
//Controlla il numero della carta, deve essere un numero di 3 cifre
function checkNumeroCarta($numero){
  return
            is_numeric($numero)
        &&  strlen($numero) == 3;
}
//Controlla che sia stato passato un valore esistente per il tipo di account, register indica se si tratta di una registrazione
function checkAtype($atype,$register){
  $result = false;
  if(!empty($atype)){
    switch($atype){
      case "clienti" : $result = true; break;
      case "fornitori" : $result = true; break;
      case "fattorini" : $result = true; break;
      case "admin" : $result = !$register; break;
    }
  }
  return $result;
}
//Controlla che sia un indirizzo civico valido
function checkIndirizzo($indirizzo){
  return
              checkDimension($indirizzo,1,30)
           && preg_match("#[A-Za-z]([,]{1}|[ ])[ ]*[0-9]#", $indirizzo);
}
//Controlla che il valore di costo sia un numero positivo
function checkCosto($costo) {
  return checkRangeNumber(floatval($costo),0.00,99.99);
}
//Controlla che il valore passato sia un orario
function checkOrario($orario){
  if(!empty($orario) && strlen($orario) == 4) $orario = "0".$orario;
  return
              !empty($orario)
          &&  strlen($orario) == 5
          &&  preg_match("#([0-1][0-9]|[2][0-3])[:][0-5][0-9]#", $orario);
}
//Controlla che l'ora di inizio sia minore dell'ora di fine
function checkRangeOrario($da,$a){
  return strtotime($da) < strtotime($a);
}
//Controlla che il numero massimo di ordini sia un numero maggiore di 0 (o vuoto)
function checkMaxOrdini($maxOrdini){
  return empty($maxOrdini) || (is_numeric($maxOrdini) && $maxOrdini > 0);
}
//Controlla che il numero massimo di ordini sia un numero maggiore di 0 (o vuoto)
function checkRistorante($ristorante){
  return checkDimension($ristorante,1,15);
}
//Controlla che la dimensione delle note sia minore di 100 (o vuoto)
function checkNote($note){
  return empty($note) || checkDimension($note,1,100);
}
//Controlla se sono stati passati dei giorni corretti, massimo 4
function checkGiorni($giorni){
  $error = false;
  $i = 0;
  foreach ($giorni as $giorno) {
    $i++;
    if(!checkRangeNumber($giorno,1,5)) $error = true;
  }
  return ($i < 5) && (!$error);
}
//Controlla che gli ingredienti siano un array di numeri
function checkIngredienti($ingredienti){
  $error = false;
  $i = 0;
  foreach ($ingredienti as $ingrediente) {
    $i++;
    if(!is_numeric($ingrediente)) $error = true;
  }
  //return ($i > 0) && (!$error);
  return (!$error);
}
//La descrizione deve avere il formato 'Cucina: nome Categoria: nome', la descrizione deve avere dimensione minore di 76 e i nomi nel range (1,25)
function checkDescrizione($descrizione){
  $ok = true;
  if(empty($descrizione) || strlen($descrizione) > 76) $ok = false;
  else {
    $cucina = stristr($descrizione, "Cucina: ");
    $categoria = stristr($descrizione, " Categoria: ");
    if($cucina == false || $categoria == false) $ok = false;
    else {
      $cucina += 8;
      $cucina_str = substr($descrizione,$cucina,$categoria - $cucina);
      $categoria_str = substr($descrizione,$categoria+11);
      if(!checkDimension($cucina,1,25) || !checkDimension($categoria,1,25)) $state = false;
    }
  }
  return $descrizione;
}
//Controlla che l'orario sia precedente a quello attuale
function checkOrarioPrecedente($orario){
  $orario_attuale = date("H:i");
  return checkRangeOrario($orario_attuale, $orario);
}

?>

<?php
require 'db_connect.php';
require 'login_functions.php';
require 'common_queries.php';

sec_session_start(); //Avvio sessione php sicura
if(isset($_GET["request"])) {
   if ($conn->connect_error) {
     $output["Errore"] = "Errore di comunicazione con il server";
     die(json_encode($output));
   }

   $output = array();
   if(!isset($_SESSION["user_id"]) && !isset($_GET["id"])) {
     $output["Errore"] = "Dati inviati invalidi";
     die(json_encode(closeConnectionAndReturn($conn,$output)));
   }
   $id = (isset($_GET["id"]) ? $_GET["id"] : $_SESSION["user_id"]);
   if(!checkFornitoreExists($conn,$id))  {
     $output["Errore"] = "Dati inviati invalidi";
     die(json_encode(closeConnectionAndReturn($conn,$output)));
   }

   switch ($_GET["request"]) {
      case "piatti": //Richieste cucine di un fornitore

        $cucine = getCucineFornitore($conn,$id);
        $Cucine = array();

        foreach ($cucine as $cucina) { //Per ogni cucina
          //INGREDIENTI
          getIngredientiFornitore($conn,$id,$cucina["Id"],$ingredienti);
          $Cucine[$cucina["Id"]]["Id"] = $cucina["Id"];
          $Cucine[$cucina["Id"]]["Ingredienti"] = $ingredienti;
          $Cucine[$cucina["Id"]]["Nome"] = $cucina["Nome"];

          //CATEGORIE
          $categorie = getCategorieCiboCucina($conn,$cucina["Id"]);
          foreach($categorie as $categoria){ //Per ogni categoria
            $Cucine[$cucina["Id"]]["Categorie"][$categoria["Id"]]["Nome"] = $categoria["Nome"];
            $Cucine[$cucina["Id"]]["Categorie"][$categoria["Id"]]["Id"] = $categoria["Id"];
          }
          $ingredienti = [];
        }
        //PIATTI
        getPiattiInFornituraFornitore($conn,$id,$piattiInCategorie);
        foreach($piattiInCategorie as $piatti)
          foreach($piatti as $piatto)
            $Cucine[$piatto["IdCucina"]]["Categorie"][$piatto["IdCategoriaInCucina"]]["Piatti"][] =  $piatto;

        $output["CategorieIngredienti"] = getCategorieIngredienti($conn);
        $output["Cucine"] = $Cucine;

        print json_encode($output);
        break;
  }
  closeConnection($conn);
} else { //Login non effettuato
  $output["Errore"] = "Errore di comunicazione con il server";
  die(json_encode(closeConnectionAndReturn($conn,$output)));
}
?>

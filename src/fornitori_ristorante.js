var modifying = false; //Stato pagina
var name = ""; //Valore attuale del nome
var address = ""; //Valore attuale dell'indirizzo
var tel = ""; //Valore attuale del telefono
var spese = ""; //Valore attuale delle spese
var daOra = ""; //Valore attuale del orario di apertura
var aOra = ""; //Valore attuale del orario di chiusura
var maxOrd = ""; //Valore attuale del massimo di ordini
var logo = "";
var logo_name = "";
var notes = "";
var days = "";
var change_status = true;
const logo_default = "./images/loghi/default.png";

function toggleDisableObj(disable,obj){
  if(disable){
    obj.css("opacity","0.6");
    obj.attr("disabled","disabled");
  } else {
    obj.css("opacity","1");
    obj.removeAttr("disabled");
  }
}

//Evento collegato alla selezione del file, mostralo in antemprima come logo
function loadFile(event){
  var file = event.target.files[0];
  if (file != null && file != undefined){
    var img_url = URL.createObjectURL(file);
    document.getElementById("logo-img").src = img_url;
    toggleDisableObj(false,$("button.annulla"));
    toggleDisableObj(false,$("button.salva"));
  }
}
//Annulla logo
function annullaLogo(){
  var image_selector = document.getElementById("seleziona-img");
  if(image_selector.value != ""){
    document.getElementById("logo-img").src = logo;
    document.getElementById("seleziona-img").value = "";
    toggleDisableObj(true,$("button.annulla"));
    toggleDisableObj(true,$("button.salva"));
  }
}
//Elimina logo
function eliminaLogo(){
  $.post("./aggiorna_dati_ristorante.php",
  {
    request: "rimuovi-logo"
  },
  function(state){
    var messaggio = "";
    if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
    else if (state == "no-logo")  messaggio = "Non è stato caricato nessun logo (il logo di default non è eliminabile)";
    if(messaggio != ""){ //Si è verificato qualche errore, mostra modal
      $("#modal-file").append("<header><h3>Operazione non riuscita</h3></header><div>"+messaggio+"</div>");
      $(".modal").show();
    } else { //Imposta logo di default
      logo = logo_default;
      document.getElementById("logo-img").src = logo;
      document.getElementById("seleziona-img").value = "";
      toggleDisableObj(true,$("button.annulla"));
      toggleDisableObj(true,$("button.salva"));
      toggleDisableObj(true,$("button.elimina"));
    }
  });
}
//Invia logo
function inviaLogo(){
  var datiForm = new FormData();
  datiForm.append("logo",$("#seleziona-img")[0].files[0]);
  datiForm.append("request","aggiorna-logo");
  $.ajax({
   url: "aggiorna_dati_ristorante.php",
   type: "POST", //Le info testuali saranno passate in POST
   data: datiForm, //I dati, forniti sotto forma di oggetto FormData
   cache: false,
   processData: false, //Serve per NON far convertire l’oggetto
            //FormData in una stringa, preservando il file
   contentType: false, //Serve per NON far inserire automaticamente
            //un content type errato
   success: function(state)
   {
     var messaggio = "";
     if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
     else if(state == "no-file") messaggio = "Non è stato inviato correttamente il file";
     else if(state == "no-image") messaggio = "Il file caricato non è un immagine";
     if(messaggio != ""){ //Si è verificato qualche errore, mostra modal
       $("#modal-file").append("<header><h3>Operazione non riuscita</h3></header><div>"+messaggio+"</div>");
       $(".modal").show();
       //Reset a dati iniziali
       annullaLogo();
     } else {
       logo = document.getElementById("logo-img").src;
       toggleDisableObj(true,$("button.annulla"));
       toggleDisableObj(true,$("button.salva"));
       toggleDisableObj(false,$("button.elimina"));
     }
   }
  });
}
//Controlla correttezza dati in form
function checkDati(nome,indirizzo,telefono,speseConsegna,daOra,aOra, maxOrdini, note){
  var edo = checkOrario(daOra).replace("orario", "orario di apertura");
  var eao = checkOrario(aOra).replace("orario", " orario di chiusura");

  var errori =
                  checkRistorante(nome)
                + checkIndirizzo(indirizzo)
                + checkTelefono(telefono,true)
                + checkSpeseConsegna(speseConsegna)
                + edo
                + eao
                + ((edo == "" && eao == "") ? checkRangeOrario(daOra,aOra) : "") //Esegue il controllo del range dell'orario solo se sono stati passati orari validi
                + checkMaxOrdini(maxOrdini)
                + checkNote(note);
  return errori;
}

function toggleGiorni(){
  if(modifying){
    var errori = "";
    var giorni = $("ul.giorni input:checked");
    var i = 0;
    var giorni_chiusi = [];
    var giorni_nomi = "";
    for(;giorni[i];i++) {
      giorni_chiusi[i] = giorni[i].value;
      giorni_nomi += " "+giorni[i].name+",";
    }
    if(giorni_nomi != "") giorni_nomi = giorni_nomi.trim().substr(0,giorni_nomi.length-2)+".";
    else giorni_nomi = "Nessuno";
    errori = checkGiorni(giorni_chiusi);
    if(errori != ""){ //Dati inseriti non corretti, mostra modal
      $("#modal-file").append("<header><h3>Errori in Form</h3></header><ul>" + errori + "</ul>");
      $(".modal").show();
      change_status = false; //Non bisogna uscire dalla modalità di modifica
    } else{ //Dati inseriti corretti
      $("<span class=\"giorni\">"+giorni_nomi+"</span>").replaceAll("ul.giorni");
      if(days != giorni_nomi){ //Aggiorno i dati
        $.post("./aggiorna_dati_ristorante.php",
        {
          request: "giorni",
          giorni: giorni_chiusi
        },
        function(state){
          var messaggio = "";
          if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
          else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
          else if(state == "query-error") messaggio = "Non è stato possibile aggiornare uno o più dati" //C'è stato un errore nel caricare i dati
          if(messaggio != ""){ //Si è verificato qualche errore, mostra modal
            $("#modal-file").append("<header><h3>Operazione non riuscita</h3></header><div>"+messaggio+"</div>");
            $(".modal").show();
            //Reset a dati iniziali
            $("span.giorni").html(days);
          } else {
            if(state == "query-error") loadGiorni(); //Ricarico i giorni in seguito ad un errore di inserimento
            else days = (giorni_nomi == "" ? "Nessuno" : giorni_nomi);
          }
        });
      }
    }
  } else{
    days = $("span.giorni").html();
    var checkbox =
                    "<ul class=\"giorni\">"
                  + "<li><label for=\"lunedì\"><input type=\"checkbox\" name=\"lunedì\" value=\"1\" id=\"lunedì\">Lunedì</input></label></li>"
                  + "<li><label for=\"martedì\"><input type=\"checkbox\" name=\"martedì\" value=\"2\" id=\"martedì\">Martedi</input></label></li>"
                  + "<li><label for=\"mercoledì\"><input type=\"checkbox\" name=\"mercoledì\" value=\"3\" id=\"mercoledì\">Mercoledì</input></label></li>"
                  + "<li><label for=\"giovedì\"><input type=\"checkbox\" name=\"giovedì\" value=\"4\" id=\"giovedì\">Giovedì</input></label></li>"
                  + "<li><label for=\"venerdì\"><input type=\"checkbox\" name=\"venerdì\" value=\"5\" id=\"venerdì\">Venerdì</input></label></li>"
                  + "</ul>";
    $(checkbox).replaceAll("span.giorni");
    if(!checkEmpty(days)){
      var giorni = days.replace(".","").trim().split(",");
      for(giorno in giorni) $("#"+giorni[giorno].trim()).attr("checked","checked");
    }
  }
}

//Cambia modalità visualizzazione dati
function toggleDati(){
  if(modifying){
    var errori = "";
    var nome = $("input[name=nome]").val().trim();
    var indirizzo = $("input[name=indirizzo]").val().trim();
    var telefono = $("input[name=telefono]").val().trim();
    var speseConsegna = $("input[name=speseConsegna]").val().trim();
    var orarioApertura =  $("input[name=orarioApertura]").val().trim();
    var orarioChiusura =  $("input[name=orarioChiusura]").val().trim();
    var maxOrdini =  $("input[name=maxOrdini]").val().trim();
    var note = $("textarea[name=note]").val().trim();

    errori = checkDati(nome, indirizzo, telefono, speseConsegna, orarioApertura, orarioChiusura, maxOrdini, note);

    if(errori != ""){ //Dati inseriti non corretti, mostra modal
      $("#modal-file").append("<header><h3>Errori in Form</h3></header><ul>" + errori + "</ul>");
      $(".modal").show();
      change_status = false; //Non bisogna uscire dalla modalità di modifica
    } else{ // Dati inseriti correttamente
      $("<span class=\"nome\">"+nome+"</span>").replaceAll("input[name=nome]");
      $("<span class=\"nome-lb label\">Nome:</span>").replaceAll("label[for=nome]");
      $("<span class=\"indirizzo\">"+indirizzo+"</span>").replaceAll("input[name=indirizzo]");
      $("<span class=\"indirizzo-lb label\">Indirizzo:</span>").replaceAll("label[for=indirizzo]");
      $("<span class=\"telefono\">"+telefono+"</span>").replaceAll("input[name=telefono]");
      $("<span class=\"telefono-lb label\">Telefono:</span>").replaceAll("label[for=telefono]");
      $("<span class=\"speseConsegna\">"+speseConsegna+"</span>").replaceAll("input[name=speseConsegna]");
      $("<span class=\"speseConsegna-lb label\">Spese di consegna:</span>").replaceAll("label[for=speseConsegna]");
      $("<span class=\"orarioApertura\">"+orarioApertura+"</span>").replaceAll("input[name=orarioApertura]");
      $("<span class=\"orarioApertura-lb label\">Orario di apertura:</span>").replaceAll("label[for=orarioApertura]");
      $("<span class=\"orarioChiusura\">"+orarioChiusura+"</span>").replaceAll("input[name=orarioChiusura]");
      $("<span class=\"orarioChiusura-lb label\">Orario di chiusura:</span>").replaceAll("label[for=orarioChiusura]");
      $("<span class=\"maxOrdini\">"+maxOrdini+"</span>").replaceAll("input[name=maxOrdini]");
      $("<span class=\"maxOrdini-lb label\">Massimo numero di ordini contemporaneamente:</span>").replaceAll("label[for=maxOrdini]");
      $("<span class=\"note\">"+note+"</span>").replaceAll("textarea[name=note]");
      $("<span class=\"note-lb label\">Note:</span>").replaceAll("label[for=note]");

      if(name != nome || address != indirizzo || tel != telefono || spese != speseConsegna || daOra != orarioApertura || aOra != orarioChiusura || maxOrd != maxOrdini || note != notes){ //I dati sono stati modificati
        $.post("./aggiorna_dati_ristorante.php",
        {
          request: "dati",
          nome: nome,
          indirizzo: indirizzo,
          telefono: telefono,
          speseConsegna: speseConsegna,
          orarioApertura: orarioApertura,
          orarioChiusura: orarioChiusura,
          maxOrdini: maxOrdini,
          note: note
        },
        function(state){
          var messaggio = "";
          if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
          else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
          if(messaggio != ""){ //Si è verificato qualche errore, mostra modal
            $("#modal-file").append("<header><h3>Operazione non riuscita</h3></header><div>"+messaggio+"</div>");
            $(".modal").show();
            //Reset a dati iniziali
            $("span.nome").html(name);
            $("span.indirizzo").html(address);
            $("span.telefono").html(tel);
            $("span.speseConsegna").html(spese);
            $("span.orarioApertura").html(daOra);
            $("span.orarioChiusura").html(aOra);
            $("span.maxOrdini").html(maxOrdini);
            $("span.note").html(notes);
          }
        });
      }
    }
  } else{
    name = $("span.nome").html();
    address = $("span.indirizzo").html();
    tel = $("span.telefono").html();
    spese = $("span.speseConsegna").html();
    daOra = $("span.orarioApertura").html();
    aOra = $("span.orarioChiusura").html();
    maxOrd = $("span.maxOrdini").html();
    notes = $("span.note").html();
    $("<input type=\"text\" id=\"nome\" name=\"nome\" value=\""+ name + "\" maxlength=\"15\">").replaceAll("span.nome");
    $("<label for=\"nome\">Nome:</label>").replaceAll("span.nome-lb");
    $("<input type=\"text\" id=\"indirizzo\" name=\"indirizzo\" value=\""+ address + "\" maxlength=\"30\">").replaceAll("span.indirizzo");
    $("<label for=\"indirizzo\">Indirizzo:</label>").replaceAll("span.indirizzo-lb");
    $("<input type=\"tel\" id=\"telefono\" name=\"telefono\" value=\""+ tel + "\" maxlength=\"10\">").replaceAll("span.telefono");
    $("<label for=\"telefono\">Telefono:</label>").replaceAll("span.telefono-lb");
    $("<input type=\"number\" id=\"speseConsegna\" name=\"speseConsegna\" value=\""+ spese + "\">").replaceAll("span.speseConsegna");
    $("<label for=\"speseConsegna\">Spese di consegna:</label>").replaceAll("span.speseConsegna-lb");
    $("<input type=\"time\" id=\"orarioApertura\" name=\"orarioApertura\" value=\""+ daOra + "\">").replaceAll("span.orarioApertura");
    $("<label for=\"orarioApertura\">Orario di apertura:</label>").replaceAll("span.orarioApertura-lb");
    $("<input type=\"time\" id=\"orarioChiusura\" name=\"orarioChiusura\" value=\""+ aOra + "\">").replaceAll("span.orarioChiusura");
    $("<label for=\"orarioChiusura\">Orario di chiusura:</label>").replaceAll("span.orarioChiusura-lb");
    $("<input type=\"number\" id=\"maxOrdini\" name=\"maxOrdini\" value=\""+ maxOrd + "\">").replaceAll("span.maxOrdini");
    $("<label for=\"maxOrdini\">Massimo numero di ordini contemporaneamente:</label>").replaceAll("span.maxOrdini-lb");
    $("<textarea id=\"note\" name=\"note\" maxlength=\"100\">").replaceAll("span.note");
    $("<label for=\"note\">Note:</label>").replaceAll("span.note-lb");
    $("textarea[name=\"note\"]").html(notes);
  }
}

//Pulsante modifica/fine dati ristorante premuto
function toggleModify(classe) {
  if(classe == "restaurant-data") toggleDati();
  else toggleGiorni();
  if(change_status){
    if(modifying){
        toggleDisableObj(false,$("button.modify-btn:not(."+classe+")"));
        $("button."+classe).html("Modifica");
    } else{
      toggleDisableObj(true,$("button.modify-btn:not(."+classe+")"));
      $("button."+classe).html("Fine");
    }
    modifying = !modifying;
  }
  change_status = true;
}

//Carica dati
function loadDati(){
  $.post("./richiesta_dati_ristorante.php",
  {
    request: "dati"
  },
  function(data){
    var res = JSON.parse(data);
    if(res["Errore"] == undefined){ //Non ci sono stati errori
      name = res["Nome"];
      address = res["Indirizzo"];
      tel = res["Telefono"];
      spese = res["SpeseConsegna"];
      daOra = res["OrarioApertura"];
      aOra = res["OrarioChiusura"];
      maxOrd = (res["MaxOrdini"] != null ? res["MaxOrdini"] : "");
      notes = (res["Note"] != null ? res["Note"] : "");
      $("span.nome").html(name);
      $("span.indirizzo").html(address);
      $("span.telefono").html(tel);
      $("span.speseConsegna").html(spese);
      $("span.orarioApertura").html(daOra);
      $("span.orarioChiusura").html(aOra);
      $("span.maxOrdini").html(maxOrd);
      $("span.note").html(notes);
    }
  });
}
//Carica giorni
function loadGiorni(){
  $.post("./richiesta_dati_ristorante.php",
  {
    request: "giorni"
  },
  function(data){
    var res = JSON.parse(data);
    var giorni = "";
    if(res["Errore"] == undefined){ //Non ci sono stati errori
      if(res[0] != "Nessuno"){
        for(giorno in res) giorni += " "+res[giorno].substr(0,res[giorno].length-1)+"ì,";
        if(giorni != "") giorni = giorni.trim().substr(0,giorni.length-2)+".";
      } else giorni = "Nessuno";
      days = giorni;
      $("span.giorni").html(days);
    }
  });
}

$(document).ready(function(){

  logo = document.getElementById("logo-img").src; //Prendo il percorso del logo
  toggleDisableObj(true,$("button.annulla"));
  toggleDisableObj(true,$("button.salva"));


  if(logo.includes(logo_default.substr(1))) toggleDisableObj(true,$("button.elimina"));

  //Carica dati al primo caricamento della pagina
  loadDati();
  //Carica i giorni al primo caricamento della pagina
  loadGiorni();

  //Modifica dati ristorante
  $(document).on("click", ".restaurant-data", function() {
    toggleModify("restaurant-data");
  });

  //Modifica dati personali
  $(document).on("click", "button.chiusura", function() {
    toggleModify("chiusura");
  });

  //Click sull'immagine
  $(document).on("click", "#logo-img", function(){
    $("#seleziona-img").click();
  });

  //Chiudi modal
  $(document).on("click", ".close", function(){
    setTimeout(function() {
      $(".modal").hide();
      $("#modal-file").children().remove();
    }, 100);
  });
});

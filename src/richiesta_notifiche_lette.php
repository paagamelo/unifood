<?php
/* Marca come lette tutte le notifiche dell'utente */
header('Content-Type: application/json');

require 'db_connect.php';
require 'login_functions.php';
sec_session_start();
$output = array();

if($conn->connect_error) {
  $output["errore"] = "errore";
  die(json_encode($output));
}

if(!login_check($conn)) {
  $output["errore"] = "errore";
  closeConnection($conn);
  die(json_encode($output));
}

if(!isset($_POST['notifiche']) || empty($_POST['notifiche'])) {
  $output["errore"] = "errore";
  closeConnection($conn);
  die(json_encode($output));
}

$notifiche = $_POST['notifiche'];
$idutente = $_SESSION['user_id'];
$atype = $_SESSION['atype'];

switch($atype) {
  case 'clienti':
    $stmt = $conn->prepare("UPDATE NOTIFICHE SET LettaYN = '1' WHERE IdUtente = ? AND Id = ?");
    break;
  case 'fornitori':
    $stmt = $conn->prepare("UPDATE NOTIFICHE SET LettaYN = '1' WHERE IdFornitore = ? AND Id = ?");
    break;
  case 'fattorini':
    $stmt = $conn->prepare("UPDATE NOTIFICHE SET LettaYN = '1' WHERE IdFattorino = ? AND Id = ?");
    break;
  default:
    $output["errore"] = "errore";
    closeConnection($conn);
    die(json_encode($output));
}

foreach($notifiche as $k => $id_) {

  $id = $id_;
  if(
       !$stmt
    || !$stmt->bind_param('ii', $idutente, $id)
    || !$stmt->execute())
  {
    $output["errore"] = "errore";
    closeConnection($conn);
    die(json_encode($output));
  }
}

print json_encode("success");

closeConnection($conn);
?>

//Carica ordini
function loadOrdini(){
  $.post("./richiesta_ordini_clienti.php",
  {
    request: "ordini"
  },
  function(data){
    $("ul.ordini").children().remove(); //Rimuovo ordini
    $("div.no-ordini").remove(); //Rimuovo messaggi no ordini
    //$("div.errore").remove(); //Rimuovo messaggi di errore
    var res = JSON.parse(data);
    var no_ordini = "<div class=\"no-ordini\">Nessun ordine disponibile</div>";
    if(res["Errore"]!= undefined) $("ul.ordini").after("<div class=\"no-ordini\">"+res["Errore"]+"</div>");
    else{ //Non si sono verificati errori e c'è almeno un ordine da mostrare
      for(ordine in res){
        ordine = res[ordine];
        var posizione = (ordine["Stato"] != "Consegnato" ? "ul.in-corso" : "ul.conclusi");
        var stato = (posizione == "ul.in-corso" ?
                      "<span class=\"stato-ordine\">"+ordine["Stato"]+"</span>"
                    : "<span class=\"data\">"+ordine["Data"]+"</span>");
        var string_ordine =
                            "<li class=\"ordine\">"
                            + "<span class=\"ristorante\">"+ordine["Fornitore"]+"</span>"
                            + stato
                            + "<ul class=\"piatti\">";
        var piatti = ordine["Piatti"];
        var num_piatto = 1;
        for(; piatti[num_piatto]!= undefined; num_piatto++){
          var piatto = piatti[num_piatto];
          var modifiche = (piatto["Modifiche"] != undefined ? " ("+piatto["Modifiche"].trim().substr(2)+")" : "");
          string_ordine +=
                              "<li class=\"piatto\">"
                              + "<span class=\"nome\">"+piatto["Piatto"]+modifiche+"</span>"
                              + "<span class=\"quantità-piatto\"> x " + piatto["Quantita"]+ "</span>"
                              + "</li>";
        }
        string_ordine += "</ul><span class=\"prezzo\">"+ordine["Prezzo"]+" €</span></li>";
        $(posizione).prepend(string_ordine);
      }
      if($("ul.in-corso").children().length == 0) $("ul.in-corso").after(no_ordini);
      if($("ul.conclusi").children().length == 0) $("ul.conclusi").after(no_ordini);
    }
  });
}

$(document).ready(function(){

  //Carica ordini al primo caricamento della pagina
  loadOrdini();

  //Aggiorna gli ordini ogni 30 secondi
  setInterval(loadOrdini,30000);

});

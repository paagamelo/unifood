var modifying = false; //Stato pagina
var tel = ""; //Valore attuale del telefono
var usr = ""; //Valore attuale di username
var idDatore = 0; //Valore attuale del datore (solo per i fattorini)
var listaDatori = []; //Lista datori di lavoro disponibili
var change_status = true;
var atype = "clienti";
var meno = "<span class=\"symbol minus\" aria-label=\"Rimuovi\"><i aria-hidden=\"true\" class=\"fas fa-minus-circle fa-lg\"></i></span>";
var più = "<span class=\"symbol plus\" aria-label=\"Aggiungi\"><i aria-hidden=\"true\" class=\"fas fa-plus-circle fa-lg\"></i></span>";

function caricaDatoriLavoro(){
  var display_datori = "";
  for (datore in listaDatori){
    var selected = (idDatore == datore ? " selected" : "");
    display_datori += "<option value=\""+datore+"\""+selected+" >"+listaDatori[datore]+"</option>"
  }
  $("#datore-lavoro").append(display_datori);
}

//Controlla correttezza dati in form
function checkDati(telefono, username){
  var errori = "";
  if(atype != "fornitori") errori +=  (atype == "clienti" ? checkTelefono(telefono, false) : checkTelefono(telefono, true));
  errori += checkUsername(username);

  return errori;
}

//Cambia modalità visualizzazione dati
function toggleDati(){
  if(modifying){
    var errori = "";
    var telefono = (atype == "fornitori" ? null : $("input[name=telefono]").val().trim());
    var username = $("input[name=username]").val().trim();
    var datore = (atype == "fattorini" ? $("#datore-lavoro").val() : null);

    errori = checkDati(telefono, username);

    if(atype == "fattorini") errori += checkDatore(datore);
    if(errori != ""){ //Dati inseriti non corretti, mostra modal
      $("#modal-file").append("<header><h3>Errori in Form</h3></header><ul>" + errori + "</ul>");
      $(".modal").show();
      change_status = false; //Non bisogna uscire dalla modalità di modifica
    } else{ // Dati inseriti correttamente, rimuovo input
      if(atype != "fornitori") {
        $("<span class=\"telefono-lb label\">Telefono:</span>").replaceAll("label[for=telefono]");
        $("<span class=\"telefono\">"+telefono+"</span>").replaceAll("input[name=telefono]");
      }
      if(atype == "fattorini"){
        $("<span class=\"datore-lavoro-lb label\">Datore di lavoro:</span>").replaceAll("label[for=datore-lavoro]");
        $("<span class=\"datore-lavoro\">"+listaDatori[datore]+"</span>").replaceAll("select[name=datore-lavoro]");
      }
      $("<span class=\"username-lb label\">Username:</span>").replaceAll("label[for=username]");
      $("<span class=\"username\">"+username+"</span>").replaceAll("input[name=username]");

      if(usr != username || (atype == "fornitori" ? 0 : tel != telefono) || (atype == "fattorini" ? idDatore != datore : 0)){ //I dati sono stati modificati
        var data = "";
        if(atype == "fornitori"){
          data = {
            request: "dati",
            username : username
          };
        } else if (atype == "fattorini"){

          data = {
            request: "dati",
            username : username,
            telefono : telefono,
            datore : (datore != 0 ? datore : null)
          };
        } else {
          data = {
            request: "dati",
            username : username,
            telefono : telefono
          };
        }
        $.post("./aggiorna_impostazioni.php",data,
        function(state){
          var messaggio = "";
          if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
          else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
          if(messaggio != ""){ //Si è verificato qualche errore, mostra modal
            $("#modal-file").append("<header><h3>Operazione non riuscita</h3></header><div>"+messaggio+"</div>");
            $(".modal").show();
            //Reset a dati iniziali
            if(atype == "fattorini") $("span.datore-lavoro").html(listaDatori[idDatore]);
            if(atype != "fornitori") $("span.telefono").html(tel);
            $("span.username").html(usr);
          }
        });
      }
    }
  } else{ //Inserisco gli input
    if(atype != "fornitori"){
      tel = $("span.telefono").html();
      $("<label for=\"telefono\">Telefono:</label>").replaceAll("span.telefono-lb");
      $("<input type=\"tel\" name=\"telefono\" id=\"telefono\" value=\""+ tel + "\" maxlength=\"10\">").replaceAll("span.telefono");
    }
    if(atype == "fattorini"){
      idDatore = listaDatori.indexOf($("span.datore-lavoro").html());
      $("<label for=\"datore-lavoro\">Datore di lavoro:</label>").replaceAll("span.datore-lavoro-lb");
      $("<select class=\"form-control\" id=\"datore-lavoro\" name=\"datore-lavoro\"></select>").replaceAll("span.datore-lavoro");
      caricaDatoriLavoro();
    }
    usr = $("span.username").html();
    $("<label for=\"username\">Username:</label>").replaceAll("span.username-lb");
    $("<input type=\"text\" name=\"username\" id=\"username\" value=\""+ usr + "\" maxlength=\"15\">").replaceAll("span.username");
  }
}

//Cambia modalità visualizzazione pagamenti
function togglePagamenti(){
  if(modifying){
    $(".minus").remove();
    $(".aggiungi-carta").remove();
  } else {
    $("li.carta-pagamento").prepend(meno);
    $("#metodi-pagamento").append("<div class=\"aggiungi-carta\">Aggiungi carta"+più+"</div>");
  }
}

//Pulsante modifica/fine dati/telefono premuto
function toggleModify(classe) {
  if(classe == "payment-card") togglePagamenti();
  else toggleDati();
  if(change_status){
    if(modifying) { /* close modifying stuff */
      $("button.modify-btn:not(."+classe+")").css("opacity","1");
      $("button.modify-btn:not(."+classe+")").removeAttr("disabled");
      $("button."+classe).html("Modifica");
    } else{
      $("button.modify-btn:not(."+classe+")").css("opacity","0.6");
      $("button.modify-btn:not(."+classe+")").attr("disabled","disabled");
      $("button."+classe).html("Fine");
    }
    modifying = !modifying;
  }
  change_status = true;
}

//Carica dati
function loadDati(){
  $.post("./richiesta_impostazioni.php",
  {
    request: "dati"
  },
  function(data){
    var res = JSON.parse(data);
    if(res["Errore"] == undefined){ //Non ci sono stati errori
      usr = res["Username"];
      $("span.username").html(usr);
      if(atype != "fornitori"){ //Aggiorna i dati del telefono solo se non si tratta di un fornitore
        tel = res["Telefono"];
        $("span.telefono").html(tel);
      }
      if(atype == "fattorini"){
        idDatore = res["Datore"];
        listaDatori = res["Datori"];
        $("span.datore-lavoro").html(listaDatori[idDatore]);
      }
    }
  });
}

//Carica carte
function loadCarte(){ //Evento generabile solo dai clienti
  if(atype == "clienti"){ //Controllo che sia un cliente
    $.post("./richiesta_impostazioni.php",
    {
      request: "carte"
    },
    function(data){
      var res = JSON.parse(data);
      if(res["Errore"]!= undefined) $("ul.carte-pagamento").after("<div class=\"no-carte\">"+res["Errore"]+"</div>");
      else{ //Non si sono verificati errori, mostra carte
        var pre_string =
                        "<li class=\"carta-pagamento\">"
                        + "<span class=\"symbol card\"><i class=\"far fa-credit-card\"></i></span>";
        var i = 1;
        for(; res[i]!= undefined; i++){
          var dati =
                   "<span class=\"numero-carta\">***"+res[i]["Numero"]+"</span>"
                   + "<span class=\"tipo-carta\">"+res[i]["Tipo"]+"</span></li>";
          $("ul.carte-pagamento").append(pre_string+dati);
        }
      }
    });
  }
}

$(document).ready(function(){

  var tel_ex = !checkEmpty(document.getElementsByClassName("telefono")[0]);
  var mdp_ex = !checkEmpty(document.getElementById("metodi-pagamento"));

  if(!tel_ex && !mdp_ex) atype = "fornitori";
  else if(tel_ex && !mdp_ex) atype = "fattorini";

  //Carica dati al primo caricamento della pagina
  loadDati();
  if(atype == "clienti") loadCarte(); //Solo per i clienti

  //Modifica dati personali
  $(document).on("click", ".personal-data", function() {
    toggleModify("personal-data");
  });

  //Modifica metodi di pagamento
  $(document).on("click", ".payment-card", function() { //Evento generabile solo dai clienti
    if(atype == "clienti") toggleModify("payment-card"); //Controllo che sia un cliente
  });

  //Rimuovi carta
  $(document).on("touchend mouseup", ".minus", function(){ //Evento generabile solo dai clienti
    if(atype == "clienti"){ //Controllo che sia un cliente
      var numero = $(this).next().next().html();
      var tipo = $(this).next().next().next().html();
      var carta = $(this).parent(); //Carta da rimuovere
      $.post("./aggiorna_impostazioni.php",
      {
        request: "rimuovi-carta",
        numero: numero,
        tipo: tipo
      },
      function(state){
        var messaggio = "";
        if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
        else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
        if(messaggio != ""){ //Si è verificato qualche errore, mostra modal
          $("#modal-file").append("<header><h3>Operazione non riuscita</h3></header><div>"+messaggio+"</div>");
          $(".modal").show();
        } else{ //Rimozione avvenuta
          carta.remove();
          if($("ul.carte-pagamento").children().length == 0) $("ul.carte-pagamento").after("<div class=\"no-carte\">Nessun metodo di pagamento disponibile</div>");
        }
      });
    }
  });

  //Aggiungi carta
  $(document).on("touchend mouseup", ".plus", function(){ //Evento generabile solo dai clienti
    if(atype == "clienti"){ //Controllo che sia un cliente
      $("#modal-file").load("./aggiungi_carta.html");
      $(".modal").show();
    }
  });

  //Modifica password
  $(document).on("click", ".password", function(){
    $("#modal-file").load("./modifica_password.html");
    $(".modal").show();
  });

  //Chiudi modal
  $(document).on("touchend mouseup", ".close", function(){
    setTimeout(function() {
      $(".modal").hide();
      $("#modal-file").children().remove();
    }, 100);
  });
});

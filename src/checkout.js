var orario;
var piano;
var note;

//Carica carte
function loadCarte(){
  $.post("./richiesta_impostazioni.php",
  {
    request: "carte"
  },
  function(data){
    var res = JSON.parse(data);
    if(res["Errore"]!= undefined && res["Errore"] != 'Nessun metodo di pagamento disponibile') {
      $("#pagamento form").html('<p>Errore di comunicazione con il server</p>');
    } else { //Non si sono verificati errori, mostra carte
      var html = "";
      var i = 1;
      for(; res[i]!= undefined; i++){
        html += '<div class="row"><label for="' + res[i]["Numero"] + '" class="visuallyhidden">'
          + res[i]["Tipo"] + " " + res[i]["Numero"] + '</label>'
          + '<input type="radio" name="pagamento" value="' + res[i]["Numero"]
          + '" id="' + res[i]["Numero"] + '">'
          + '<span class="symbol card"><i class="far fa-credit-card"></i></span>'
          + '<span class="numero-carta">***' + res[i]["Numero"] + '</span>'
          + '<span class="tipo-carta">' + res[i]["Tipo"] + '</span><br/></div>';
      }
      $("#pagamento form .row:first").after(html);
    }
  });
}

$(document).ready(function() {
  $("#pagamento").hide();
  $("#esito").hide();

  loadCarte();

  $(document).on("click", "#completa input[value='Indietro']", function() {
    var html = '<p>Proseguendo perderai il tuo ordine</p><button type="button" id="prosegui">Prosegui</button><button type="button" id="annulla">Annulla</button>';
    $("#modal-file").html(html);
    $(".modal").show();
      //window.location.href = "./fornitore.php?id=" + $("#riepilogo").attr('data-idfornitore');
  });

  $(document).on("click", "#annulla", function() {
    $("#modal-file").html("");
    $(".modal").hide();
  });

  $(document).on("click", "#prosegui", function() {
    window.location.href = "./fornitore.php?id=" + $("#riepilogo").attr('data-idfornitore');
  });

  $(document).on("click", "#completa input[type='submit']", function(e) {
    e.preventDefault();
    $("#completa .error").remove();
    orario = $("#orario").val().trim();
    piano = $("#piano").val().trim();
    note = $("#note").val().trim();
    var errori =
                  checkOrario(orario)
                + checkOrarioPrecedente(orario)
                + (checkRangeNumber(piano, 1, 3) ? "<li>Inserire un piano valido</li>" : "")
                + (note.length <= 100 ? "" : '<li>Note non valide</li>');
    if(errori != "") {
      $("#completa").append('<div class="error"><ul>' + errori + "</ul></div>");
    } else {
      $("#completamento").hide();
      $("#pagamento").show();
    }
  });

  $(document).on("click", ".aggiungi-carta", function() {
    $("#modal-file").load("./aggiungi_carta.html");
    $(".modal").show();
  });

  //Chiudi modal
  $(document).on("click", ".close", function(){
    $(".modal").hide();
    $("#modal-file").children().remove();
  });

  $(document).on("click", "#pagamento input[value='Indietro']", function() {
    $("#pagamento").hide();
    $("#completamento").show();
  });

  $(document).on("click", "#pagamento input[value='Invia']", function() {
    event.preventDefault();
    var numero = $("#pagamento input[name='pagamento']:checked").val();
    var tipo = $("#pagamento input[name='pagamento']:checked").siblings(".tipo-carta").html();
    var data;
    if(numero == "paga alla consegna") {
      data = {
        orario: orario,
        piano: piano,
        note: note,
        pagatoYN: 0
      }
    } else {
      data = {
        orario: orario,
        piano: piano,
        note: note,
        pagatoYN: 1,
        numero: numero,
        tipo: tipo
      }
    }
    $.post("./inserisci_ordine.php", data, function(state) {
      $("#pagamento").hide();
      $("#esito").show();
      if(state === "success") {
        $("#esito p").html('Il tuo ordine è stato ricevuto correttamente, <a href="./unifood.php">torna alla home</a>');
      } else {
        $("#esito p").html('Qualcosa è andato storto, <a href="./unifood.php">torna alla home</a>');
      }
      console.log(state);
    });

  });
});

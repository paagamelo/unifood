//Svuota form
function emptyForm(){
  $(".modal input").val("");
}

$(document).ready(function(){

  //Salva password
  $(document).on("click", "#salva-password", function() {
    event.preventDefault();
    $(".error").remove(); //Rimuovi errori precedentemente visualizzati
    var n_pwd = $("#n_pwd").val();
    var c_pwd = $("#c_pwd").val();
    var a_pwd = $("#a_pwd").val();

    var errori =
                    checkPassword(a_pwd).replace("password", "password attuale")
                  + checkPassword(n_pwd).replace("password", "nuova password");
    if(n_pwd == c_pwd){ //La nuova password e la conferma corrispondono
      if(errori != ""){ //Password inserite invalide, mostra errore
        $("#modal-file").append("<div class=\"error\"><ul>"+errori+"</ul></div>");
        emptyForm();
      } else { //Password inserite valide
        a_pwd = hex_sha512(a_pwd);
        n_pwd = hex_sha512(n_pwd);
        $.post("./aggiorna_impostazioni.php",
        {
          request: "password",
          attuale: a_pwd,
          nuova: n_pwd
        },
        function(state){
          var messaggio = "";
          if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
          else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
          else if(state == "not-correct") messaggio = "La password attuale non è corretta";
          if(messaggio != "") { //Si sono verificati degli errori, mostrali
            $("#modal-file").append("<div class=\"error\">"+messaggio+"</div>");
            emptyForm();
          }else{ //Nessun errore, chiudi modal
            $(".modal").hide();
            $("#modal-file").children().remove();
          }
        });
      }
    } else { //Le nuova password e la conferma non corrispondono
      $("#modal-file").append("<div class=\"error\">La password di conferma non corrisponde alla nuova password inserita</div>");
      emptyForm();
    }
  });
});

<!DOCTYPE html>
<?php
require 'db_connect.php';
require 'login_functions.php';

sec_session_start(); //Avvio sessione php sicura
$logged = loggedAs($conn,"clienti");
if($logged) { //Login effettuato
?>
  <html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Font Awesome (for icons) -->
    <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>
    <!-- Topbar style -->
    <link rel="stylesheet" type="text/css" href="./topbar/topbar_style.css">
    <link rel="stylesheet" type="text/css" href="clienti_ordini_style.css">
    <link rel="stylesheet" type="text/css" href="breadcrumb.css">
    <script src="./clienti_ordini.js"></script>
    <title>I Tuoi Ordini</title>
  </head>
  <body>
    <header>
      <h1>I Tuoi Ordini</h1>
    <ul class="breadcrumb">
      <li><a href="./unifood.php">Home</a></li>
      <li>Ordini</li>
    </ul>
    </header>
    <section id="ordini-in-corso">
      <header><h2>In corso</h2></header>
      <ul class="ordini in-corso">
        <!--Ordini in corso -->
      </ul>
    </section>
    <section id="ordini-conclusi">
      <header><h2>Conclusi</h2></header>
      <ul class="ordini conclusi">
        <!--Ordini conclusi -->
      </ul>
    </section>
    <?php require './topbar/topbar.php'; closeConnection($conn);?>
  </body>
  </html>
<?php
} else header('Location: ./'.closeConnectionAndReturn($conn,"login.php?Atype=clienti")); //Effettua il login per accedere
?>

<?php
require 'db_connect.php';
require 'login_functions.php';
require 'common_queries.php';
include_once('informazioni_ordini_fornitori.php');

sec_session_start(); //Avvio sessione php sicura
if($conn->connect_error) {
  die("Impossibile connettersi al database");
}
$logged = login_check($conn);
$header = "";
$error = "";
if(!isset($_GET['id'])) {
  die(closeConnectionAndReturn($conn, "Impossibile trovare il fornitore"));
}
$idfornitore = $_GET['id'];
getInfoFornitore($conn, $idfornitore, $fornitore);
if((isset($fornitore["errore"]) && $fornitore["errore"]) || empty($fornitore)) {
  die(closeConnectionAndReturn($conn, "Impossibile trovare il fornitore"));
}
$header = $fornitore["Nome"];
getPiattiFornitore($conn, $idfornitore, $categorie);
if(isset($categorie["errore"]) && $categorie["errore"]) {
  die(closeConnectionAndReturn($conn, "Errore"));
}
$previous_url = "./fornitori.php";
$previous_name = "Fornitori";
if(isset($_SERVER['HTTP_REFERER'])) {
  $referer = $_SERVER['HTTP_REFERER'];
  $query = parse_url($referer, PHP_URL_QUERY);
  if($query != null) {
    $idcucina = explode("=", $query)[1];
    getInfoCucina($conn, $idcucina, $cucina);
    if(!(isset($cucina["errore"]) && $cucina["errore"]) && !empty($cucina)) {
      $previous_url = $referer;
      $previous_name .= " di ".$cucina["Nome"];
    }
  }
}
$c = countOrdiniFornitore($conn, $idfornitore, 'In preparazione');
if($c < 0) {
  die(closeConnectionAndReturn($conn, "Errore"));
}
$max = $fornitore["MaxOrdiniContemporaneamente"];
if($max == NULL) {
  $isfull = false;
} else {
  $isfull = $c >= $max;
}

$stmt = $conn->prepare("SELECT * FROM GIORNI_LIBERI WHERE IdFornitore = ?");
if(
     !$stmt
  || !$stmt->bind_param("i", $idfornitore)
  || !$stmt->execute())
{
  die(closeConnectionAndReturn($conn, "Errore"));
}

$result = $stmt->get_result();
$stmt->close();
$closed = false;
$closing_days = array();
while($row = $result->fetch_assoc()) {
  $closing_days[] = $row;
  if($row["IdGiorno"] == date('N')) {
    $closed = true;
  }
}

$closed = $closed || (date("H:i") > $fornitore['ApertoA']) || (date("H:i") < $fornitore["ApertoDa"]);

$f = function ($day_) {
  $day = "";
  switch($day_["IdGiorno"]) {
    case '1': $day = 'Lunedì'; break;
    case '2': $day = 'Martedì'; break;
    case '3': $day = 'Mercoledì'; break;
    case '4': $day = 'Giovedì'; break;
    case '5': $day = 'Venerdì'; break;
    case '6': $day = 'Sabato'; break;
    case '7': $day = 'Domenica'; break;
  }
  return $day;
};

$closing_days_ = array_map($f, $closing_days);

?>
<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- JQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- Font Awesome (for icons) -->
  <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>
  <!-- Topbar style -->
  <link rel="stylesheet" type="text/css" href="./topbar/topbar_style.css">
  <link rel="stylesheet" href="breadcrumb.css">
  <link rel="stylesheet" href="fornitore.css">
  <link rel="stylesheet" href="alert.css">
  <script src="./fornitore.js"></script>
  <script src="./carrello.js"></script>
  <title><?php echo $header; ?></title>
</head>
<body>
  <div class="main">
  <?php
  if($closed) {
    echo '<div class="alert warning"><span class="closebtn">&times;</span><strong>Attenzione! </strong>Il fornitore è chiuso al momento</div>';
  } else if($isfull) {
    echo '<div class="alert warning"><span class="closebtn">&times;</span><strong>Attenzione! </strong>Il fornitore non riceve altri ordini per il momento</div>';
  }
  ?>
  <?php require "./topbar/topbar.php"; ?>
  <div class="fornitore-header">
    <div id="logo-area">
      <?php
      $dir = "./images/loghi/";
      $filename = "logo".$idfornitore."*";
      $file = glob($dir.$filename);
      if(empty($file)) $file = $dir."default.png";
      else $file = $file[0];
      ?>
      <img src="<?php print $file; ?>" alt="logo" class=".logo" id="logo-img">
    </div>
    <div id="info">
      <header><h1><?php echo $header; closeConnection($conn); ?></h1></header>
      <?php
        $html = '<span class="indirizzo" aria-label="indirizzo"><i class="fas fa-map-pin fa-fw" aria-hidden="true"></i>'
          .$fornitore["Indirizzo"].'</span><span class="telefono" aria-label="telefono"><i class="fas fa-phone fa-fw" aria-hidden="true"></i>'
          .$fornitore["Telefono"].'</span><span class="speseconsegna" data-value="'.$fornitore["SpeseConsegna"].'">Spese di consegna: € '
          .$fornitore["SpeseConsegna"].'</span><span class="orarioapertura">Aperto: '
          .$fornitore["ApertoDa"].' - '.$fornitore["ApertoA"].'</span>';
        if($closing_days != NULL && !empty($closing_days)) {
          $html .= '<span class="chiusura">Giorni di chiusura: '.implode(", ", $closing_days_).'</span>';
        }
        if($fornitore["Note"] != NULL) {
          $html .= '<span class="note">Note: '.$fornitore["Note"].'</span>';
        }
        echo $html;
      ?>
    </div>
  </div>
  <ul class="breadcrumb">
    <li><a href="./unifood.php">Home</a></li>
    <li><?php echo '<a href="'.$previous_url.'">'.$previous_name.'</a></li>'; ?>
    <li><?php echo $header; ?></li>
  </ul>
  <section id="categorie-cibo">
    <header><h2>Categorie</h2></header>
    <?php
    $html = "";
    if(empty($categorie)) {
      $html = "<p>Nessuna categoria da mostrare</p>";
    } else {
      foreach($categorie as $categoria => $piatti) {
        $html .= '<a>'.$categoria.'<i class="fas fa-angle-right fa-lg"></i></a>';
        if(!empty($piatti)) {
          $html .= '<ul class="piatti">';
          foreach($piatti as $key => $piatto) {
            $html .= '<li data-id="'.$piatto["IdPiattoInFornitore"]
                  .'" data-modificabile="'.$piatto["ModificabileYN"] .'">';
            $html .= '<span class="nome">'.$piatto["Nome"].'</span>';
            if($closed || $isfull || ($logged && $_SESSION['atype'] != 'clienti')) {
              $html .= '<button type="button" class="inactive">+</button>';
            } else {
            //if(!$logged || $_SESSION['atype'] === 'clienti') {
              $html .= '<button type="button">+</button>';
            }
            $html .= '<div class="prezzo"><span class="currency">€</span><span class="value">'
                  .$piatto["Prezzo"].'</span></div>';
            if(!empty($piatto["ingredienti"])) {
              $html .= '<span class="ingredienti">';
              $first = true;
              foreach($piatto["ingredienti"] as $key => $ingrediente) {
                if(!$first) $html .= ", ";
                $first = false;
                $html .= $ingrediente["Nome"];
              }
              $html .= '</span></li>';
            }
          }
          $html .= '</ul>';
        }
      }
    }
    echo $html;
    ?>
  </section>
  <section id="piatti">
    <header><h2>Piatti</h2></header>
    <p class="content-placeholder">Seleziona una categoria</p>
  </section>
  <section id="carrello">
    <header><h2>Carrello</h2></header>
    <div class="carrello">
      <div class="carrello-row">
      <h3>Riepilogo ordine</h3>
      <button type="button" class="modify">Modifica</button>
    </div>
      <ul>
      <li class="content-placeholder">Il carrello è vuoto</li>
      </ul>
      <div class="total">
        <span class="value">0.90</span>
        <span class="currency">€</span>
      </div>
      <button type="button" class="checkout">Vai alla cassa</button>
    </div>
  </section>
  <div id="aggiungi" class="modal">
    <div class="modal-content">
      <span class="close">&times;</span>
      <p>Vuoi modificare <span class="selected-item"></span>?</p>
      <button type="button" class="modal-aggiungi">Aggiungi</button>
      <button type="button" class="modal-modifica">Modifica</button>
    </div>
  </div>
  <div id="modifica" class="modal">
    <div class="modal-content">
      <span class="close">&times;</span>
      <header><h3>Modifica </h3></header>
    </div>
  </div>
</div>
</body>
</html>

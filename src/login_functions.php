<?php

//Avvia sessione di php sicura
function sec_session_start() {
        $session_name = 'sec_session_id';
        $secure = false; // Impostare il parametro a true per utilizzare il protocollo 'https'
        $httponly = true; // Impedisco ad un javascript di accedere all'id di sessione
        ini_set('session.use_only_cookies', 1); // Forzo la sessione ad utilizzare solo i cookie.
        $cookieParams = session_get_cookie_params(); // Leggo i parametri correnti relativi ai cookie
        session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
        session_name($session_name); // Imposto il nome di sessione
        session_start(); // Avvio la sessione php
        session_regenerate_id(); // Rigenero la sessione e cancello quella creata in precedenza
}

//Effettua il login
function login($email, $password, $atype, $conn) {
   // Usando statement sql 'prepared' non sarà possibile attuare un attacco di tipo SQL injection
   if($conn->connect_error) return(1);
   $stmt = $conn->prepare("SELECT Id, Username, Password, Salt FROM ".strtoupper($atype)." WHERE Email = ? LIMIT 1");
   $stmt->bind_param('s', $email);
   if(!$stmt->execute()) return(1); //Errore server
   $stmt->store_result();
   $stmt->bind_result($user_id, $username, $db_password, $salt);
   $stmt->fetch();

   $password = hash('sha512', $password.$salt); // Codifico la password usando una chiave univoca
   if($stmt->num_rows == 0) return 3; //L'utente non esiste
   $stmt->close();
   if(checkbrute($user_id, $atype, $conn)) return 2; //Account disabilitato
   if($db_password == $password) { // Password corretta
     $user_browser = $_SERVER['HTTP_USER_AGENT']; // Recupero il parametro 'user-agent' relativo all'utente corrente
     $user_id = preg_replace("/[^0-9]+/", "", $user_id); // Permette di proteggersi da un attacco XSS
     $_SESSION['user_id'] = $user_id;
     $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username); // Permette di protegersi da un attacco XSS
     $_SESSION['username'] = $username;
     $_SESSION['login_string'] = hash('sha512', $password.$user_browser); //Creo una stringa di login criptata usando la password e il parametro'user-agent'
     $_SESSION['atype'] = $atype;
     delete_failed_attempts($user_id, $atype,$conn); //Rimuovo i tentativi sbagliati di accesso
     return 0; //Login eseguito con successo
   } else { // Password sbagliata, registro il tentativo fallito nel database
     $now = time();
     $conn->query("INSERT INTO LOGIN_ATTEMPTS (Id, Time, AccountType) VALUES ('$user_id', '$now', '$atype')");
     return 3; //Password sbagliata
   }
}

//L'account risulta disabilitato se sono stati effettuati più di 5 tentativi errati nelle ultime 2 ore
function checkbrute($user_id,  $atype, $conn) {
   $valid_attempts = time() - (2 * 60 * 60);
   $stmt = $conn->prepare("SELECT time FROM LOGIN_ATTEMPTS WHERE Id = ? AND AccountType = ? AND Time > ?");
   $stmt->bind_param('isi', $user_id, $atype, $valid_attempts);
   if(!$stmt->execute()) return true; //Errore query, per sicurezza assumo che l'account sia disabilitato
   $stmt->store_result();
   return ($stmt->num_rows > 5);
}

//Controllo che sia stato effettuato il login
function login_check($conn) {
  if($conn->connect_error) return false;
  if(!isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['login_string'], $_SESSION['atype'])) return false; //Non sono stati passati tutti i parametri richiesti
  $user_id = $_SESSION['user_id'];
  $login_string = $_SESSION['login_string'];
  $username = $_SESSION['username'];
  $user_browser = $_SERVER['HTTP_USER_AGENT']; // Recupero il parametro 'user-agent' relativo all'utente corrente
  $stmt = $conn->prepare("SELECT Password FROM ".strtoupper($_SESSION['atype'])." WHERE Id = ? LIMIT 1");
  $stmt->bind_param('i', $user_id);
  if(!$stmt->execute()) return false; //Errore nella query
  $stmt->store_result();
  if($stmt->num_rows == 0) return false; // L'utente non esiste
  $stmt->bind_result($password);
  $stmt->fetch();
  $login_check = hash('sha512', $password.$user_browser); //Creo una stringa di login criptata usando la password e il parametro'user-agent'
  return ($login_check == $login_string);
}

//Cripta la password
function cripta_password($password){
$result = array();
$result["Salt"] = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true)); // Creo una chiave casuale
$result["Password"] = hash('sha512', $password.$result["Salt"]); // Creo una password usando la chiave appena creata e quella inserita dall'utente
return $result;
}

//Svuota i tentativi errati di un utente, in seguito ad un login corretto
function delete_failed_attempts($id, $atype, $conn){
  if ($stmt = $conn->prepare("DELETE FROM LOGIN_ATTEMPTS WHERE Id = ? AND AccountType = ?")) { //Non ci sono errori nella query
     $stmt->bind_param('is', $id,$atype);
     $stmt->execute();
  }
}
//Controlla se l'utente è loggato con un determinato tipo di account
function loggedAs($conn,$atype){
  return login_check($conn) && $_SESSION["atype"] == $atype;
}

?>

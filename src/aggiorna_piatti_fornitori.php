<?php
require 'db_connect.php';
require 'common_queries.php';
require 'login_functions.php';
require 'checkDati.php';

function undoOrdine(&$conn, $idordine) {
  $stmt = $conn->prepare("DELETE FROM ORDINI WHERE Id = ?");
  if(  !$stmt
    || !$stmt->bind_param("i", $idordine)
    || !$stmt->execute())
  {
    return false;
  }
  return true;
}

function undoPiatti(&$conn, $idordine) {
  $stmt = $conn->prepare("DELETE FROM PIATTI_IN_ORDINE WHERE IdOrdine = ?");
  if(  !$stmt
    || !$stmt->bind_param("i", $idordine)
    || !$stmt->execute())
  {
    return false;
  }
  return true;
}

function undoModifiche(&$conn, $idordine) {
  $stmt = $conn->prepare("DELETE FROM MODIFICHE WHERE IdOrdine = ?");
  if(  !$stmt
    || !$stmt->bind_param("i", $idordine)
    || !$stmt->execute())
  {
    return false;
  }
  return true;
}

function undoIngredientiPiatto(&$conn, $idpiatto, $idfornitore) {
  $stmt = $conn->prepare("DELETE FROM PIATTI_INGREDIENTI WHERE IdPiattoInFornitore = ? AND IdFornitore = ?");
  if(  !$stmt
    || !$stmt->bind_param("ii", $idpiatto, $idfornitore)
    || !$stmt->execute())
  {
    return false;
  }
  return true;
}

function checkAggiungi($cucina,$categoria,$nome,$prezzo,$modificabile,$ingredienti){
  return
             checkId($cucina)
          && checkId($categoria)
          && checkCosto($prezzo)
          && checkDimension($nome,1,25)
          && ($modificabile == 1 || $modificabile == 0)
          && checkIngredienti($ingredienti);
}

sec_session_start(); //Avvio sessione php sicura
if(loggedAs($conn, "fornitori") && isset($_POST["request"])) { //Login come fornitore effettuato e richiesta in post valida

   if ($conn->connect_error) die("error");

   $state = "";
   $id = $_SESSION["user_id"];

   switch ($_POST["request"]) {
      case "rimuovi": //Rimuovi piatto del fornitore

        $piatto = trim($_POST["piatto"]);
        $ordini = [];

        if(!is_numeric($piatto)) die(closeConnectionAndReturn($conn,"dati-invalidi"));


        $stmt = $conn->prepare("SELECT DISTINCT p.IdOrdine FROM PIATTI_IN_ORDINE p WHERE p.IdPiattoInFornitore = ? AND p.IdFornitore = ?");
        if($stmt == false) die(closeConnectionAndReturn($conn,"query-error"));
        $stmt->bind_param("ss", $piatto, $id);
        if(!$stmt->execute()) die(closeConnectionAndReturn($conn,"query-error"));
        $result = $stmt->get_result();
        if($result != false) while($row = $result->fetch_assoc()) $ordini[] = $row["IdOrdine"];
        else die(closeConnectionAndReturn($conn,"query-error"));

        $stmt->close();

        foreach($ordini as $ordine) if(!undoModifiche($conn, $ordine) || !undoPiatti($conn,$ordine) || !undoOrdine($conn,$ordine)) die(closeConnectionAndReturn($conn,"query-error"));

        if(!undoIngredientiPiatto($conn,$piatto,$id)) die(closeConnectionAndReturn($conn,"query-error"));

        $stmt = $conn->prepare("DELETE FROM PIATTI WHERE IdFornitore = ? AND IdPiattoInFornitore = ?");
        if($stmt == false) die(closeConnectionAndReturn($conn,"query-error"));
        $stmt->bind_param("ss", $id, $piatto);
        $state = ($stmt->execute() == false ? "error" : "ok");
        $stmt->close();

        print $state;
        break;

      case "aggiungi": //Aggiungi piatto del fornitore

          $cucina = trim($_POST["cucina"]);
          $categoria = trim($_POST["categoria"]);
          $nome = trim($_POST["nome"]);
          $prezzo = str_replace(",",".",trim($_POST["prezzo"]));
          $modificabile = trim($_POST["modificabile"]);
          $ingredienti = $_POST["ingredienti"];

          if(!checkAggiungi($cucina,$categoria,$nome,$prezzo,$modificabile,$ingredienti)) die(closeConnectionAndReturn($conn,"dati-invalidi"));

          $exist = checkPiattoExists($conn, $id, $cucina, $categoria, $nome);

          if(isset($exist["Errore"])) die(closeConnectionAndReturn($conn,"error"));
          else if(is_numeric($exist["Result"])) die(closeConnectionAndReturn($conn,"yet-present"));

          $stmt = $conn->prepare("INSERT INTO PIATTI(IdFornitore, Nome, Prezzo, ModificabileYN, IdCategoriaInCucina, IdCucina) VALUES(?,?,?,?,?,?)");
          if($stmt == false) die(closeConnectionAndReturn($conn,"query-error"));
          $stmt->bind_param("ssssss", $id, $nome, $prezzo, $modificabile, $categoria, $cucina);
          $state = ($stmt->execute() == false ? "error" : "ok");
          $stmt->close();

          if($state != "ok") die(closeConnectionAndReturn($conn,$state));

          $stmt = $conn->prepare("SELECT IdPiattoInFornitore FROM PIATTI WHERE Nome = ? AND IdFornitore = ? AND IdCucina = ? AND IdCategoriaInCucina = ?");
          if($stmt == false) die(closeConnectionAndReturn($conn,"query-error"));
          $stmt->bind_param("ssss", $nome, $id, $cucina, $categoria);
          if($stmt->execute() == false) die(closeConnectionAndReturn($conn,"query-error"));
          $stmt->bind_result($state);
          $stmt->fetch();

          if(!is_numeric($state)) die(closeConnectionAndReturn($conn,"query-error"));

          $stmt->close();

          $stmt = $conn->prepare("INSERT INTO PIATTI_INGREDIENTI (IdFornitore, IdPiattoInFornitore, IdIngrediente) VALUES(?,?,?)");
          if($stmt == false) die(closeConnectionAndReturn($conn,"query-error"));
          $stmt->bind_param("sss", $id, $state, $ingrediente);
          foreach ($ingredienti as $ingrediente) if($stmt->execute() == false) $state = "query-error";
          $stmt->close();

          print $state;
          break;

        case "modifica": //Modifica piatto del fornitore

          $cucina = trim($_POST["cucina"]);
          $categoria = trim($_POST["categoria"]);
          $nome = trim($_POST["nome"]);
          $prezzo = str_replace(",",".",trim($_POST["prezzo"]));
          $modificabile = trim($_POST["modificabile"]);
          $idpiatto = trim($_POST["piatto"]);
          $ingredienti = $_POST["ingredienti"];

          if(!checkAggiungi($cucina,$categoria,$nome,$prezzo,$modificabile,$ingredienti) && !is_numeric($idpiatto)) die(closeConnectionAndReturn($conn,"dati-invalidi"));

          $exist = checkPiattoExists($conn,$fornitore, $cucina, $categoria, $nome);
          if(isset($exist["Errore"])) die(closeConnectionAndReturn($conn,"error"));
          else if(is_numeric($exist["Result"]) && $exist["Result"] != $idpiatto) die(closeConnectionAndReturn($conn,"yet-present"));

          $stmt = $conn->prepare("UPDATE PIATTI SET Nome = ?, Prezzo = ?, ModificabileYN = ?, IdCategoriaInCucina = ? WHERE IdPiattoInFornitore = ? AND IdFornitore = ?");
          if($stmt == false) die(closeConnectionAndReturn($conn,"query-error"));
          $stmt->bind_param("ssssss", $nome, $prezzo, $modificabile, $categoria, $idpiatto, $id);
          $state = ($stmt->execute() == false ? "error" : "ok");
          $stmt->close();

          if($state != "ok") die(closeConnectionAndReturn($conn,$state));

          $stmt = $conn->prepare("DELETE FROM PIATTI_INGREDIENTI WHERE IdFornitore = ? AND IdPiattoInFornitore = ?");
          if($stmt == false) die(closeConnectionAndReturn($conn,"query-error"));
          $stmt->bind_param("ss", $id, $idpiatto);
          $state = ($stmt->execute() == false ? "error" : "ok");
          $stmt->close();

          if($state != "ok") die(closeConnectionAndReturn($conn,$state));

          $stmt = $conn->prepare("INSERT INTO PIATTI_INGREDIENTI (IdFornitore, IdPiattoInFornitore, IdIngrediente) VALUES(?,?,?)");
          if($stmt == false) die(closeConnectionAndReturn($conn,"query-error"));
          $stmt->bind_param("sss", $id, $idpiatto, $ingrediente);
          foreach ($ingredienti as $ingrediente) if($stmt->execute() == false) $state = "query-error";
          $stmt->close();

          print $state;
          break;
  }
  closeConnection($conn);
} else die(closeConnectionAndReturn($conn,"error"));
?>

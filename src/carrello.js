function Ingredient(id, name, isAddition) {
  this.id = id;
  this.name = name;
  this.isAddition = isAddition;
}

function Item(id, name, quantity, modifiable, price, modifications) {
  this.id = id;
  this.name = name;
  this.quantity = quantity;
  this.isModifiable = modifiable;
  this.price = price; //unitario
  this.modifications = modifications;
}

function Cart() {
  this.items = [];
  this.add = function(id, name, modifiable, price, modifications) {
    var found = false;
    for(key in this.items) {
      if(this.items[key].id === id && this.items[key].modifications == modifications) {
        this.items[key].quantity++;
        found = true;
      }
    }
    if(!found) {
      this.items.push(new Item(id, name, 1, modifiable, price, modifications));
    }
  }
  this.increase = function(key) {
    if(key in this.items) {
      this.items[key].quantity++;
      return true;
    } else {
      return false;
    }
  }
  this.remove = function(key) {
    if(key in this.items) {
      delete this.items[key];
    }
  }
  this.decrease = function(key) {
    if(key in this.items) {
      this.items[key].quantity--;
      if(this.items[key].quantity == 0) {
        delete this.items[key];
        return true;
      }
      return false;
    }
  }
}

var cart = new Cart();
var empty = true;

function shaky() {
  $("#carrello-btn svg").addClass("shake");
  $("#carrello-btn").addClass("glow");
  setTimeout (function(){
    $("#carrello-btn svg").removeClass("shake");
    $("#carrello-btn").removeClass("glow"); },
  800);
}

$(document).ready(function() {

  //displayAlert('sajmxas');

  var idfornitore = new URL(window.location.href).searchParams.get("id");
  var speseconsegna = $("#info .speseconsegna").attr('data-value');

  if($("#carrello").css("display") == "none") { //mobile
    $("#carrello-topbar").css("display","block");
  }
  /* show/hide the shopping cart ('carrello') on topbar on mobile/desktop */
  $(window).resize(function() {
    if($("#carrello").css("display") == "none") { //mobile
      $("#carrello-topbar").css("display","block");
    } else { //desktop
      $("#carrello-topbar").css("display","none");
    }
  });

  /* shopping cart modify handling */
  var modifying = false;

  function toggleModify() {
    if(modifying) { /* close modifying stuff */
      $(".carrello .minus").css("display", "none");
      $(".carrello .plus").css("display", "none");
      $(".carrello .item-modify-area").css("display", "none");
      $(".modification.last").css("margin-bottom", "0px");
      $(".carrello .item-quantity").each(function() {
        $(this).css("margin-left","0.5em");
        $(this).html("x" + $(this).html());
        $(this).siblings(".item-price").css("margin-top", "0.09em");
      });
      $(".carrello .modify").html("Modifica");
    } else { /* open modifying stuff */
      $(".carrello .minus").css("display", "inline-block");
      $(".carrello .plus").css("display", "inline-block");
      $(".carrello .item-modify-area").css("display", "inline-block");
      $(".modification.last").css("margin-bottom", "0.4em");
      $(".carrello .item-quantity").each(function() {
        $(this).css("margin-left","0px");
        $(this).html($(this).html().substring(1,2));
        $(this).siblings(".item-price").css("margin-top", "0.4em");
      });
      $(".carrello .modify").html("Fine");
    }
    modifying = !modifying;
  }

  $(document).on("click", ".modify", function() {
    toggleModify();
  });

  /* hides some controls and inserts a placeholder (cart is empty) */
  function emptyCart() {
    //$(".carrello-row, .carrello h3, .carrello .modify, .carrello .total, .carrello .checkout").css("display", "none");
    $(".carrello .modify, .carrello .total, .carrello .checkout").css("display", "none");
    $(".carrello ul").html('<li class="content-placeholder">Il carrello è vuoto</li>');
  }

  /* show some controls and removes a placeholder (cart is not empty) */
  function firstItemAdded() {
    //$(".carrello h3, .carrello .modify, .carrello .checkout").css("display", "inline-block");
    //$(".carrello-row").css("display", "block");
    $(".carrello .modify, .carrello .checkout").css("display", "inline-block");
    $(".carrello .total").css("display", "block");
  }

  /* adding items to the shopping cart ('carrello') */
  function add(id, nome, modifiable, prezzo, ingredients) {
    shaky();
    /* toggle modify on shopping cart if necessary */
    if(modifying) {
      toggleModify();
    }
    /* first item added */
    if($(".carrello .content-placeholder").length > 0) {
      firstItemAdded();
    }
    /* actually adding item */
    cart.add(id, nome, modifiable, prezzo, ingredients);
    /* re-show shopping cart ('carrello') */
    var list = "";
    var total = 0;
    for(key in cart.items) {
      list += '<li data-id="' + cart.items[key].id + '" data-key="'
              + key + '"><div class="row"><span class="item-name">'
              + cart.items[key].name + '</span>'
              + '<button type="button" class="minus">-</button><span class="item-quantity">x'
              //+ '<span class="item-quantity">x'
              + cart.items[key].quantity + '</span><button type="button" class="plus">+</button><div class="item-price"><span class="currency">€</span><span class="value">'
              //+ cart.items[key].quantity + '</span<div class="item-price"><span class="currency">€</span><span class="value">'
              + cart.items[key].price + '</span></div></div>';
      if(cart.items[key].modifications !== null && Object.keys(cart.items[key].modifications).length > 0) {
        for(var m in cart.items[key].modifications) {
          list += '<div class="modification';
          if(m == cart.items[key].modifications.length - 1) {
            list += ' last">';
          } else {
            list += '">';
          }
          list += '<span class="isaddition ' + (cart.items[key].modifications[m].isAddition ? 'yes">+' : 'no">-')
                + '</span>' + cart.items[key].modifications[m].name + '</div>';
        }
      }
      list += '<div class="item-modify-area"><button type="button" class="item-remove">Rimuovi</button>';
      if(cart.items[key].isModifiable) {
        list += '<button type="button" class="item-modify">Modifica</button>';
      }
      list += '</div></li>';
      total += cart.items[key].quantity * parseFloat(cart.items[key].price);
    }
    total += parseFloat(speseconsegna);
    $(".carrello ul").html(list);
    if(speseconsegna > 0) {
      $(".carrello ul").append('<li class="speseconsegna">Spese di consegna<div class="item-price"><span class="currency">€</span><span class="value">'
        + parseFloat(speseconsegna).toFixed(2) + '</span></div></li>');
    }
    $(".total>.value").html(total.toFixed(2));
  }


  /* adding item button listener */
  var piatto = {};

  $(document).on("click", ".piatti button", function() {
    if($(this).hasClass("inactive")) {
      return;
    }
    if($(this).parent().attr("data-modificabile") === "1") {
      piatto['id'] = $(this).parent().attr("data-id");
      piatto['nome'] = $(this).siblings(".nome").html();
      piatto['prezzo'] = $(this).siblings(".prezzo").find(".value").html();
      piatto['modifiche'] = 'empty';
      $("#aggiungi.modal").find(".selected-item").html(piatto['nome']);
      $("#aggiungi.modal").css("display", "block");
    } else { //not modifiable, simply add it
      add($(this).parent().attr("data-id"), $(this).siblings(".nome").html(),
        false, $(this).siblings(".prezzo").find(".value").html(), null);
    }
  });

  /* adding item without modifying it */
  $(document).on("click", "#aggiungi.modal .modal-aggiungi", function() {
    add(piatto['id'], piatto['nome'], true, piatto['prezzo'], null);
    $("#aggiungi.modal").css("display", "none");
  });

  function displayAlert(message) {
    $(".modal").css("display", "none");
    $(".alert").remove();
    $(".main").prepend('<div class="alert error"><span class="closebtn">&times;</span>'
      + '<strong>Errore!</strong>' + message + '</div>');
  }

  $(document).on("click", ".alert .closebtn", function() {
    $(this).parent(".alert").remove();
  })

  function modify(modifiche) {
    var url = "";
    if(modifiche != null && Object.keys(modifiche).length > 0) {
      url = "richiesta_ingredienti.php?idfornitore=" + idfornitore + "&idpiatto=" + piatto['id'] + "&modificato=1";
      for(var key in modifiche) {
        url += "&modifiche%5B%5D=" + modifiche[key].id;
      }
    } else {
      url = "richiesta_ingredienti.php?idfornitore=" + idfornitore + "&idpiatto=" + piatto['id'] + "&modificato=0";
    }
    $.getJSON(url,
    function(ingredienti) {
      if(ingredienti["errore"] === "errore") {
        displayAlert(" Qualcosa è andato storto");
      } else {
        if(modifiche != null && Object.keys(modifiche).length > 0) {
          piatto['ingredienti']['strutturali'] = (ingredienti['default']).filter(function(i){return i["StrutturaleYN"] == '1';});
          piatto['ingredienti']['rimovibili'] = (ingredienti['default']).filter(function(i){return !(i["StrutturaleYN"] == '1');});
          var keys = Object.keys(ingredienti['fornitore']).filter(function(k){return !(ingredienti['fornitore'][k]["StrutturaleYN"] == '1');});
          piatto['ingredienti']['aggiungibili'] = keys.reduce((r, k) => r.concat(ingredienti['fornitore'][k]), []);//Object.keys(ingredienti['fornitore']).filter(function(k){return !(i["StrutturaleYN"] == '1');});
        } else {
          piatto['ingredienti'] = ingredienti;
        }
        //console.log(piatto['ingredienti']);
        $("#modifica.modal>.modal-content").attr('data-id', piatto['id']);
        $("#modifica.modal>.modal-content").html('<span class="close">&times;</span><header><h3>Modifica <span>' + piatto['nome'] + '</span></header>');
        var html = "";

        var strutturali = ingredienti['strutturali'];
        if(true){//strutturali != null && Object.keys(strutturali).length > 0) {
          $("#modifica.modal>.modal-content").append('<div class="ingredienti-strutturali"><ul></ul></div>');
          for(var k in strutturali) {
            html += '<li data-id="' + strutturali[k]["Id"] + '"><div class="strutturale-row"><span>' + strutturali[k]["Nome"] + '</span>';
            var strutturali_alt = strutturali[k]['alt'];
            if(strutturali_alt != null && Object.keys(strutturali_alt).length > 0) {
              html += '<button type="button">Modifica</button></div><div class="strutturale-alt">'
                + '<label><input type="radio" name="' + strutturali[k]["Id"] + '" value="' + strutturali[k]["Id"] + '" checked="checked">'
                + strutturali[k]["Nome"] + '</label><div class="prezzo"><span class="currency">€</span><span class="value">'
                + strutturali[k]["CostoAggiunta"] + '</span></div><br/>';
              for(var k_a in strutturali_alt) {
                html += '<label><input type="radio" name="' + strutturali[k]["Id"] + '" value="' + strutturali_alt[k_a]["Id"] + '">'
                  + strutturali_alt[k_a]["Nome"] + '</label><div class="prezzo"><span class="currency">€</span><span class="value">'
                  + strutturali_alt[k_a]["CostoAggiunta"] + '</span></div><br/>';
              }
              html += '</div>';
            }
            html += '</li>';
          }
          $("#modifica.modal .ingredienti-strutturali").find("ul").append(html);
        }

        var rimovibili = ingredienti['rimovibili'];
        if(true) {//rimovibili != null && Object.keys(rimovibili).length > 0) {
          $("#modifica.modal>.modal-content").append('<div class="ingredienti-rimovibili"><ul></ul></div>');
          html = "";
          for(var k in rimovibili) {
            html += '<li data-id="' +  rimovibili[k]["Id"] + '"><span>' + rimovibili[k]["Nome"] + '</span>'
              + '<button type="button">-</button><div class="prezzo"><span class="currency">€</span><span class="value">'
              + rimovibili[k]["CostoAggiunta"] + '</span></div>';
          }
          $("#modifica.modal .ingredienti-rimovibili").find("ul").append(html);
        }

        var aggiungibili = ingredienti['aggiungibili'];
        if(true) {//aggiungibili != null && Object.keys(aggiungibili).length > 0) {
          $("#modifica.modal>.modal-content").append('<div class="ingredienti-aggiungibili"><ul></ul></div>');
          html = "";
          for(var k in aggiungibili) {
            html += '<li data-id="' +  aggiungibili[k]["Id"] + '"><span>' + aggiungibili[k]["Nome"]
              + '</span><button type="button">+</button><div class="prezzo"><span class="currency">€</span><span class="value">'
              + aggiungibili[k]["CostoAggiunta"] + '</span></div>';
          }
          $("#modifica.modal .ingredienti-aggiungibili").find("ul").append(html);
        }

        html = '<div class="total"><span class="value">'
          + piatto['prezzo'] + '</span><span class="currency">€</span></div>'
          +'<button type="button" class="modal-aggiungi">Aggiungi</button>';
        $("#modifica.modal .modal-content").append(html);
        $("#aggiungi.modal").css("display", "none");
        $("#modifica.modal").css("display", "block");
      }
    });
  }

  $(document).on("click", "#aggiungi.modal .modal-modifica", function() {
    modify(null);
  });

  $(document).on("click", ".ingredienti-strutturali li button", function() {
    $(this).parent(".strutturale-row").siblings(".strutturale-alt").toggle();
    if($(this).html() == "Fine") {
      $(this).html("Modifica");
    } else {
      $(this).html("Fine");
    }
  });

  $(document).on("change", "input[type='radio']", function() {
    var previous = $(this).closest("div").find("input[checked='checked']");
    var strutturale_new_price = parseFloat($(this).parent("label").next(".prezzo").find(".value").html());
    var strutturale_old_price = parseFloat(previous.parent("label").next(".prezzo").find(".value").html());
    $("#modifica.modal .total>.value").html((parseFloat($("#modifica.modal .total>.value").html())
      + strutturale_new_price - strutturale_old_price).toFixed(2));
    $(previous).removeAttr("checked");
    $(this).attr("checked", "checked");
    $(this).parents(".strutturale-alt").siblings(".strutturale-row").find("span").html($(this).parent("label").text());
    console.log($(this).parent("label").text());
  });

  $(document).on("click", ".ingredienti-rimovibili li button", function() { // -
    var li = $(this).parent("li");
    li.remove();
    if(li.find(".prezzo").length == 0) {
      li.find("button").after('<div class="prezzo"><span class="value">0.00</span><span class="currency">€</span></div>');
    } else {
      $("#modifica.modal .total>.value").html((parseFloat($("#modifica.modal .total>.value").html())
        - parseFloat(li.find(".prezzo").find(".value").html())).toFixed(2));
    }
    li.find("button").html("+");
    $(".ingredienti-aggiungibili ul").append(li);
  });

  $(document).on("click", ".ingredienti-aggiungibili li button", function() { // +
    var li = $(this).parent("li");
    li.remove();
    li.find("button").html("-");
    $(".ingredienti-rimovibili ul").append(li);
    $("#modifica.modal .total>.value").html((parseFloat($("#modifica.modal .total>.value").html())
      + parseFloat(li.find(".prezzo").find(".value").html())).toFixed(2));
  });

  $(document).on("click", "#modifica.modal .modal-aggiungi", function() {

    var strutturali = Object.keys(piatto['ingredienti']['strutturali']).map(function(k,i) {
      return piatto['ingredienti']['strutturali'][k]["Id"];
    });

    /*
    var strutturali_ = piatto['ingredienti'].filter(function(i) { return i["StrutturaleYN"]; });
    var strutturali = Object.keys(strutturali_).map(function(k,i) {
      return strutturali_[k]["Id"];
    });
    */
    var modifiche = [];
    $(".ingredienti-strutturali li input[checked='checked']").each(function() {
      var id = parseInt($(this).attr('value'));
      if(!strutturali.includes(id)) {
        modifiche.push(new Ingredient(
          id, //id
          $(this).parent("label").text(), //nome
          //$(this).parent("label").next(".prezzo").find(".value").html(),
          true //isAddition
        ));
      }
    });

    var rimovibili = Object.keys(piatto['ingredienti']['rimovibili']).map(function(k,i) {
      return piatto['ingredienti']['rimovibili'][k]["Id"];
    });
    /*
    var rimovibili_ = piatto['ingredienti'].filter(function(i) { return !i["StrutturaleYN"]; });
    var rimovibili = Object.keys(rimovibili_).map(function(k,i) {
      return rimovibili_[k]["Id"];
    });
    */
    $(".ingredienti-rimovibili li").each(function() {
      var id = parseInt($(this).attr('data-id'));
      if(!rimovibili.includes(id)) {
        modifiche.push(new Ingredient(id, $(this).find("span").html(), true));
      }
    });

    var aggiungibili = Object.keys(piatto['ingredienti']['aggiungibili']).map(function(k,i) {
      return piatto['ingredienti']['aggiungibili'][k]["Id"];
    });
    /*
    var aggiungibili = Object.keys(ingredienti['fornitore']).map(function(k,i) {
      return ingredienti['fornitore'][k]["Id"];
    });
    */
    $(".ingredienti-aggiungibili li").each(function() {
      var id = parseInt($(this).attr('data-id'));
      if(!aggiungibili.includes(id)) {
        modifiche.push(new Ingredient(id, $(this).find("span").html(), false));
      }
    });
    if(piatto['key'] != null) cart.remove(piatto['key']);
    if(Object.keys(modifiche).length == 0) {
      add(piatto['id'], piatto['nome'], true, $("#modifica.modal .total>.value").html(), null);
    } else {
      add(piatto['id'], piatto['nome'], true, $("#modifica.modal .total>.value").html(), modifiche);
    }
    $("#modifica.modal").css("display", "none");
  });

  $(document).on("click", ".modal-content>.close", function() {
    $(this).parents(".modal").css("display", "none");
  });

  $(document).on("click touchstart", function(e) {
    if($(e.target).hasClass("modal")) {
      $(".modal").css("display", "none");
    }
  });

  $(document).on("click", ".carrello .minus", function() {
    /* decrease total */
    $(".total>.value").html((parseFloat($(".total>.value").html())
    - parseFloat(cart.items[$(this).parents("li").attr('data-key')].price)).toFixed(2));

    if(cart.decrease($(this).parents("li").attr('data-key'))) { //item removed
      $(this).parents("li").remove();
      if(Object.keys(cart.items).length == 0) { //cart empty
        emptyCart();
      }
    } else {
      $(this).next().html(parseInt($(this).next().html()) - 1);
    }
  });

  $(document).on("click", ".carrello .plus", function() {
    /* increase total */
    $(".total>.value").html((parseFloat($(".total>.value").html())
    + parseFloat(cart.items[$(this).parents("li").attr('data-key')].price)).toFixed(2));

    cart.increase($(this).parents("li").attr('data-key'));
    $(this).prev().html(parseInt($(this).prev().html()) + 1);
  });

  $(document).on("click", ".carrello .item-remove", function() {
    var key = $(this).parents("li").attr('data-key');
    $(".total>.value").html((parseFloat($(".total>.value").html())
    - parseFloat(cart.items[key].price * cart.items[key].quantity)).toFixed(2));
    cart.remove(key);
    $(this).parents("li").slideUp(function() {
      if(Object.keys(cart.items).length == 0) { //cart empty
        emptyCart();
      }
    });//.remove();

  });

  $(document).on("click", ".carrello .item-modify", function() {
    var key = $(this).parents("li").attr('data-key');
    piatto['id'] = cart.items[key].id;
    piatto['nome'] = cart.items[key].name;
    piatto['prezzo'] = cart.items[key].price;
    //piatto['quantity'] = cart.items[key].quantity;
    piatto['key'] = key;
    piatto['modifiche'] = cart.items[key].modifications;
    modify(cart.items[key].modifications);
  });

  $(document).on("click", ".checkout", function() {
    var piatti = {};
    for(key in cart.items) {
      piatti[key] = {};
      piatti[key]['id'] = cart.items[key].id;
      //piatti[key]['nome'] = cart.items[key].name;
      piatti[key]['quantita'] = cart.items[key].quantity;
      piatti[key]['modifiche'] = [];
      if(cart.items[key].modifications != null) {
        for(keyy in cart.items[key].modifications) {
          piatti[key]['modifiche'].push(cart.items[key].modifications[keyy]);
        }
      }
    }
    console.log(piatti);
    var data = {
      idfornitore: idfornitore,
      piatti: piatti
    };

    $.post("richiesta_checkout.php", data, function(state) {
      if(state === "success") {
        window.location.replace("./checkout.php");
      } else if(state === "login") {
        window.location.replace("./login.php?Atype=clienti");
      } else {
        displayAlert(" Qualcosa è andato storto: " +state);
      }
    });

  });
});

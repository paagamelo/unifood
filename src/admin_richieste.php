<?php

function getRichieste($conn, $filter, &$richieste) {
  $stmt = $conn->prepare(
    "SELECT RICHIESTE.Id, Descrizione, Nome
    FROM RICHIESTE, FORNITORI
    WHERE RICHIESTE.IdFornitore = FORNITORI.Id
    AND ApprovataYN = ?");
  if(
       !$stmt
    || !$stmt->bind_param("s", $filter)
    || !$stmt->execute())
  {
    die(closeConnectionAndReturn("errore"));
  }
  $result = $stmt->get_result();
  $stmt->close();
  $richieste = array();
  while($row = $result->fetch_assoc()) {
    $richieste[] = $row;
  }
}

require 'db_connect.php';
require 'login_functions.php';
sec_session_start(); //Avvio sessione php sicura

if($conn->connect_error) {
  die("errore");
}

$logged = loggedAs($conn, 'admin');

if($logged){

  getRichieste($conn, '0', $inattesa);
  getRichieste($conn, '1', $approvate);
?>
<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- JQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- Font Awesome (for icons) -->
  <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>
  <script src="./admin_richieste_script.js"></script>
  <link rel="stylesheet" type="text/css" href="./topbar/topbar_style.css">
  <link rel="stylesheet" type="text/css" href="./admin_richieste_style.css">
  <title>Richieste</title>
</head>
<body>
  <div class="main">
  <?php require './topbar/topbar.php'; closeConnection($conn); ?>
  <header><h1>Le tue richieste</h1></header>
  <ul class="breadcrumb">
    <li><a href="./unifood.php">Home</a></li>
    <li>Richieste</li>
  </ul>
  <div id="in-attesa">
    <header><h2>In Attesa</h2></header>
    <table>
      <thead>
        <th>Descrizione</th>
        <th>Richiedente</th>
        <th></th>
      </thead>
      </thead>
      <tbody>
        <?php
        $html = "";
        foreach($inattesa as $richiesta) {
          $html .= '<tr data-id="'.$richiesta["Id"].'"><td>'.$richiesta['Descrizione'].'</td><td>'.$richiesta["Nome"].'</td><td><button type="button">Approva</button></td></tr>';
        }
        echo $html;
        ?>
      </tbody>
    </table>
  </div>
  <div id="approvate">
    <header><h2>Approvate</h2></header>
    <table>
      <thead>
        <th>Descrizione</th>
        <th>Richiedente</th>
      </thead>
      </thead>
      <tbody>
        <?php
        $html = "";
        foreach($approvate as $richiesta) {
          $html .= '<tr data-id="'.$richiesta["Id"].'"><td>'.$richiesta['Descrizione'].'</td><td>'.$richiesta["Nome"].'</td></tr>';
        }
        echo $html;
        ?>
      </tbody>
    </table>
  </div>
  </div>
</body>
<?php } else header('Location: ./'.closeConnectionAndReturn($conn,"login.php?Atype=admin")); ?>

function displayAlert(type, message) {
  var prestring = "";
  switch(type) {
    case 'info': prestring = "Attenzione! "; break;
    case 'success': prestring = "Successo! "; break;
    case 'errore': prestring = 'Errore! '; break;
  }
  $(".modal").css("display", "none");
  $('.alert.' + type + '').remove();
  $(".main").prepend('<div class="alert ' + type + '"><span class="closebtn">&times;</span>'
    + '<strong>' + prestring + '</strong>' + message + '</div>');
}

$(document).ready(function() {

  //displayAlert('info', 'prova');

  $(document).on("click", ".alert .closebtn", function() {
    $(this).parent(".alert").remove();
  });

  $(document).on("click", "td>button", function() {
    var idrichiesta = $(this).parents("tr").attr('data-id');
    $.getJSON("richiesta_approvazione_richiesta.php?idrichiesta=" + idrichiesta,
    function(state) {
      console.log(state);
      if(state === 'success') {
        //displayAlert('success', 'Approvazione avvenuta con successo');
        location.reload(true);
      } else {
        displayAlert('error', 'Qualcosa è andato storto');
      }
    });
  });
});

<?php
  /* queries used by topbar, NOT safe with null arguments etc */

  include_once("common_queries.php");

  function getNotifiche(&$conn, $idutente, &$notifiche, $atype) {
    $notifiche = array();
    switch($atype) {
      case 'clienti':
        $stmt = $conn->prepare("SELECT * FROM NOTIFICHE WHERE IdUtente = ? ORDER BY LettaYN ASC");
        break;
      case 'fornitori':
        $stmt = $conn->prepare("SELECT * FROM NOTIFICHE WHERE IdFornitore = ? ORDER BY LettaYN ASC");
        break;
      case 'fattorini':
        $stmt = $conn->prepare("SELECT * FROM NOTIFICHE WHERE IdFattorino = ? ORDER BY LettaYN ASC");
        break;
      default:
        $notifiche["errore"] = true;
        return;
    }
    if(
         !$stmt
      || !$stmt->bind_param('i', $idutente)
      || !$stmt->execute())
    {
      $notifiche["errore"] = true;
    } else {
      $result = $stmt->get_result();
      while($notifica = $result->fetch_assoc()) {
        $notifiche[] = $notifica;
      }
      $stmt->close();
    }
  }

  function getOrdini(&$conn, $idutente, &$ordini, $atype) {
    $ordini = array();
    /* gets every order for the current user */
    switch($atype) {
      case 'clienti':
        $stmt = $conn->prepare(
          "SELECT * FROM ORDINI
          WHERE ORDINI.IdUtente = ?
          AND Stato <> 'Consegnato'
          ORDER BY Stato ASC, OraConsegna ASC");
        break;
      case 'fornitori':
        $stmt = $conn->prepare(
          "SELECT DISTINCT ORDINI.*
          FROM ORDINI, PIATTI_IN_ORDINE
          WHERE ORDINI.Id = PIATTI_IN_ORDINE.IdOrdine
          AND PIATTI_IN_ORDINE.IdFornitore = ?
          AND Stato = 'In preparazione'
          ORDER BY OraConsegna ASC");
        break;
      case 'fattorini':
        $stmt = $conn->prepare(
          "SELECT * FROM ORDINI, CLIENTI
          WHERE ORDINI.IdUtente = CLIENTI.Id
          AND IdFattorino = ?
          AND Stato = 'In consegna'
          ORDER BY OraConsegna ASC");
        break;
      default:
        $ordini["errore"] = true;
        return;
    }
    if(
         !$stmt
      || !$stmt->bind_param('i', $idutente)
      || !$stmt->execute())
    {
      $ordini["errore"] = true;
      return;
    }
    $result = $stmt->get_result();
    $stmt->close();
    while($row = $result->fetch_assoc()) {
      /* selects only recent orders
      (in preparazione e in consegna per i clienti, in preparazione per i fornitori) */
      //if($row['Stato'] != 'Consegnato' && ($atype == 'fornitori' ? ($row['Stato'] != 'In consegna') : true)) {
        $key = $row['Id'];
        $ordini[$key] = $row;
        $ordini[$key]["piatti"] = array();
        /* gets every item for the current order */
        $stmt = $conn->prepare(
            "SELECT * FROM PIATTI_IN_ORDINE, PIATTI
            WHERE PIATTI_IN_ORDINE.IdFornitore = PIATTI.IdFornitore
            AND PIATTI_IN_ORDINE.IdPiattoInFornitore = PIATTI.IdPiattoInFornitore
            AND PIATTI_IN_ORDINE.IdOrdine = ?"
          );
        if(
             !$stmt
          || !$stmt->bind_param('i', $row["Id"])
          || !$stmt->execute())
        {
          $ordini["errore"] = true;
          return;
        }
        $piatti = $stmt->get_result();
        $stmt->close();
        while($piatto = $piatti->fetch_assoc()) {
          getInfoFornitore($conn, $piatto["IdFornitore"], $fornitore);
          if((isset($fornitore["errore"]) && $fornitore["erorre"]) || empty($fornitore)) {
            $ordini["errore"] = true;
            return;
          }
          $ordini[$key]['fornitore'] = $fornitore["Nome"];
          $key_piatto = $piatto['IdPiattoInOrdine'];
          $ordini[$key]["piatti"][$key_piatto] = $piatto;
          $ordini[$key]["piatti"][$key_piatto]["modifiche"] = array();
          /* gets every modification fot the current item */
          $stmt = $conn->prepare(
              "SELECT * FROM MODIFICHE, INGREDIENTI
              WHERE MODIFICHE.IdIngrediente = INGREDIENTI.Id
              AND MODIFICHE.IdOrdine = ?
              AND MODIFICHE.IdPiattoInOrdine = ?"
          );
          if(
               !$stmt
            || !$stmt->bind_param("ii", $piatto["IdOrdine"], $piatto["IdPiattoInOrdine"])
            || !$stmt->execute())
          {
            $ordini["errore"] = true;
            return;
          }
          $modifiche = $stmt->get_result();
          $stmt->close();
          if($modifiche->num_rows > 0) {
            while($modifica = $modifiche->fetch_assoc()) {
              $ordini[$key]["piatti"][$key_piatto]["modifiche"][] = $modifica;
            }
          }
        //}
      }
    }
  }

?>

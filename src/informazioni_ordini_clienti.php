<?php

function getOrdini($id, $conn){
  $output = array();
  $stmt = $conn->prepare(
    " SELECT o.Id, o.Stato, o.Data, o.Totale as Prezzo, f.Nome as Fornitore
      FROM ORDINI o, FORNITORI f
      WHERE o.IdUtente = ?
      AND o.IdFornitore = f.Id;"
  );
  $stmt->bind_param("s", $id);
  if(!$stmt->execute()) $output["Errore"]= "Errore nel comunicare con il server";
  else { //Non si sono verificati errori
    $result = $stmt->get_result();
    if($result != false) while($row = $result->fetch_assoc()) $output[]=$row;
    if(empty($output)) $output["Errore"] = "Nessun ordine disponibile";
  }
  $stmt->close();
  return $output;
}


function getPiatti($id, $conn){
  $output = array();
  $stmt = $conn->prepare(
    " SELECT o.Id Ordine, po.IdPiattoInOrdine IdPiatto, p.Nome Piatto, po.Quantita
      FROM PIATTI_IN_ORDINE po, PIATTI p, FORNITORI f, ORDINI o
      WHERE o.IdUtente = ?
      AND o.Id = po.IdOrdine
      AND po.IdPiattoInFornitore = p.IdPiattoInFornitore
      AND po.IdFornitore = p.IdFornitore
      AND f.Id = po.IdFornitore;"
    );
  $stmt->bind_param("s", $id);
  if(!$stmt->execute()) $output["Errore"]= "Errore nel comunicare con il server";
  else { //Non si sono verificati errori
    $result = $stmt->get_result();
    if($result != false) while($row = $result->fetch_assoc()) $output[]=$row;
    if(empty($output)) $output["Errore"] = "Nessun ordine disponibile";
  }
  $stmt->close();
  return $output;
}

function getModifiche($id, $conn){
  $output = array();
  $stmt = $conn->prepare(
  " SELECT m.IdOrdine Ordine, m.IdPiattoInOrdine IdPiatto, i.Nome as Ingrediente, m.AggiuntaYN as Aggiunta
    FROM MODIFICHE m, INGREDIENTI i, FORNITURE_INGREDIENTI fi, ORDINI o
    WHERE o.IdUtente = ?
    AND m.IdOrdine = o.Id
    AND m.IdIngrediente = i.Id
    AND m.IdIngrediente = fi.IdIngrediente
    AND fi.IdFornitore = (SELECT IdFornitore
                          FROM PIATTI_IN_ORDINE po
                          WHERE po.IdOrdine = m.IdOrdine
                          LIMIT 1);"
    );
  $stmt->bind_param("s", $id);
  if(!$stmt->execute()) $output["Errore"]= "Errore nel comunicare con il server";
  else { //Non si sono verificati errori
    $result = $stmt->get_result();
    if($result != false) while($row = $result->fetch_assoc()) $output[]=$row;
    if(empty($result)) $output["Errore"] = "Nessun ordine disponibile";
  }
  $stmt->close();
  return $output;
}

 ?>

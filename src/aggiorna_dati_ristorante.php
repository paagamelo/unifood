<?php
require 'checkDati.php';
require 'db_connect.php';
require 'login_functions.php';

function checkDatiRistorante($nome, $indirizzo, $telefono, $speseConsegna, $daOra, $aOra, $maxOrdini){
  return
             checkRistorante($nome)
          && checkIndirizzo($indirizzo)
          && checkTelefono($telefono, true)
          && checkCosto($speseConsegna)
          && checkOrario($daOra)
          && checkorario($aOra)
          && checkRangeOrario($daOra, $aOra)
          && checkMaxOrdini($maxOrdini)
          && checkNote($note);
}

sec_session_start(); //Avvio sessione php sicura
if(loggedAs($conn, "fornitori") && isset($_POST["request"])) { //Login come fornitore effettuato e richiesta in post valida

   if ($conn->connect_error) die("error");

   $state = "";
   $id = $_SESSION["user_id"];

   switch ($_POST["request"]) {
      case "dati": //Aggiornamento dati ristorante

        $nome = trim($_POST["nome"]);
        $indirizzo = trim($_POST["indirizzo"]);
        $telefono = trim($_POST["telefono"]);
        $speseConsegna = str_replace(",",".",$_POST["speseConsegna"]);
        $orarioApertura = trim($_POST["orarioApertura"]);
        $orarioChiusura = trim($_POST["orarioChiusura"]);
        $maxOrdini = $_POST["maxOrdini"];
        $note = $_POST["note"];

        if(!checkDatiRistorante($nome,$indirizzo,$telefono,$speseConsegna,$orarioApertura,$orarioChiusura,$maxOrdini, $note)) die(closeConnectionAndReturn($conn,"dati-invalidi"));
        if(empty($maxOrdini)) $maxOrdini = null;
        if(empty($note)) $note = null;
        if(strlen($orarioApertura) == 4) $orarioApertura = "0".$orarioApertura;
        if(strlen($orarioChiusura) == 4) $orarioChiusura = "0".$orarioChiusura;
        $stmt = $conn->prepare("UPDATE FORNITORI SET Nome = ?, Indirizzo = ?, Telefono = ?, SpeseConsegna = ?, ApertoDa = ?, ApertoA = ?, MaxOrdiniContemporaneamente = ?, Note = ? WHERE Id = ?");
        $stmt->bind_param("sssssssss", $nome, $indirizzo, $telefono, $speseConsegna, $orarioApertura, $orarioChiusura, $maxOrdini, $note, $id);
        $state = ($stmt->execute() == false ? "error" : "ok");

        $stmt->close();
        print $state;
        break;

      case "aggiorna-logo": //Aggiornamento del logo

        if(!isset($_FILES['logo']) || !is_uploaded_file($_FILES['logo']['tmp_name'])) die(closeConnectionAndReturn($conn,"no-file"));
        //File mandato correttamente
        $estensione = strrchr($_FILES['logo']['type'],"/");
        $estensione = substr($estensione,1);
        $dir = './images/loghi/logo';
        $file = $dir.$id.".".$estensione;
        $tmp_img = $_FILES['logo']['tmp_name'];
        $is_img = getimagesize($tmp_img);
        if (!$is_img) die(closeConnectionAndReturn($conn,"no-image"));
        //E' un immagine
        $pre_file = glob($dir.$id.".*"); //Cerco l'eventuale logo precedente
        $pre_est = "";
        if(!empty($pre_file)) { //Esiste già un logo per questo fornitore, cambio il nome al logo precedente
          $pre_est = strrchr($pre_file[0],".");
          rename($pre_file[0],"./images/loghi/".$id.$pre_est);
        }
        $state = (move_uploaded_file($tmp_img,$file) ? "ok" : "error"); //Carico il logo nella cartella
        if(!empty($pre_file)){ //Esisteva un logo precedente
          if($state == "ok") unlink('./images/loghi/'.$id.$pre_est); //Elimino il logo precedente
          else rename('./images/loghi/'.$id.$pre_est,$pre_file[0]); //Bisogna ripristinare il logo precedente
        }
        print $state;
        break;

        case "rimuovi-logo": //Rimozione del logo
          $dir = './images/loghi/logo';
          $file = glob($dir.$id.".*"); //Cerco l'eventuale logo
          if(!empty($file)) $state = (unlink($file[0]) ? "ok" : "error");
          else $state = "no-logo";

          print $state;
          break;

        case "giorni" : //Aggiornamento dei giorni
          $giorni = $_POST["giorni"];
          if(!checkGiorni($giorni)) die(closeConnectionAndReturn($conn,"dati-invalidi"));
          $stmt = $conn->prepare("DELETE FROM GIORNI_LIBERI WHERE IdFornitore = ?");
          $stmt->bind_param("s", $id);
          $state = ($stmt->execute() == false ? "error" : "ok");
          if($state == "error") die(closeConnectionAndReturn($conn,"error"));
          $stmt->close();
          if(!empty($giorni)){
            $stmt = $conn->prepare("INSERT INTO GIORNI_LIBERI (IdFornitore,IdGiorno) VALUES (?,?)");
            $stmt->bind_param("ss", $id, $giorno);
            foreach ($giorni as $giorno) if($stmt->execute() == false) $state = "query-error";
            $stmt->close();
          }

          print $state;
          break;
  }
  closeConnection($conn);
} else die(closeConnectionAndReturn($conn,"error"));
?>

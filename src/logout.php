<?php

include 'login_functions.php';

sec_session_start(); //Avvio sessione php sicura
$_SESSION = array(); // Elimino tutti i valori della sessione
$params = session_get_cookie_params(); // Recupero i parametri di sessione
setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]); // Cancello i cookie attuali
session_destroy(); // Cancello la sessione
header('Location: ./unifood.php');

?>

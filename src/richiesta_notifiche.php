<?php
header('Content-Type: application/json');
$output = array();

require 'db_connect.php';
require 'login_functions.php';
include_once('./topbar_functions.php');
sec_session_start();

if($conn->connect_error) {
  $output["errore"] = "errore";
  die(json_encode($output));
}

if(!login_check($conn)) {
  $output["errore"] = "errore";
  closeConnection($conn);
  die(json_encode($output));
}

if($_SESSION['atype'] != 'admin') {
  getNotifiche($conn, $_SESSION['user_id'], $notifiche, $_SESSION['atype']);
  if(isset($notifiche["errore"]) && $notifiche["errore"]) {
    $output["errore"] = "errore";
    closeConnection($conn);
    die(json_encode($output));
  }

  print json_encode($notifiche);
}

closeConnection($conn);

?>

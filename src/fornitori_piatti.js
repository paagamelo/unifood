var idPiattoModifica = null;
var cucinaPiattoModifica = null;
var categoriaPiattoModifica = null;
var modificabilePiattoModifica = null;
var nomePiattoModifica = null;
var prezzoPiattoModifica = null;
var ingredientiPiattoModifica = null;
var ingredienti_cucine = [];
var cucineInfo = [];
var piatti_fornitore = [];
var ingredienti_piatti = [];
var categorie_ingredienti = [];
var modifica = "<button type=\"button\" class=\"modifica-piatto modify-btn\">Modifica</button>";
var no_cucine = "<div class=\"no-cucine\"> Nessuna cucina da mostrare </div>";
var no_piatti = "<div class=\"no-piatti\"> Nessun piatto da mostrare </div>";
var angolodx = "<span class=\"symbol a-d\" aria-label=\"Espandi\"><i aria-hidden=\"true\" class=\"fas fa-angle-right fa-lg\"></i></span>";
var angologiù = "<span class=\"symbol a-g\" aria-label=\"Riduci\"><i aria-hidden=\"true\" class=\"fas fa-angle-down fa-lg\"></i></span>";
var meno = "<span class=\"symbol minus\" aria-label=\"Rimuovi\"><i aria-hidden=\"true\" class=\"fas fa-minus-circle fa-lg\"></i></span>";
var più = "<span class=\"symbol plus\" aria-label=\"Aggiungi\"><i aria-hidden=\"true\" class=\"fas fa-plus-circle fa-lg\"></i></span>";

function checkCatVal(cat){
  return cat != null;
}

function parseIdCat(id){
  var risultato = [];
  var c1 = id.indexOf("c");
  var c2 = id.lastIndexOf("c");
  risultato["IdCucina"] = Number(id.substring(c1+1,c2));
  risultato["IdCategoria"] = Number(id.substr(c2+1));
  return risultato;
}

//Ottieni le info degli ingredienti data una cucina e l'id degli ingredienti
function calcolaIngredienti(ingredienti,cucina){
  var result = [];
  var infoIngredienti = ingredienti_cucine[cucina];
  for(ingr in infoIngredienti) if(ingredienti.indexOf(infoIngredienti[ingr]["Id"]) != -1) result.push(infoIngredienti[ingr]);
  return result;
}

//Ottieni le categorie in seguito alla cucina
function getCategorieCucina(idCucina){
  var display_categorie = "";
  var idselected = null;
  var selected = "selected";
  if(idCucina != null){ //Esiste almeno una cucina
    var categorie = cucineInfo[idCucina]["Categorie"];
    for(categoria in categorie){
      display_categorie += "<option value=\""+categoria+"\" "+selected+" >"+categorie[categoria]+"</option>";
      if(selected == "selected"){
        idselected = categoria;
        selected = "";
      }
    }
  }
  return display_categorie;
}
//Aggiungi piatto
function aggiungiPiatto(){
  var display_cucine = "";
  var selected = "selected";
  var idselected = null;
  for (cucina in cucineInfo) {
    display_cucine += "<option value=\""+cucina+"\" "+selected+" >"+cucineInfo[cucina]["Nome"]+"</option>";
    if(selected == "selected"){
      idselected = cucina;
      selected = "";
    }
  }
  if(idselected != null){
    cucina_corrente = idselected;
    var display_categorie = getCategorieCucina(idselected);
    if(display_categorie != ""){ //Esiste almeno una categoria
      $("#modal-file").load("./aggiungi_piatto.html",
      function(){
        $("#cucina").append(display_cucine);
        $("#categoria").append(display_categorie);
        if(!caricaIngredienti(cucina_corrente)) toggleAggiungi(false);
        $(".modal").show();
      });
    } else { //Errore in categorie
      $("#modal-file").append("<header><h3>Operazione non riuscita</h3></header><div>Qualcosa è andato storto, ricaricare la pagina</div>");
      $(".modal").show();
    }
  } else { //Errore in cucine
    $("#modal-file").append("<header><h3>Operazione non riuscita</h3></header><div>Qualcosa è andato storto, ricaricare la pagina</div>");
    $(".modal").show();
  }
}

//Modifica piatto
function modificaPiatto(piatto){

  idPiattoModifica = Number(piatto[0]["id"]);
  var infoId = parseIdCat($("#"+idPiattoModifica).parent().parent()[0]["id"]);
  var display_cucina = "<option value=\""+infoId["IdCucina"]+"\" selected >"+cucineInfo[infoId["IdCucina"]]["Nome"]+"</option>";
  var display_categorie = getCategorieCucina(infoId["IdCucina"]);
  nomePiattoModifica = $("#"+idPiattoModifica).find(".nome-piatto").html();
  prezzoPiattoModifica = $("#"+idPiattoModifica).find(".prezzo-piatto").html();
  cucinaPiattoModifica = infoId["IdCucina"];
  categoriaPiattoModifica = infoId["IdCategoria"];
  modificabilePiattoModifica = $("#"+idPiattoModifica).hasClass("modificabile");
  ingredientiPiattoModifica = ingredienti_piatti[idPiattoModifica];

  $("#modal-file").load("./aggiungi_piatto.html",
  function(){
    $("#modal-file h3").html("Modifica Piatto");
    $("#cucina").append(display_cucina);
    $("#cucina").attr("disabled","disabled");
    $("#cucina").addClass("disabled");
    $("#categoria").append(display_categorie);
    $("#categoria option[value="+categoriaPiattoModifica+"]").attr("selected", true);
    $("#nome-piatto").val(nomePiattoModifica);
    $("#costo").val(prezzoPiattoModifica);
    $("#modificabile").attr("checked",modificabilePiattoModifica);
    if(!caricaIngredienti(cucinaPiattoModifica)) toggleAggiungi(false);
    cucina_corrente = cucinaPiattoModifica;
    var ingr_cucina = [];
    var ing = ingredienti_cucine[cucina_corrente];
    var length = ing.length;
    var selezionatiComuni = 0;
    for(var i = 0; i<length; i++) ingr_cucina[ing[i]["Id"]] = ing[i];
    for(ingr in ingredientiPiattoModifica){ //Per ogni ingrediente del piatto
      var idIngrediente = ingredientiPiattoModifica[ingr];
      ingredienti_selezionati.push(Number(idIngrediente));
      ingredienti_selezionabili[ingredienti_selezionabili.indexOf(Number(idIngrediente))] = -1;
      var idCategoria = ingr_cucina[idIngrediente]["StrutturaleYN"] == 0 ? 0 : ingr_cucina[idIngrediente]["IdCategoria"];
      var codice = "c"+idCategoria+"i"+idIngrediente;
      var display_ingrediente = "<li class=\"ingrediente\" id=\""+codice+"\">"
                                +meno+"<span class=\"nome-ingrediente\">"+ingr_cucina[idIngrediente]["Nome"]+"</span></li>";
      $("#ingredienti-selezionati").append(display_ingrediente);

      if(idCategoria != 0) {
        categorie_selezionabili[categorie_selezionabili.indexOf(Number(idCategoria))] = null;
        $("#categoria-ingrediente option[value="+idCategoria+"]").remove();
      } else selezionatiComuni++;
    }

    if(ingredienti_categoria[0] != undefined && getIngredientiCategoria(0) == ""){
      categorie_selezionabili[categorie_selezionabili.indexOf(0)] = null;
      $("#categoria-ingrediente option[value=0]").remove();
    }

    var categorie_rimanenti = categorie_selezionabili.filter(checkCatVal);

    $("#ingrediente").empty();
    if(categorie_rimanenti.length > 0){
      var categoria_attuale = $("#categoria-ingrediente").val();
      $("#ingrediente").append(getIngredientiCategoria(categoria_attuale));
    } else toggleAggiungi(false);

    $("#aggiungi-piatto").attr("id","salva-modifiche-piatto");
    $(".modal").show();
  });
}

//Rimozione di un piatto
function rimuoviPiatto(piatto){
  var id = piatto[0]["id"];
  piatti_fornitore[id] = [];
  $.post("./aggiorna_piatti_fornitori.php",
  {
    request: "rimuovi",
    piatto: id
  },
  function(state){
    var messaggio = "";
    if(state == "error") messaggio = "Si è verificato un errore nel comunicare con il database";
    else if(state == "dati-invalidi") messaggio = "Dati inviati invalidi";
    else if(state == "query-error") messaggio = "Qualcosa è andato storto, si consiglia di ricaricare la pagina";
    if(messaggio != ""){ //Si è verificato qualche errore, mostra modal
      $("#modal-file").append("<header><h3>Operazione non riuscita</h3></header><div>"+messaggio+"</div>");
      $(".modal").show();
    } else {
      if($("#"+id).parent().find("li.piatto").length == 1) $("#"+id).parent().after(no_piatti);
      piatto.remove();
    }
  });
}

function getStringaIngredienti(ingredienti){
  var display_ingrediente = "";
  for(idIngrediente in ingredienti) display_ingrediente += " "+ingredienti[idIngrediente]["Nome"]+",";
  if(display_ingrediente != "")  display_ingrediente = "("+display_ingrediente.trim().substr(0,display_ingrediente.length-2)+")";
  return display_ingrediente;
}

function registraIngredientiPiatto(idpiatto,ingredienti){
  ingredienti_piatti[idpiatto] = [];
  for(idIngrediente in ingredienti) ingredienti_piatti[idpiatto].push(ingredienti[idIngrediente]["Id"]);
}
//Crea la stringa per il display della cucina
function displayCucina(idCucina){
  var cucina = cucine[idCucina];
  ingredienti_cucine[cucina["Id"]] = cucina["Ingredienti"];
  cucineInfo[cucina["Id"]] = [];
  cucineInfo[cucina["Id"]]["Nome"] = cucina["Nome"];
  var idcucina = "c"+cucina["Id"];
  var display_cucina =
                      "<li class=\"cucina\" id=\""+idcucina+"\">"
                    + "<span class=\"nome-cucina\">"+cucina["Nome"]+"</span>"
                    + "<ul class=\"categorie\">";
  var categorie = cucina["Categorie"];
  cucineInfo[cucina["Id"]]["Categorie"] = [];
  for(idCategoria in categorie){ //Per ogni categoria
    var categoria = categorie[idCategoria];
    cucineInfo[cucina["Id"]]["Categorie"][categoria["Id"]] = categoria["Nome"];
    var idcategoria = idcucina+"c"+categoria["Id"];
    var display_categoria =
                              "<li class =\"categoria\" id=\""+idcategoria+"\">"
                            + "<span class =\"nome-categoria\">" + categoria["Nome"] + "</span>"
                            + "<ul class=\"piatti\">";
    var piatti = categoria["Piatti"];
    for(idPiatto in piatti){ //Per ogni piatto
      var piatto = piatti[idPiatto];
      var idpiatto = piatto["IdPiattoInFornitore"];
      piatti_fornitore[idpiatto] = piatto;
      var display_ingredienti = getStringaIngredienti(piatto["ingredienti"]);
      registraIngredientiPiatto(idpiatto, piatto["ingredienti"]);
      var modificabile  =  (piatto["ModificabileYN"] == 0 ? "" : " modificabile");
      var display_piatto =
                                "<li class =\"piatto"+modificabile+"\" id=\""+idpiatto+"\">"
                              + "<span class =\"nome-piatto\">" + piatto["Nome"] + "</span>"
                              + "<span class=\"prezzo-piatto\">" + piatto["Prezzo"]+"</span>"
                              + "<span class=\"ingredienti-piatto\">" + display_ingredienti + "</span></li>";
      display_categoria += display_piatto;
    }
    display_cucina += display_categoria +"</ul>";
  }
  return display_cucina;
}

//Carica giorni
function loadPiatti(){
  $.get("./richiesta_piatti_fornitori.php",
  {
    request: "piatti"
  },
  function(data){
    var res = JSON.parse(data);
    if(res["Errore"] == undefined){ //Non ci sono stati errori
      cucine = res["Cucine"];
      for(cucina in cucine) $(".cucine").append(displayCucina(cucina));
      $(".nome-cucina").after(angolodx);
      $(".prezzo-piatto").after(modifica+meno);
      //$(".nome-piatto").before(meno);
      if($("ul.cucine").children().length == 0) {
        $("#piatti").after(no_cucine);

        $("button.piatti").addClass("disabled");
        $("button.piatti").attr("disabled","disabled");
      }
      var categorie = $("li.categoria");
      var length = categorie.length;
      for(var i = 0; i < length; i++){
        var categoria = categorie[i];
        var id = categoria["id"];
        if($("#"+id+" li.piatto").length == 0) $("#"+id).append(no_piatti);
      }
      var categorieI = res["CategorieIngredienti"]
      categorie_ingredienti[0] = "ingredienti comuni";
      for(cat in categorieI) categorie_ingredienti[categorieI[cat]["Id"]] = categorieI[cat]["Nome"];
    }
  });
}

$(document).ready(function(){

  //Carica dati al primo caricamento della pagina
  loadPiatti();

  //Chiudi modal
  $(document).on("touchend mouseup", ".close", function(){
    setTimeout(function() {
      $(".modal").hide();
      $("#modal-file").children().remove();
    }, 100);
  });

  //Cliccato l'angolo destro di una cucina
  $(document).on("touchend mouseup", ".a-d", function(){
    var context = this;
    setTimeout(function() {
      $(context).parent().find(".categorie").show();
      $(context).replaceWith(angologiù);
    }, 100);
  });

  //Cliccato l'angolo giù di una cucina
  $(document).on("touchend mouseup", ".a-g", function(){
    var context = this;
    setTimeout(function() {
      $(context).parent().find(".categorie").hide();
      $(context).replaceWith(angolodx);
    }, 100);
  });

  //Cliccato il meno
  $(document).on("touchend mouseup", ".piatto .minus", function(){
    rimuoviPiatto($(this).parent());
  });

  //Cliccato il bottone di modifica
  $(document).on("touchend mouseup", ".modifica-piatto", function(){
    modificaPiatto($(this).parent());
  });
});

<?php
require 'db_connect.php';
require 'login_functions.php';
require 'checkDati.php';

//Controlla correttezza parametri passati
function checkData($email, $password, $telefono, $username, $atype){
    return
                checkEmail($email)
            &&  checkPassword($password)
            &&  ($atype == "clienti" ? checkTelefono($telefono,false) : checkTelefono($telefono,true))
            &&  checkUsername($username);
}

//Controlla correttezza parametri passati per i fornitori
function checkFornitori($nome,$indirizzo,$speseConsegna,$apertoDa,$apertoA){
  return
              checkRistorante($nome)
          &&  checkIndirizzo($indirizzo)
          &&  checkCosto($speseConsegna)
          &&  checkOrario($apertoDa)
          &&  checkOrario($apertoA)
          &&  checkRangeOrario($apertoDa, $apertoA);
}

$atype = $_POST["atype"];
if(checkAtype($atype,true)){

  $dataOk = false;

  $email = trim($_POST["email"]);
  $password = $_POST["p"];
  $nome = trim($_POST["nome"]);
  $telefono= trim($_POST["telefono"]);
  $username= trim($_POST["username"]);

  if($atype == "fornitori"){
    $indirizzo = trim($_POST["indirizzo"]);
    $speseConsegna = str_replace(",",".",$_POST["speseConsegna"]);
    $apertoDa = trim($_POST["orarioApertura"]);
    $apertoA = trim($_POST["orarioChiusura"]);
    $dataOk = checkFornitori($nome,$indirizzo,$speseConsegna,$apertoDa, $apertoA);
  } else {
    $cognome = trim($_POST["cognome"]);
    $dataOk =  checkNome($nome) && checkCognome($cognome);
  }

  $dataOk = ($dataOk && checkData($email,$password, $telefono, $username, $atype));


  if($dataOk){ //Dati di registrazioni corretti

    $tabella = strtoupper($atype);
    $stmt=$conn->prepare("SELECT Email FROM ".$tabella." WHERE Email = ?");
    $stmt->bind_param('s', $email);
    if(!$stmt->execute()){
      header('Location: ./'.closeConnectionAndReturn($conn,"registrazione.php?Errore=1&Atype=".$atype)); //Errore nella query
      die();
    }
    $stmt->store_result();
    $stmt->bind_result($result);
    $stmt->fetch();
    if($stmt->num_rows > 0) {
      header('Location: ./'.closeConnectionAndReturn($conn,"registrazione.php?Errore=2&Atype=".$atype));//Email già esistente
      die();
    }
    $result = cripta_password($password);
    $stmt->close();
    if($atype == "fornitori"){
      $stmt=$conn->prepare("INSERT INTO ".$tabella." (Nome, Email, Telefono, Username, Password, Salt, Indirizzo, SpeseConsegna, ApertoDa, ApertoA) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
      $stmt->bind_param('ssssssssss', $nome, $email, $telefono, $username, $result["Password"], $result["Salt"], $indirizzo, $speseConsegna, $apertoDa, $apertoA);
    } else {
      $stmt=$conn->prepare("INSERT INTO ".$tabella."(Nome, Cognome, Email, Telefono, Username, Password, Salt) VALUES (?, ?, ?, ?, ?, ?, ?)");
      $stmt->bind_param('sssssss', $nome, $cognome, $email, $telefono, $username, $result["Password"], $result["Salt"]);
    }
    if($stmt->execute()){ //Registrazione eseguita
      $stmt->close();
      sec_session_start(); // Avvio sessione php sicura
      if(login($email, $password, $atype, $conn) == 0) header('Location: ./'.closeConnectionAndReturn($conn,($atype != "fornitori" ? "unifood.php" : "fornitori_ristorante.php"))); //Login effettuato
      else header('Location: ./'.closeConnectionAndReturn($conn,"login.php?Atype=".$atype)); //Problema con il login
    } else header('Location: ./'.closeConnectionAndReturn($conn,"registrazione.php?Errore=1&Atype=".$atype)); //Errore nella query
  } else header('Location: ./'.closeConnectionAndReturn($conn,"registrazione.php?Errore=3&Atype=".$atype)); //Dati passati non validi
} else print closeConnectionAndReturn($conn,'Invalid request');

?>

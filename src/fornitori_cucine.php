<!DOCTYPE html>
<?php
require 'db_connect.php';
require 'login_functions.php';

sec_session_start(); //Avvio sessione php sicura
$logged = loggedAs($conn,"fornitori");
if($logged) { //Login effettuato
?>
  <html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Font Awesome (for icons) -->
    <script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js" integrity="sha384-EIHISlAOj4zgYieurP0SdoiBYfGJKkgWedPHH4jCzpCXLmzVsw1ouK59MuUtP4a1" crossorigin="anonymous"></script>
    <!-- Topbar style -->
    <link rel="stylesheet" type="text/css" href="./topbar/topbar_style.css">
    <link rel="stylesheet" type="text/css" href="fornitori_cucine_style.css">
    <link rel="stylesheet" type="text/css" href="modal.css">
    <link rel="stylesheet" type="text/css" href="breadcrumb.css">
    <script src="./fornitori_cucine.js"></script>
    <script src="./crea_richiesta.js"></script>
    <script src="./checkDati.js"></script>
    <title>Le Tue Cucine</title>
  </head>
  <body>
    <header>
      <h1>Le Tue Cucine</h1>
    <ul class="breadcrumb">
      <li><a href="./unifood.php">Home</a></li>
      <li>Le Tue Cucine</li>
    </ul>
    </header>
    <section id="cucine-fornite">
      <header><h2>Le cucine che fornisci</h2>
      <button type="button" class="cucine modify-btn" onclick="toggleModify();">Modifica</button>
      </header>
      <ul class="cucine cucine-fornite">
        <!--Cucine Fornite -->
      </ul>
    </section>
    <section id="cucine-non-fornite" class="hidden">
      <header><h2>Altre cucine che puoi fornire</h2></header>
        <ul class="cucine cucine-non-fornite">
          <!-- Cucine Non Fornite-->
        </ul>
      <button type="button" class="richieste modify-btn" onclick="creaRichiesta();">Proponi categoria</button>
    </section>
  <!-- The Modal -->
  <div class="modal">
    <!-- Modal content -->
    <div class="modal-content">
      <span class="close">&times;</span>
      <!-- Load modal file-->
      <div id="modal-file">

      </div>
    </div>
  </div>
    <?php require './topbar/topbar.php'; closeConnection($conn);?>
  </body>
  </html>
<?php
} else header('Location: ./'.closeConnectionAndReturn($conn,"login.php?Atype=fornitori")); //Effettua il login per accedere
?>

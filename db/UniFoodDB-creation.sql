-- Database Section
-- ________________

/*
create database unifood;
use unifood;
*/


-- Tables Section
-- _____________

/* ENGINE=MyISAM to enable AUTO_INCREMENT in a column which is not primary key */
/* Note that: if a table has a constraint with another table and they have different engines this may cause some errors, so MyISAM is used even for table that normally wouldn't need it */

create table ADMIN (
     Id int not null AUTO_INCREMENT PRIMARY KEY,
     Username char(15) not null,
     Email char(30) not null,
     Password char(128) not null,
     Salt char(128) not null);


create table CATEGORIE_CIBO (
     IdCategoriaInCucina bigint not null AUTO_INCREMENT,
     IdCucina bigint not null,
     Nome char(25) not null,
     constraint IDCATEGORIE_CIBO primary key (IdCucina, IdCategoriaInCucina)
)ENGINE=MyISAM;

create table CATEGORIE_INGREDIENTI (
     Id bigint not null AUTO_INCREMENT PRIMARY KEY,
     Nome char(25) not null);

create table CLIENTI (
     Id bigint not null AUTO_INCREMENT PRIMARY KEY,
     Nome char(15) not null,
     Cognome char(15) not null,
     Email char(30) not null,
     Telefono char(10) null,
     Username char(15) not null,
     Password char(128) not null,
     Salt char(128) not null,
     PuntiFedelta int,
     BuonoSconto int);

create table CUCINE (
     Id bigint not null AUTO_INCREMENT PRIMARY KEY,
     Nome char(15) not null,
     PathImmagine char(20) not null);

create table FATTORINI (
     Id bigint not null AUTO_INCREMENT PRIMARY KEY,
     Nome char(15) not null,
     Cognome char(15) not null,
     Email char(30) not null,
     Telefono char(10) not null,
     Username char(15) not null,
     Password char(128) not null,
     Salt char(128) not null,
     IdFornitore bigint);

create table FORNITORI (
     Id bigint not null AUTO_INCREMENT PRIMARY KEY,
     Nome char(15) not null,
     Email char(30) not null,
     Telefono char(10) not null,
     Username char(15) not null,
     Password char(128) not null,
     Salt char(128) not null,
     Indirizzo char(30) not null,
     PathLogo char(20),
     SpeseConsegna decimal(4,2) not null,
     Note char(100),
     Stelle int,
     MaxOrdiniContemporaneamente int,
     ApertoDa char(5) not null,
     ApertoA char(5) not null);

create table FORNITURE (
     IdFornitore bigint not null,
     IdCucina bigint not null,
     constraint IDFornitura primary key (IdFornitore, IdCucina));

create table FORNITURE_INGREDIENTI (
     IdIngrediente bigint not null,
     IdFornitore bigint not null,
     IdCucina bigint not null,
     CostoAggiunta decimal(4,2) not null,
     constraint IDDISPOSIZIONI primary key (IdIngrediente, IdFornitore, IdCucina));

create table GIORNI (
     Id int not null AUTO_INCREMENT PRIMARY KEY,
     Nome char(10) not null);

create table GIORNI_LIBERI (
     IdGiorno int not null,
     IdFornitore bigint not null,
     constraint IDGIORNI_LIBERI primary key (IdFornitore, IdGiorno));

create table INGREDIENTI (
     Id bigint not null AUTO_INCREMENT PRIMARY KEY,
     Nome char(25) not null,
     StrutturaleYN char not null,
     IdCategoria bigint);

create table METODI_DI_PAGAMENTO (
     IdCliente bigint not null,
     IdMetodo int not null AUTO_INCREMENT,
     Info char(25) not null,
     constraint IDMETODI_DI_PAGAMENTO primary key (IdCliente, IdMetodo)
)ENGINE=MyISAM;

create table MODIFICHE (
     IdOrdine bigint not null,
     IdPiattoInOrdine bigint not null,
     IdIngrediente bigint not null,
     AggiuntaYN char,
     constraint IDMODIFICHE primary key (IdIngrediente, IdOrdine, IdPiattoInOrdine)
)ENGINE=MyISAM;

create table NOTIFICHE (
     Id bigint not null AUTO_INCREMENT PRIMARY KEY,
     Testo text not null,
     LettaYN char not null,
     IdFattorino bigint,
     IdFornitore bigint,
     IdUtente bigint,
     IdOrdine bigint);

create table ORDINI (
     Id bigint not null AUTO_INCREMENT,
     OraConsegna char(5) not null,
     PianoConsegna int not null,
     Stato char(15) not null,
     Data date not null,
     Totale decimal(6,2) not null,
     PagatoYN char not null,
     Note char(100),
     IdFornitore bigint not null,
     IdUtente bigint not null,
     IdFattorino bigint,
     constraint IDOrdine primary key (Id));

create table PIATTI (
     IdFornitore bigint not null,
     IdPiattoInFornitore bigint not null AUTO_INCREMENT,
     Nome char(25) not null,
     Prezzo decimal(4,2) not null,
     ModificabileYN char not null,
     Carboidrati int,
     Proteine int,
     Grassi int,
     IdCategoriaInCucina bigint not null,
     IdCucina bigint not null,
     constraint IDPiatto_ID primary key (IdFornitore, IdPiattoInFornitore)
)ENGINE=MyISAM;

create table PIATTI_IN_ORDINE (
     IdOrdine bigint not null,
     IdPiattoInOrdine bigint not null AUTO_INCREMENT,
     Quantita int not null,
     IdFornitore bigint not null,
     IdPiattoInFornitore bigint not null,
     constraint IDCibo_in_ordine primary key (IdOrdine, IdPiattoInOrdine)
)ENGINE=MyISAM;

create table PIATTI_INGREDIENTI (
     IdIngrediente bigint not null,
     IdFornitore bigint not null,
     IdPiattoInFornitore bigint not null,
     constraint IDCOMPOSIZIONI primary key (IdIngrediente, IdFornitore, IdPiattoInFornitore)
)ENGINE=MyISAM;

create table RECENSIONI (
     IdFornitore bigint not null,
     IdCliente bigint not null,
     Stelle int not null,
     descrizione char(50) not null,
     constraint IDRecensione primary key (IdCliente, IdFornitore));

create table RICHIESTE (
     Id bigint not null AUTO_INCREMENT PRIMARY KEY,
     Descrizione char(75) not null,
     ApprovataYN char not null,
     IdAdmin int,
     IdFornitore bigint not null);

create table LOGIN_ATTEMPTS (
    Id bigint not null,
    Time varchar(30) not null,
    AccountType char(9)
)ENGINE=InnoDB;


-- Constraints Section
-- ___________________

alter table CATEGORIE_CIBO add constraint FKrelativa_a_
     foreign key (IdCucina)
     references CUCINE (Id);

alter table FATTORINI add constraint FKlavora_per_
     foreign key (IdFornitore)
     references FORNITORI (Id);

alter table FORNITURE add constraint FKper_
     foreign key (IdCucina)
     references CUCINE (Id);

alter table FORNITURE add constraint FKha_
     foreign key (IdFornitore)
     references FORNITORI (Id);

alter table FORNITURE_INGREDIENTI add constraint FKdis_ING
     foreign key (IdIngrediente)
     references INGREDIENTI (Id);

alter table GIORNI_LIBERI add constraint FKGIO_FOR
     foreign key (IdFornitore)
     references FORNITORI (Id);

alter table GIORNI_LIBERI add constraint FKGIO_GIO
     foreign key (IdGiorno)
     references GIORNI (Id);

alter table INGREDIENTI add constraint FKappartenenza
     foreign key (IdCategoria)
     references CATEGORIE_INGREDIENTI (Id);

alter table METODI_DI_PAGAMENTO add constraint FKpossiede_
     foreign key (IdCliente)
     references CLIENTI (Id);

alter table MODIFICHE add constraint FKmod_ING
     foreign key (IdIngrediente)
     references INGREDIENTI (Id);

alter table MODIFICHE add constraint FKmod_PIA
     foreign key (IdOrdine, IdPiattoInOrdine)
     references PIATTI_IN_ORDINE (IdOrdine, IdPiattoInOrdine)
     ON DELETE CASCADE
     ON UPDATE CASCADE;

alter table NOTIFICHE add constraint FKa2_
     foreign key (IdFattorino)
     references FATTORINI (Id);

alter table NOTIFICHE add constraint FKa3_
     foreign key (IdFornitore)
     references FORNITORI (Id);

alter table NOTIFICHE add constraint FKa1_
     foreign key (IdUtente)
     references CLIENTI (Id);

alter table NOTIFICHE add constraint FKriguardo_
     foreign key (IdOrdine)
     references ORDINI (Id);

alter table ORDINI add constraint FKeffettuazione
     foreign key (IdUtente)
     references CLIENTI (Id);

alter table ORDINI add constraint FKassegnato_a_
     foreign key (IdFattorino)
     references FATTORINI (Id);

alter table ORDINI add constraint FKordini_fornitore_
     foreign key (IdFornitore)
     references FORNITORI (Id);

-- Not implemented
-- alter table PIATTI add constraint IDPiatto_CHK
--     check(exists(select * from PIATTI_INGREDIENTI
--                  where PIATTI_INGREDIENTI.IdFornitore = IdFornitore and PIATTI_INGREDIENTI.IdPiattoInFornitore = IdPiattoInFornitore));

alter table PIATTI add constraint FKpreparazione
     foreign key (IdFornitore)
     references FORNITORI (Id);

alter table PIATTI add constraint FKR
     foreign key (IdCategoriaInCucina, IdCucina)
     references CATEGORIE_CIBO (IdCategoriaInCucina, IdCucina);

alter table PIATTI_IN_ORDINE add constraint FKper_un_
     foreign key (IdFornitore, IdPiattoInFornitore)
     references PIATTI (IdFornitore, IdPiattoInFornitore);

alter table PIATTI_IN_ORDINE add constraint FKrelativo_a_
     foreign key (IdOrdine)
     references ORDINI (Id)
     ON DELETE CASCADE;

alter table PIATTI_INGREDIENTI add constraint FKcom_PIA
     foreign key (IdFornitore, IdPiattoInFornitore)
     references PIATTI (IdFornitore, IdPiattoInFornitore);

alter table PIATTI_INGREDIENTI add constraint FKcom_ING
     foreign key (IdIngrediente)
     references INGREDIENTI (Id);

alter table RECENSIONI add constraint FKscrive_
     foreign key (IdCliente)
     references CLIENTI (Id);

alter table RECENSIONI add constraint FKsu_un_
     foreign key (IdFornitore)
     references FORNITORI (Id);

alter table RICHIESTE add constraint FKapprovata_da_
     foreign key (IdAdmin)
     references ADMIN (Id);

alter table RICHIESTE add constraint FKrichiesta_aggiunta_elemento
     foreign key (IdFornitore)
     references FORNITORI (Id);

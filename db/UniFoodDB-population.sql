/* Cucine */

INSERT INTO CUCINE(Nome, PathImmagine) VALUES ("pizza", "dummy_path");
INSERT INTO CUCINE(Nome, PathImmagine) VALUES ("panini", "dummy_path");
INSERT INTO CUCINE(Nome, PathImmagine) VALUES ("italiano", "dummy_path");
INSERT INTO CUCINE(Nome, PathImmagine) VALUES ("giapponese", "dummy_path");
INSERT INTO CUCINE(Nome, PathImmagine) VALUES ("cinese", "dummy_path");

/* Categorie cibo */

INSERT INTO CATEGORIE_CIBO(IdCucina, Nome) VALUES(1, "pizze bianche");
INSERT INTO CATEGORIE_CIBO(IdCucina, Nome) VALUES(1, "pizze rosse");
INSERT INTO CATEGORIE_CIBO(IdCucina, Nome) VALUES(1, "pizze classiche");
INSERT INTO CATEGORIE_CIBO(IdCucina, Nome) VALUES(1, "pizze speciali");

INSERT INTO CATEGORIE_CIBO(IdCucina, Nome) VALUES(2, "hamburger di carne");
INSERT INTO CATEGORIE_CIBO(IdCucina, Nome) VALUES(2, "hamburger di pollo");
INSERT INTO CATEGORIE_CIBO(IdCucina, Nome) VALUES(2, "panini classici");

INSERT INTO CATEGORIE_CIBO(IdCucina, Nome) VALUES(3, "primi");
INSERT INTO CATEGORIE_CIBO(IdCucina, Nome) VALUES(3, "secondi");

INSERT INTO CATEGORIE_CIBO(IdCucina, Nome) VALUES(4, "ramen");

INSERT INTO CATEGORIE_CIBO(IdCucina, Nome) VALUES(5, "piatti di riso");

/* Clienti */
/* la password è "password" per entrambi */

INSERT INTO CLIENTI(Nome, Cognome, Email, Telefono, Username, Password, Salt) VALUES ("Lorenzo", "Paganelli", "lori_paga@example.it", 3339786695, "paagamelo", "c417e51c389711bfb55ae2c64c621d93e823e0e981df207a451dc26f538e3d3762af538d089bf0920d2d5714e32687db03586a5ca21b1f79f088973135f537d7", "5625d37e97b971eed0448847d8885535e019a64e458178cda768c4d949d2ddad78d5fa4df59549999797cee99cca2bb5ed6a7971c6245c8f6dc492c14bec6fb0");

INSERT INTO CLIENTI(Nome, Cognome, Email, Telefono, Username, Password, Salt) VALUES ("Alessandro", "Retico", "ale_retico@example.it", 1023456789, "aleretico", "5a17ee6adbc0ab6f4d723bac5a7ae9b557249283e66a905ec383cbcd532fbde4d7f0f476b5fa6b6298543ea59128b4c2ae92a9c2945d1aa4329a6deb49871d70", "e968c4f99ff2618cb79de8e899d1923c9e5be62103a6ef143db6c8e2154327186c73951091a500a4d3c95415c68537f1613b07f03105a643935440c1ac3b0979");

/* Admin */
/* la password è "password" per entrambi */
INSERT INTO ADMIN(Username, Email, Password, Salt) VALUES ("paagamelo", "lori_paga@example.it", "5a17ee6adbc0ab6f4d723bac5a7ae9b557249283e66a905ec383cbcd532fbde4d7f0f476b5fa6b6298543ea59128b4c2ae92a9c2945d1aa4329a6deb49871d70", "e968c4f99ff2618cb79de8e899d1923c9e5be62103a6ef143db6c8e2154327186c73951091a500a4d3c95415c68537f1613b07f03105a643935440c1ac3b0979");
INSERT INTO ADMIN(Username, Email, Password, Salt) VALUES ("aleretico", "ale_retico@example.it", "5a17ee6adbc0ab6f4d723bac5a7ae9b557249283e66a905ec383cbcd532fbde4d7f0f476b5fa6b6298543ea59128b4c2ae92a9c2945d1aa4329a6deb49871d70", "e968c4f99ff2618cb79de8e899d1923c9e5be62103a6ef143db6c8e2154327186c73951091a500a4d3c95415c68537f1613b07f03105a643935440c1ac3b0979");

/* Giorni */

INSERT INTO GIORNI(Nome) VALUES("lunedi"),("martedi"),("mercoledi"),("giovedi"),("venerdi");

/* Fornitori */
/* la password è "password" per tutti */

INSERT INTO `FORNITORI`(`Nome`, `Email`, `Telefono`, `Username`, `Password`, `Salt`, `Indirizzo`, `PathLogo`, `SpeseConsegna`, `MaxOrdiniContemporaneamente`, `ApertoDa`, `ApertoA`) VALUES
("Piadivina", "piadivina@example.com", "0541623964", "piadivina", "5a17ee6adbc0ab6f4d723bac5a7ae9b557249283e66a905ec383cbcd532fbde4d7f0f476b5fa6b6298543ea59128b4c2ae92a9c2945d1aa4329a6deb49871d70", "e968c4f99ff2618cb79de8e899d1923c9e5be62103a6ef143db6c8e2154327186c73951091a500a4d3c95415c68537f1613b07f03105a643935440c1ac3b0979", "Via Fabio Filzi 21", "placeholder", 2, 25, "08:00", "17:00"),
("Stuzzico", "stuzzico@example.com", "0541697345", "stuzzico", "5a17ee6adbc0ab6f4d723bac5a7ae9b557249283e66a905ec383cbcd532fbde4d7f0f476b5fa6b6298543ea59128b4c2ae92a9c2945d1aa4329a6deb49871d70", "e968c4f99ff2618cb79de8e899d1923c9e5be62103a6ef143db6c8e2154327186c73951091a500a4d3c95415c68537f1613b07f03105a643935440c1ac3b0979", "Via Degli Argonauti 54", "placeholder", 0, 40, "08:00", "22:00"),
("Il Passatore", "passatore@example.com", "0543678235", "passatore", "5a17ee6adbc0ab6f4d723bac5a7ae9b557249283e66a905ec383cbcd532fbde4d7f0f476b5fa6b6298543ea59128b4c2ae92a9c2945d1aa4329a6deb49871d70", "e968c4f99ff2618cb79de8e899d1923c9e5be62103a6ef143db6c8e2154327186c73951091a500a4d3c95415c68537f1613b07f03105a643935440c1ac3b0979", "Via Righi 3", "placeholder", 4, 20, "12:00", "22:00");

/* Forniture */

INSERT INTO FORNITURE(IdFornitore,IdCucina) VALUES(1,1),(1,2),(2,1),(2,2),(3,3);

/* Categorie ingredienti */

INSERT INTO CATEGORIE_INGREDIENTI(Nome) VALUES("dimensione impasto"),("tipo impasto"),("tipo carne");

/* Ingredienti */

INSERT INTO INGREDIENTI(Nome, StrutturaleYN) VALUES("pomodoro", false),("mozzarella", false),("salsiccia", false),("prosciutto cotto", false),("patatine", false),
("formaggio", false),("cipolla", false),("ketchup", false),("maionese", false),("insalata", false), ("mortadella", false);

INSERT INTO INGREDIENTI(Nome, StrutturaleYN, IdCategoria) VALUES("impasto base", true, 1), ("doppio impasto", true, 1), ("impasto classico", true, 2), ("impasto al kamut", true, 2), ("carne di maiale", true, 3), ("carne di chianina", true, 3), ("svizzera di pollo", true, 3);

/* Piatti */

INSERT INTO PIATTI(IdFornitore, Nome, Prezzo, ModificabileYN, IdCategoriaInCucina, IdCucina) VALUES
(1, "margherita", 4.5, true, 3, 1),
(1, "salsiccia", 6, true, 3, 1),
(1, "bianca con salsiccia", 5, true, 1, 1),
(1, "rossa con salsiccia", 5, true, 2, 1),
(1, "hamburger", 7, 1, true, 2),
(1, "cheeseburger", 8, true, 1, 2),
(1, "panino con mortadella", 4, true, 3, 2),

(2, "margherita", 4.5, false, 3, 1),
(2, "salsiccia", 6, true, 3, 1),
(2, "bianca con salsiccia", 5, true, 1, 1),
(2, "rossa con salsiccia", 5, true, 2, 1),
(2, "hamburger", 7, true, 1, 2),
(2, "cheeseburger", 8, true, 1, 2),
(2, "panino con mortadella", 4, true, 3, 2),
(2, "panino con cotto", 5, true, 3, 2);

/* mancano quelli del passatore se vogliamo */

/* Forniture_ingredienti */

INSERT INTO FORNITURE_INGREDIENTI(IdIngrediente, IdFornitore, IdCucina, CostoAggiunta) VALUES
(1, 1, 1, 2.5),
(2, 1, 1, 1.5),
(3, 1, 1, 1.5),
(4, 1, 1, 1.5),
(5, 1, 1, 1),
(6, 1, 2, 2),
(7, 1, 2, 1.5),
(8, 1, 2, 1.5),
(9, 1, 2, 1.5),
(10, 1, 2, 1),
(11, 1, 2, 2),
(12, 1, 1, 0),
(13, 1, 1, 2.5),
(14, 1, 1, 0),
(15, 1, 1, 1.5),
(16, 1, 2, 0),
(17, 1, 2, 2.5),
(18, 1, 2, 3),
(4, 1, 2, 1.5),
(1, 1, 2, 1),

(1, 2, 1, 2.5),
(2, 2, 1, 1.5),
(3, 2, 1, 1.5),
(4, 2, 1, 1.5),
(5, 2, 1, 1),
(6, 2, 2, 2),
(7, 2, 2, 1.5),
(8, 2, 2, 1.5),
(9, 2, 2, 1.5),
(10, 2, 2, 1),
(11, 2, 2, 2),
(12, 2, 1, 0),
(13, 2, 1, 2.5),
(14, 2, 1, 0),
(15, 2, 1, 1.5),
(16, 2, 2, 0),
(17, 2, 2, 2.5),
(18, 2, 2, 3),
(4, 2, 2, 1.4),
(1, 2, 2, 1);


/* manca il passatore volendo */

INSERT INTO PIATTI_INGREDIENTI(IdIngrediente, IdFornitore, IdPiattoInFornitore) VALUES
(1, 1, 1),
(2, 1, 1),
(1, 1, 2),
(2, 1, 2),
(3, 1, 2),
(2, 1, 3),
(3, 1, 3),
(1, 1, 4),
(3, 1, 4),
(16, 1, 5),
(8, 1, 5),
(9, 1, 5),
(10, 1, 5),
(1, 1, 5),
(16, 1, 6),
(8, 1, 6),
(9, 1, 6),
(10, 1, 6),
(1, 1, 6),
(6, 1, 6),
(11, 1, 7),

(1, 2, 1),
(2, 2, 1),
(1, 2, 2),
(2, 2, 2),
(12, 2, 2),
(14, 2, 2),
(3, 2, 2),
(2, 2, 3),
(3, 2, 3),
(1, 2, 4),
(3, 2, 4),
(16, 2, 5),
(8, 2, 5),
(9, 2, 5),
(10, 2, 5),
(1, 2, 5),
(16, 2, 6),
(8, 2, 6),
(9, 2, 6),
(10, 2, 6),
(1, 2, 6),
(6, 2, 6),
(11, 2, 7),
(4, 2, 8);

/* Fattorini */
/* la password è "password" per entrambi */
INSERT INTO FATTORINI(Nome,Cognome,Email,Telefono,Username,Password,Salt,IdFornitore)  VALUES
("Mario", "Rossi", "mario.rossi@example.com","1234567890","mario_rossi","ba9284b9d77443e1c77847b7163264d545f7182529fa0259aeecf77733dcc540ae95b2ba3f567133e34a6dc81c5941ab2fd8bc616c7998364e53fd0343249a2d","298ed6fc9195e239ddbdf31cfe5b5491049173a2b995c3315b1a8b6092135f7e9f263ee42daf85de5cb252aece0366e4da083e5fbe2e145f8832e4a037483a7b",1),
("Giacomo", "Verdi", "giacomo.verdi@example.com","1234567810","giova_verdi","ba9284b9d77443e1c77847b7163264d545f7182529fa0259aeecf77733dcc540ae95b2ba3f567133e34a6dc81c5941ab2fd8bc616c7998364e53fd0343249a2d","298ed6fc9195e239ddbdf31cfe5b5491049173a2b995c3315b1a8b6092135f7e9f263ee42daf85de5cb252aece0366e4da083e5fbe2e145f8832e4a037483a7b",2);

/* Ordini */
INSERT INTO ORDINI(OraConsegna, PianoConsegna, Stato, Data, Totale, PagatoYN, IdFornitore, IdUtente, IdFattorino) VALUES
("12:00",1,"Consegnato","2018-10-31",32,1,1,2,1),
("14:00",3,"In preparazione","2019-01-10",15.50,0,2,2,NULL);

/* Piatti in ordine */
INSERT INTO  PIATTI_IN_ORDINE(IdOrdine, Quantita, IdFornitore, IdPiattoInFornitore) VALUES
(1,1,1,1),
(1,2,1,2),
(2,2,2,3),
(2,1,2,3);

/* Modifiche */
INSERT INTO MODIFICHE(IdOrdine, IdPiattoInOrdine, IdIngrediente, AggiuntaYN) VALUES
(1,2,2,0),
(2,1,5,1);
